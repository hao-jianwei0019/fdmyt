//#include <algorithm>
//#include <iostream>
//using namespace std;
//const int N = 1e6 + 10;
//int nums[N];
//int n, p;
//int main() {
//    cin >> n >> p;
//    for (int i = 0; i < n; i++) {
//        cin >> nums[i];
//    }
//    sort(nums, nums + n);
//    int l = 0;
//    int r = nums[l] + p + p;
//    int ans = 0;
//
//    while (l<n && n - l>ans)
//    {
//        int tmp = 0;
//        for (int i = l; i < n; i++)
//        {
//            if (nums[i] <= r)tmp++;
//            else break;
//        }
//        ans = max(tmp, ans);
//        l++;
//        while (nums[l] == nums[l - 1])l++;
//        int r = nums[l] + p + p;
//
//    }
//    cout << ans << endl;
//    return 0;
//}
//// 64 位输出请用 printf("%lld")

#include <cstdio>
#include <iostream>

using namespace std;

const int N = 2e6 + 10;
int n, p, a[N], d[N], ans;

int main()
{
    scanf("%d%d", &n, &p);
    for (int i = 1; i <= n; i++)
    {
        scanf("%d", &a[i]);
        d[max(a[i] - p, 0)] += 1, d[a[i] + p + 1] -= 1;//代表【a[i] - p，a[i] + p + 1）会有一个人，配合下面前缀和就知道每个区间多少人
    }
    for (int i = 1; i <= n; i++) d[i] += d[i - 1];
    for (int i = 1; i <= n; i++) ans = max(ans, d[i]);
    printf("%d\n", ans);
    return 0;
}
