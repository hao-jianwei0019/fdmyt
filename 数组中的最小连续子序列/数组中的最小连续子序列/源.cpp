class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * max increasing subsequence
     * @param arr int整型vector the array
     * @return int整型
     */
    int MLS(vector<int>& arr) {
        // write code here
        sort(arr.begin(), arr.end());
        int n = arr.size();
        int dp = 1;
        int ans = 1;
        for (int i = 1; i < n; i++)
        {
            if (arr[i] == arr[i - 1] + 1)
            {
                dp = dp + 1;
                ans = max(ans, dp);
            }
            else if (arr[i] == arr[i - 1])
            {
                continue;
            }
            else dp = 1;
        }
        return ans;
    }
};