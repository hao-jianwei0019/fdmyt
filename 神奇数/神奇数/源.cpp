#include <cmath>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
using namespace std;
bool issu(int k)
{
    for (int i = 2; i <= sqrt(k); i++)
    {
        if (k % i == 0)return false;
    }
    return true;
}
bool f[11][11];
int ans;
bool issq(int i)
{
    vector<int> s;
    while (i)
    {
        s.push_back(i % 10);
        i /= 10;
    }
    int n = s.size();
    for (int i = 0; i < n; i++)
        for (int j = i + 1; j < n; j++)
        {
            if (f[s[i]][s[j]])return true;
        }
    return false;
}
int main() {
    int a, b;
    cin >> a >> b;
    memset(f, false, sizeof f);
    for (int i = 10; i < 100; i++)
    {
        if (issu(i))
        {
            int a = i % 10;
            int b = (i / 10) % 10;
            f[a][b] = f[b][a] = true;
        }
    }
    ans = 0;
    for (int i = a; i <= b; i++)
    {
        if (issq(i))ans++;

    }
    cout << ans;
    return 0;
}
// 64 λ������� printf("%lld")