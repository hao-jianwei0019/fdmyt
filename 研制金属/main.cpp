#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
int N;
int A,B;
int MAX,MIN;
int main()
{
    cin>>N;
    MIN=0,MAX=0x3f3f3f;
    for(int i=0;i<N;i++)
    {
        cin>>A>>B;
        MAX=min(MAX,A/B);
        MIN=max(MIN,A/(B+1)+1);
    }
    cout<<MIN<<" "<<MAX<<endl;
    return 0;
}
