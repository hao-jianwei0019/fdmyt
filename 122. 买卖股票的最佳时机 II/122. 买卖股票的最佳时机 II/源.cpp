class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int l = 0;
        int r = 1;
        int n = prices.size();
        int ans = 0;
        while (r < n)
        {
            ans += (prices[r] - prices[l] > 0) ? prices[r] - prices[l] : 0;
            l++; r++;
        }
        return ans;
    }
};