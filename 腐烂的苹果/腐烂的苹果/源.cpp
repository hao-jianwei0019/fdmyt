#include<iostream>
#include<algorithm>
#include<vector>
#include<cstring>
#include<queue>
using namespace std;
bool vis[1010][1010];
int dx[4] = { -1,0,1,0 };
int dy[4] = { 0,1,0,-1 };
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param grid int整型vector<vector<>>
     * @return int整型
     */

    int rotApple(vector<vector<int> >& grid) {
        // write code here
        int n = grid.size();
        int m = grid[0].size();
        int ans = 0;
        int des = 0;
        queue<pair<int, int>>nums;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
            {
                if (grid[i][j] == 2)
                {
                    nums.push({ i,j });
                    vis[i][j] = true;
                }
                else if (grid[i][j] == 1)
                {
                    des++;
                }
            }
        memset(vis, false, sizeof vis);
        while (des)
        {
            ans++;
            int s = nums.size();
            if (s == 0)return -1;
            while (s--)
            {
                int x = nums.front().first;
                int y = nums.front().second;
                nums.pop();
                for (int i = 0; i < 4; i++)
                {
                    int x1 = x + dx[i], y1 = y + dy[i];
                    if (x1 >= 0 && x1 < n && y1 >= 0 && y1 < m && !vis[x1][y1])
                    {
                        if (grid[x1][y1] == 1)
                        {
                            nums.push({ x1,y1 });
                            des--;
                            vis[x1][y1] = true;
                        }
                    }
                }

            }
        }
        return ans;
    }
}O;
int main()
{
    O.rotApple({ {1, 1, 1},{1, 2, 1},{1, 1, 1} });
    return 0;
}