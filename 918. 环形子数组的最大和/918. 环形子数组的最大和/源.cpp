#include<iostream>
#include<vector>
using namespace std;

class Solution {
public:
    int maxSubarraySumCircular(vector<int> nums) {
        int n = nums.size();
        vector<int> f(n + 1, 0);//���
        auto g = f;//��С
        int fmax = INT_MIN, gmin = INT_MAX, sum = 0;
        for (int i = 1; i <= n; i++)
        {
            int tmp = nums[i - 1];
            f[i] = max(tmp, f[i - 1] + tmp);
            fmax = max(f[i], fmax);
            g[i] = min(tmp, g[i - 1] + tmp);
            gmin = min(gmin, g[i]);
            sum += tmp;
        }
        int ret = sum - gmin;
        if (ret == 0)return fmax;
        return max(fmax, ret);
    }
}o;
int main()
{
    o.maxSubarraySumCircular({ 1,-2,3,-2 });
}