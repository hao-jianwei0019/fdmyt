#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
	int a;
	int count=0;
	scanf("%d", &a);
	while(a != 0)
	{
		if ((a & 1) == 1)
			count++;
		a = a >> 1;
	}
	printf("%d", count);
	return 0;
}