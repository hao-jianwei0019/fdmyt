class Solution {
public:
    int myAtoi(string s) {
        int i = 0;//���Ʊ���
        long sum = 0;
        int op = 1;

        while (i < s.size())
        {
            if (s[i] == '-')
            {
                op *= -1;
                i++;
                break;
            }
            else if (s[i] == '+')
            {
                i++;
                break;
            }
            else  if (s[i] >= '0' && s[i] <= '9')
            {
                break;
            }
            else if (s[i] == ' ')
            {
                i++;
            }
            else {
                return 0;
            }

        }
        while (i < s.size())
        {

            if (s[i] >= '0' && s[i] <= '9')
            {
                sum = sum * 10 + (s[i] - '0');
                if (sum > INT_MAX)
                {
                    if (op == -1)
                        return INT_MIN;
                    return INT_MAX;
                }
            }
            else
            {
                return sum * op;
            }
            i++;

        }
        return sum * op;
    }
};