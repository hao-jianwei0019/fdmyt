#include<iostream>
#include<queue>
#include<cstring>

using namespace std;
int T, M, N;
const int L_MAX = 60;
char island[L_MAX][L_MAX];
bool vis[L_MAX][L_MAX];
int ans;
typedef pair<int, int> PII;
int dxx[] = { -1,-1,0,1,1,1,0,-1 }, dyy[] = { 0,1,1,1,0,-1,-1,-1 };
int dx[] = { -1,0,1,0 }, dy[] = { 0,1,0,-1 };
void bbfs(int x, int y)
{
    ans++;
    queue<PII> q;
    q.push({ x,y });
    vis[x][y] = true;
    while (q.size())
    {
        auto t = q.front();
        q.pop();
        for (int i = 0; i < 4; i++)
        {
            int x1 = t.first + dx[i], y1 = t.second + dy[i];
            if (x1 >= 0 && y1 >= 0 && x1 <= N + 1 && y1 <= M + 1 && !vis[x1][y1] && island[x1][y1] == 1)
            {
                q.push({ x1,y1 });
                vis[x1][y1] = true;
            }
        }
    }
}
void bfs(int x, int y)
{
    queue<PII> q;
    q.push({ x,y });
    vis[x][y] = true;
    while (q.size())
    {
        auto t = q.front();
        q.pop();
        for (int i = 0; i < 8; i++)
        {
            int x1 = t.first + dxx[i], y1 = t.second + dyy[i];
            if (x1 >= 0 && y1 >= 0 && x1 <= N + 1 && y1 <= M + 1 && !vis[x1][y1])
            {
                if (island[x1][y1] == '1')bbfs(x1, y1);
                else q.push({ x1,y1 }), vis[x1][y1] = true;
            }
        }
    }
}

void solve()
{
    cin >> N >> M;//�� ��
    for (int i = 1; i <= N; i++)
        for (int j = 1; j <= M; j++)
        {
            cin >> island[i][j];
        }
    bfs(0, 0);


}
int main()
{
    cin >> T;
    while (T--)
    {
        memset(vis, 0, sizeof vis);
        memset(island, '0', sizeof island);
        ans = 0;
        solve();
        cout << ans << endl;
    }
    return 0;
}