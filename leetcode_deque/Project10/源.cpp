#include <iostream>
#include<string>
#include<vector>
#include<queue>
using namespace std;
typedef long long ll;

const int N = 2e5 + 10;

ll a[N], p, q[N];
deque<int> dq;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0), cout.tie(0);

    int m, p;
    cin >> m >> p;

    char op;
    ll t, ans = 0;
    for (int i = 1; i <= m; i++) {
        cin >> op;
        if (op == 'A') {
            cin >> t;
            a[i] = (t + ans) % p;
            while (!dq.empty() && a[dq.back()] <= a[i]) dq.pop_back();
            dq.push_back(i);
        }
        else {
            int L;
            cin >> L;
            while (!dq.empty() && dq.front() <= i - L) dq.pop_front();
            ans = a[dq.front()];
            cout << ans << "\n";
        }
    }

    return 0;
}