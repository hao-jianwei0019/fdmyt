#include<iostream>
using namespace std;
class Solution {
public:
    string countAndSay(int n) {
        string cur1("1");
        if (n == 1)
            return cur1;
        n--;
        cur1 += '1';
        string cur2;
        while (--n)
        {
            int i = 0;
            while (i < cur1.size())
            {
                int j = 1;
                if (i == cur1.size() - 1)
                {
                    cur2 += j + '0';
                    cur2 += cur1[i];
                    break;
                }
                while (cur1[i] == cur1[i + 1])
                {
                    i++;
                    j++;
                }
                cur2 += j + '0';
                cur2 += cur1[i];
                i++;
            }
            cur1 = cur2;
            cur2.clear();
        }
        return cur1;
    }

};
int main()
{
    Solution s;
   string c= s.countAndSay(4);
   cout << c;

    
}