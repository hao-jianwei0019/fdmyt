#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param s string字符串 第一个整数
     * @param t string字符串 第二个整数
     * @return string字符串
     */
    string solve(string s, string t) {
        if (s == "0" || t == "0")return "0";
        string ans;
        int l = s.size() - 1;
        int r = t.size() - 1;
        int tmp = 0;
        int n = -1;
        int i, j;
        for (i = 0; i <= l; i++)
        {
            int a = s[l - i] - '0';
            for (j = 0; j <= r; j++)
            {
                tmp += (t[r - j] - '0') * a;
                if (n < i + j)//该位置原来没有
                {
                    ans += tmp % 10 + '0';
                    tmp /= 10;
                    n++;
                }
                else
                {
                    tmp += ans[i + j] - '0';
                    ans[i + j] = tmp % 10 + '0';
                    tmp /= 10;
                }
            }
            while (tmp)
            {
                if (n < i + j)//该位置原来没有
                {
                    ans += tmp % 10 + '0';
                    tmp /= 10;
                    n++;
                }
                else
                {
                    tmp += ans[i + j] - '0';
                    ans[i + j] = tmp % 10 + '0';
                    tmp /= 10;
                }
            }
        }

        string ret(ans.rbegin(), ans.rend());
        return ret;
    }
}O;
int main()
{
    O.solve("11", "99");
    return 0;
}