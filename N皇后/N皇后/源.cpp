#include<iostream>
#include<vector>
#include<string>
using namespace std;
class Solution {
public:
vector<vector<string>> solveNQueens(int n) {

    vector<vector<string>> ret;
    if (n == 1) {
        string ans = "Q";
        vector<string> tmp;
        tmp.push_back(ans);
        ret.push_back(tmp);
        return ret;
    }
    if (n < 4)
        return ret;

    string tmp(n, '.');
    int cnt = 0; //当前行
    int col = 0; //当前列
    vector<int> board(n, 0);
    while (1) {
        if (board[0] == n - 1 && (cnt == 1 && col == n - 2))
            break;
        bool unattack = true; //不可攻击
        for (int i = 0; i < cnt; i++) {
            if (col == board[i]) //列判断
            {
                unattack = false;
                break;
            }
            int subcol = col - board[i];
            int subcnt = cnt - i;
            if (subcol == subcnt || subcnt + subcol == 0) {
                unattack = false;
                break;
            }
        }
        if (unattack) //不会攻击
        {
            board[cnt] = col;
            cnt++;
            col = 0;
            if (cnt == n) {
                vector<string> a;

                for (int i = 0; i < n; i++) {
                    string ans = tmp;

                    ans[board[i]] = 'Q';
                    a.push_back(ans);
                }
                ret.push_back(a);
                do {
                    cnt--;
                    col = board[cnt] + 1;
                } while (col >= n);
            }

        }
        else //可攻击
        {
            col++;
            while (col >= n) {
                cnt--;

                col = board[cnt] + 1;
            }
        }
    }
    return ret;
}
}Q;
int main()
{
    vector<vector<string>> ret=Q.solveNQueens(4);
    cout << endl;
    return 0;
}