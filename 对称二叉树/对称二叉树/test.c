#define  _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>

typedef char BTDataType;

typedef struct TreeNode
{
    BTDataType val;
    struct TreeNode* left;
    struct TreeNode* right;
}BTNode;

BTNode* BinaryTreeCreate(BTDataType* a, int* pi)
{
    if (a[*pi] == '#')
    {
        (*pi)++;
        return NULL;
    }

    BTNode* root = (BTNode*)malloc(sizeof(BTNode));
    if (root == NULL)
    {
        perror("malloc");
        return;
    }
    root->val = a[(*pi)++];
    root->left = BinaryTreeCreate(a, pi);
    root->right = BinaryTreeCreate(a, pi);
    return root;

}
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */
bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
    if (p == NULL || q == NULL)
    {
        if (q == p)
            return true;
        else
            return false;

    }
    if (p->val != q->val)
        return false;
    bool ret = isSameTree(p->left, q->right);
    if (ret == false)
        return ret;
    ret = isSameTree(p->right, q->left);
    return ret;

}
bool isSymmetric(struct TreeNode* root) {


    return  isSameTree(root->left, root->right);


}

int main()
{
    char arr[100];
    scanf("%s", arr);
    int i = 0;

    BTNode* root = BinaryTreeCreate(arr, &i);
    printf("%d",isSymmetric(root));
}