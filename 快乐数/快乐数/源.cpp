class Solution {
public:
    int change(int n)
    {
        int tmp = 0;
        while (n)
        {
            tmp += pow(n % 10, 2);
            n /= 10;
        }
        return tmp;
    }

    bool isHappy(int n) {

        int fast = n;
        int slow = n;
        while (1)
        {
            fast = change(fast);
            fast = change(fast);
            slow = change(slow);
            if (fast == slow)
            {
                if (fast == 1)return true;
                else return false;
            }
        }
    }
};