#include <stdio.h>
void  change(int *pa,int  *pb)
{
	int c;
	c = *pa;
	*pa = *pb;
	*pb = c;

}
int main()
{
	int a, b;
	a = 5;
	b = 10;
	printf("%d  %d\n", a, b);
	change(&a, &b);
	printf("%d  %d", a, b);
	return 0;
}