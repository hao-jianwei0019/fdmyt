#include<iostream>
using namespace std;

class Solution {
public:
    int firstUniqChar(string s) {
        int i = 0;
        int arr[26];
        memset(arr, 0, 26*4);
        while (i < s.size())
        {
            arr[s[i] - 'a']++;
            i++;
        }
        i = 0;
        string goal;
        while (i < 26)
        {
            while (arr[i] != 1)
            {
                i++;
                if (i == 26)
                    break;
            }
            if (i == 26)
                break;
            goal += i + 'a';
            i++;
        }
        if (goal.size() == 0)
            return -1;
        int emp = s.find_first_of(goal);
        return emp;
    }
};
int main()
{
    string s("loveleetcode");
    Solution a1;
    int num=a1.firstUniqChar(s);
    cout << num;
}