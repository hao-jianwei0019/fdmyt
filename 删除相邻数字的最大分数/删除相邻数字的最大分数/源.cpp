#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1e4 + 10;

long long dp[N][2];
int vis[N];
int n;
int main() {
    int tmp;
    cin >> n;
    int mx = 0;
    for (int i = 0; i < n; i++)
    {
        cin >> tmp;
        vis[tmp]++;
        mx = max(mx, tmp);
    }
    dp[0][1] = dp[0][0] = 0;
    for (int i = 1; i <= mx; i++)
    {
        dp[i][1] = dp[i - 1][0] + i * vis[i];
        dp[i][0] = max(dp[i - 1][0], dp[i - 1][1]);
    }
    cout << max(dp[mx][0], dp[mx][1]);
    return 0;

}
// 64 λ������� printf("%lld")