#include <iostream>
using namespace std;

int main() {
    string s;
    cin >> s;
    bool vis[12] = { false };
    int n = s.size() - 1;
    int ans = 0;
    for (int i = n; i >= 0; i--)
    {
        int tmp = s[i] - '0';
        if (!vis[tmp])
        {
            ans = ans * 10 + tmp;
            vis[tmp] = true;
        }
    }
    cout << ans << endl;
    return 0;
}
// 64 λ������� printf("%lld")