#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
int n, m, k;
const int mod = 1e9 + 7, N = 110, M = 7, K = 21;
int dp[N][1 << M][1 << M][K];
int get_one(int x)
{
    int y = 0;
    while (x)
    {
        y++;
        x -= (x & (-x));
    }
    return y;
}
int main()
{
    cin >> n >> m >> k;
    dp[0][0][0][0] = 1;
    for (int i = 1; i <= m; i++)
        for (int a = 0; a < 1 << n; a++)
            for (int b = 0; b < 1 << n; b++)
            {
                if (((a << 2) & b) || ((b << 2) & a))continue;
                for (int c = 0; c < 1 << n; c++)
                {
                    if (((c << 2) & b) || ((b << 2) & c))
                        continue;
                    if (((c << 1) & a) || ((a << 1) & c))
                        continue;
                    int t = get_one(c);

                    for (int j = t; j <= k; j++)
                        dp[i][b][c][j] = (dp[i][b][c][j] + dp[i - 1][a][b][j - t]) % mod;
                }
            }
    int res = 0;
    for (int a = 0; a < 1 << n; a++)
        for (int b = 0; b < 1 << n; b++)
            res = (dp[m][a][b][k] + res) % mod;

    cout << res << endl;
    return 0;

}