#include <iostream>
using namespace std;
const int N = 510;
int nums[N];
const int M = 5e4 + 10;
int f[M];
int main() {
    int k = 0;
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++)
    {
        cin >> nums[i];
        k += nums[i];
    }
    if (k % 2 != 0)
    {
        cout << "false";
        return 0;
    }
    k /= 2;
    f[0] = true;

    for (int i = 1; i <= n; i++)
        for (int j = k; j >= 0; j--)
        {
            if (f[j])f[j + nums[i]] = true;
        }
    if (f[k])cout << "true" << endl;
    else cout << "false" << endl;
    return 0;
}
// 64 λ������� printf("%lld")