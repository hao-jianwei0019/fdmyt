/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
bool hasCycle(struct ListNode* head) {
    struct ListNode* fastList = head;
    struct ListNode* shortList = head;
    while (fastList && fastList->next)
    {
        fastList = fastList->next->next;
        shortList = shortList->next;
        if (fastList == shortList)
        {
            return true;

        }
    }
    return false;

}