#include <algorithm>
#include <iostream>
using namespace std;

int main() {
    int T;
    cin >> T;
    while (T--) {
        long long n, a, b;
        cin >> n >> a >> b;
        long long ret = 0;
        if (n <= 2)
        {
            cout << min(a, b) << endl;
            continue;
        }
        if (3 * a < 2 * b) {
            ret += n / 2 * a;
            n %= 2;
            if (n != 0)ret += min(a, min(b, b - a));
        }
        else {
            ret += n / 3 * b;
            n %= 3;
            if (n == 1)ret += min(a, min(a * 2 - b, b));
            if (n == 2)ret += min(a, min(a * 3 - b, b));
        }
        cout << ret << endl;

    }
    return 0;
}
// 64 λ������� printf("%lld")