#include <bits/stdc++.h>

const int mod1 = 998244353, mod2 = 1e9 + 9;
const int N = 1e6 + 5;
void solve() {
	int n; std::cin >> n;
	std::vector<std::vector<int> > a(n, std::vector<int>(n));
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			std::cin >> a[i][j];
		}
	}
	int q; std::cin >> q;
	int now = 0, cur = 1;
	while (q--) {
		int op; std::cin >> op;
		now ^= 1;
		if (op == 1) {
			cur ^= 1;
		}
	}
	if (now) {
		for (int i = n - 1; i >= 0; i--) {
			if (cur) {
				for (int j = 0; j < n; j++) {
					std::cout << a[i][j] << " \n"[j == n - 1];
				}
			}
			else {
				for (int j = n - 1; j >= 0; j--) {
					std::cout << a[i][j] << " \n"[j == 0];
				}
			}
		}
	}
	else {
		for (int i = 0; i < n; i++) {
			if (cur) {
				for (int j = 0; j < n; j++) {
					std::cout << a[i][j] << " \n"[j == n - 1];
				}
			}
			else {
				for (int j = n - 1; j >= 0; j--) {
					std::cout << a[i][j] << " \n"[j == 0];
				}
			}
		}
	}
}

int main() {
	std::ios::sync_with_stdio(false), std::cin.tie(0);
	int T = 1; //std::cin >> T;
	while (T--) {
		solve();
	}
	return 0;
}