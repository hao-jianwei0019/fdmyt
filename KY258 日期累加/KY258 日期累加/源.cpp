#include <iostream>
#include<assert.h>
using namespace std;
class Date {
public:
    friend ostream& operator<<(ostream& out, const Date& d);
    friend istream& operator>>(istream& in, Date& d);
    Date(int a, int b, int c)
    {
        _year = a;
        _month = b;
        _day = c;
    }
    int GetMonthDay(int year, int month)
    {
        static int MonthDay[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
        if (month == 2 && (year % 400 == 0 || year % 4 == 0 && year % 100 != 0))
        {
            return 29;
        }
        return MonthDay[month];
    }

    Date& operator+=(int day)
    {
        _day += day;
        while (_day > GetMonthDay(_year, _month))
        {
            _day -= GetMonthDay(_year, _month);
            ++_month;
            if (_month == 13)
            {
                ++_year;
                _month = 1;
            }
        }
        return *this;
    }

private:
    int _year;
    int _month;
    int _day;
};
ostream& operator<<(ostream& out, const Date& d)
{
    if (d._month >=10)
    {
        if (d._day > 10)
            out << d._year << "-" << d._month << "-" << d._day << endl;
        else
            out << d._year << "-" << d._month << "-0" << d._day << endl;
    }
    else
    {
        if (d._day > 10)
            out << d._year << "-0" << d._month << "-" << d._day << endl;
        else
            out << d._year << "-0" << d._month << "-0" << d._day << endl;
    }

    return out;
}
istream& operator>>(istream& in, Date& d)
{
    int year, month, day;
    in >> year >> month >> day;

    if (month > 0 && month < 13
        && day > 0 && day <= d.GetMonthDay(year, month))
    {
        d._year = year;
        d._month = month;
        d._day = day;
    }
    else
    {
        cout << "非法日期" << endl;
        assert(false);
    }

    return in;
}
int main() {
    int i = 0;
    cin >> i;
    Date d1(0, 0, 0);
    while (i)
    {
        cin >> d1;
        int n = 0;
        cin >> n;
        d1 += n;
        cout << d1;
        i--;
    }
}
// 64 位输出请用 printf("%lld")