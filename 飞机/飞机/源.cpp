#include <iostream>
#include <vector>
#include <string>

// 航班信息结构体
struct Flight {
    std::string flightNumber;
    std::string departureTime;
    std::string arrivalTime;
    std::string departureCity;
    std::string arrivalCity;
    float ticketPrice;
    float ticketDiscount;
    int availableSeats;
};

// 订单结构体
struct Order {
    std::string orderNumber;
    std::string customerName;
    std::string flightNumber;
};

// 全局变量，存储航班信息和订单信息
std::vector<Flight> flights;
std::vector<Order> orders;

// 添加航班信息
void addFlight() {
    Flight newFlight;

    std::cout << "输入航班号: ";
    std::cin >> newFlight.flightNumber;

    std::cout << "输入起飞时间: ";
    std::cin >> newFlight.departureTime;

    std::cout << "输入到达时间: ";
    std::cin >> newFlight.arrivalTime;

    std::cout << "输入起飞城市: ";
    std::cin >> newFlight.departureCity;

    std::cout << "输入到达城市: ";
    std::cin >> newFlight.arrivalCity;

    std::cout << "输入票价: ";
    std::cin >> newFlight.ticketPrice;

    std::cout << "输入票价折扣: ";
    std::cin >> newFlight.ticketDiscount;

    std::cout << "输入可用座位数: ";
    std::cin >> newFlight.availableSeats;

    flights.push_back(newFlight);

    std::cout << "航班添加成功。\n";
}


// 查询航班信息
void searchFlight() {
    std::string flightNumber;
    std::cout << "输入航班号: ";
    std::cin >> flightNumber;

    for (auto& flight : flights) {
        if (flight.flightNumber == flightNumber) {
            std::cout << "航班号: " << flight.flightNumber << "\n";
            std::cout << "起飞时间: " << flight.departureTime << "\n";
            std::cout << "到达时间: " << flight.arrivalTime << "\n";
            std::cout << "起飞城市: " << flight.departureCity << "\n";
            std::cout << "到达城市: " << flight.arrivalCity << "\n";
            std::cout << "票价: " << flight.ticketPrice << "\n";
            std::cout << "票价折扣: " << flight.ticketDiscount << "\n";
            std::cout << "可用座位数: " << flight.availableSeats << "\n";
            return;
        }
    }

    std::cout << "未找到航班。\n";
}

// 订票
void bookTicket() {
    Order newOrder;

    std::cout << "输入乘客姓名: ";
    std::cin >> newOrder.customerName;

    std::cout << "输入航班号: ";
    std::cin >> newOrder.flightNumber;

    // 查找航班
    Flight* selectedFlight = nullptr;
    for (auto& flight : flights) {
        if (flight.flightNumber == newOrder.flightNumber) {
            selectedFlight = &flight;
            break;
        }
    }

    if (selectedFlight == nullptr) {
        std::cout << "未找到航班。\n";
        return;
    }

    // 显示航班信息
    std::cout << "航班信息:\n";
    std::cout << "起飞时间: " << selectedFlight->departureTime << "\n";
    std::cout << "到达时间: " << selectedFlight->arrivalTime << "\n";
    std::cout << "起飞城市: " << selectedFlight->departureCity << "\n";
    std::cout << "到达城市: " << selectedFlight->arrivalCity << "\n";
    std::cout << "票价: " << selectedFlight->ticketPrice << "\n";
    std::cout << "票价折扣: " << selectedFlight->ticketDiscount << "\n";
    std::cout << "可用座位数: " << selectedFlight->availableSeats << "\n";

    // TODO: 检查航班是否有可用座位，更新航班信息

    // 生成订单号，可以根据需要自行设计
    newOrder.orderNumber = "ORD123";

    orders.push_back(newOrder);

    std::cout << "订票成功。订单号: " << newOrder.orderNumber << "\n";
}

// 退票
void cancelTicket() {
    std::string orderNumber;
    std::cout << "输入订单号: ";
    std::cin >> orderNumber;

    for (auto& order : orders) {
        if (order.orderNumber == orderNumber) {
            // TODO: 更新航班可用座位数

            // 标记订单为退票状态
            // 可以使用额外的字段或枚举类型来表示订单状态
            order.orderNumber += " (已取消)";
            std::cout << "退票成功。\n";
            return;
        }
    }

    std::cout << "未找到订单。\n";
}

int main() {
    int choice;

    do {
        std::cout << "========== 飞机订票系统 ==========\n";
        std::cout << "1. 添加航班\n";
        std::cout << "2. 查询航班\n";
        std::cout << "3. 订票\n";
        std::cout << "4. 退票\n";
        std::cout << "5. 退出\n";
        std::cout << "请输入选择: ";
        std::cin >> choice;

        switch (choice) {
        case 1:
            addFlight();
            break;
        case 2:
            searchFlight();
            break;
        case 3:
            bookTicket();
            break;
        case 4:
            cancelTicket();
            break;
        case 5:
            std::cout << "正在退出...\n";
            break;
        default:
            std::cout << "无效的选择，请重试。\n";
        }

        std::cout << "=================================\n\n";
    } while (choice != 5);

    return 0;
}
