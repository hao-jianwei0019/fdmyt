#include<algorithm>
#include<iostream>
#include<vector>
#include<string>
using namespace std;
int N;
int main()
{
    vector<int> last(10, 0);
    cin >> N;
    int ans = 0;
    for (int i = 0; i < N; i++)
    {
        string tmp;
        cin >> tmp;
        int a = tmp[0] - '0';
        int b = tmp[tmp.size() - 1] - '0';
        last[b] = max(last[b], last[a] + 1);
        ans = max(last[b], ans);
    }
    cout << N - ans;

}