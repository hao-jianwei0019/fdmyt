#include <iostream>
#include <vector>
#include <string>
#include <deque>
using namespace std;
class Soultion
{
public:
    void inst(vector<vector<int>>& board)
    {
        n = board.size();
        m = board[0].size();
        vector<vector<bool>>vired(n, vector<bool>(m, false));//表示去过的   
        vired[0][0] = true;
        deque<string>bp;//探索过程
        bp.push_back("(0,0)");
        route(board, vired, bp, 0, 0);

        while (!ans.empty())
        {
            cout << ans.front() << endl;
            ans.pop_front();
        }

    }
    string adds(int x, int y)
    {
        string s;
        s += "(";
        s += x - '0';
        s += ",";
        s += y - '0';
        s += "0";
        return s;

    }
    void route(vector<vector<int>>& board, vector<vector<bool>>& vired, deque<string>& bp, int x, int y)
    {
        if (x == n - 1 && y == m - 1)//找到终点
        {
            if (ans.empty() || bp.size() < ans.size())//结果为空
                ans = bp;
            return;
        }
        for (int i = 0; i < 4; i++)
        {
            int nx = x + dir[i][0];
            int ny = y + dir[i][1];
            if (nx >= n || ny >= m || nx < 0 || ny < 0 || vired[nx][ny] || board[nx][ny])//越界，去过，不能走
                continue;
            vired[nx][ny] = true;
            //加入坐标
      
            char tmp[64] = { 0 };
            sprintf(tmp, "(%d,%d)", nx, ny);
            bp.push_back(string(tmp));//加入新坐标
            route(board, vired, bp, nx, ny);
            bp.pop_back();
        }
    }


private:
    int m;
    int n;
    int dir[4][2] = { {0,1},{1,0},{0,-1},{-1,0} };//对应下，右，左，上
    deque<string> ans;//最终答案
};
int main() {
    int n, m;
    cin >> n >> m;
    vector<vector<int>>board(n, vector<int>(m));
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            cin >> board[i][j];
    Soultion s;

    s.inst(board);

    return 0;



}
// 64 位输出请用 printf("%lld")