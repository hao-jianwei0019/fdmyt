class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        vector<int>tmp = nums1;
        int l = 0;
        int r = 0;

        while (l < m && r < n)
        {
            if (tmp[l] <= nums2[r])nums1[l + r] = tmp[l], l++;
            else nums1[l + r] = nums2[r], r++;
        }
        while (l < m)nums1[l + r] = tmp[l], l++;
        while (r < n)nums1[l + r] = nums2[r], r++;

    }
};