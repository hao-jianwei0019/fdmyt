/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 *	ListNode(int x) : val(x), next(nullptr) {}
 * };
 */
#include <algorithm>
#include <vector>
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param lists ListNode类vector
     * @return ListNode类
     */
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        // write code here
        vector<int>nums;
        for (int i = 0; i < lists.size(); i++)
        {
            ListNode* tmp = lists[i];
            while (tmp != nullptr)
            {
                nums.push_back(tmp->val);
                tmp = tmp->next;
            }
        }
        int n = nums.size();
        sort(nums.begin(), nums.end());
        ListNode* ret = nullptr;
        for (int i = n - 1; i >= 0; i--)
        {
            ListNode* tmp = new ListNode(nums[i]);
            tmp->next = ret;
            ret = tmp;
        }
        return ret;
    }
};