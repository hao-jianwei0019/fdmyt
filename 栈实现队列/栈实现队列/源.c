typedef int  STDataType;


typedef struct Stack
{
    STDataType* a;
    int top;
    int capacity;
}ST;
void STInit(ST* ps);
void STDestroy(ST* ps);
void STPush(ST* ps, STDataType x);
void STPop(ST* ps);
int STSize(ST* ps);
bool STEmpty(ST* ps);
STDataType STTop(ST* ps);



void STInit(ST* ps)
{
    assert(ps);
    ps->a = (STDataType*)malloc(sizeof(STDataType) * 4);
    if (ps->a == NULL)
    {
        perror("malloc");
        return;
    }
    ps->capacity = 4;
    ps->top = 0;//记录栈顶元素的下一个


}
void STDestroy(ST* ps)
{
    assert(ps);
    free(ps->a);
    ps->a = NULL;
    ps->capacity = ps->top = 0;
}
void STPush(ST* ps, STDataType x)
{


    assert(ps);
    //扩容
    if (ps->capacity == ps->top)
    {
        STDataType* tmp = (STDataType*)realloc(ps->a, sizeof(STDataType) * ps->capacity * 2);
        if (ps->a == NULL)
        {
            perror("realloc");
            return;
        }
        ps->capacity *= 2;
        ps->a = tmp;
    }


    (ps->a)[ps->top] = x;
    ps->top++;


}
void STPop(ST* ps)
{
    assert(ps);
    if (!STEmpty(ps))
        ps->top--;


}
int STSize(ST* ps)
{
    assert(ps);
    return ps->top;
}
bool STEmpty(ST* ps)
{
    assert(ps);
    return ps->top == 0;
}
STDataType STTop(ST* ps)
{
    assert(ps);
    assert(!STEmpty(ps));


    return ps->a[ps->top - 1];
}
typedef struct {
    ST reception;
    ST out;


} MyQueue;



MyQueue* myQueueCreate()
{
    MyQueue* obj = (MyQueue*)malloc(sizeof(MyQueue));


    STInit(&obj->reception);
    STInit(&obj->out);
    return obj;
}


void myQueuePush(MyQueue* obj, int x)
{
    STPush(&obj->reception, x);


}


int myQueuePop(MyQueue* obj)
{
    if (STEmpty(&obj->out))
    {
        while (!STEmpty(&obj->reception))
        {
            STPush(&obj->out, STTop(&obj->reception));
            STPop(&obj->reception);
        }
    }
    STDataType tmp = STTop(&obj->out);
    STPop(&obj->out);
    return tmp;
}


int myQueuePeek(MyQueue* obj) {
    if (STEmpty(&obj->out))
    {
        while (!STEmpty(&obj->reception))
        {
            STPush(&obj->out, STTop(&obj->reception));
            STPop(&obj->reception);
        }
    }
    STDataType tmp = STTop(&obj->out);
    return tmp;
}


bool myQueueEmpty(MyQueue* obj) {
    return STEmpty(&obj->out) && STEmpty(&obj->reception);


}


void myQueueFree(MyQueue* obj) {
    STDestroy(&obj->out);
    STDestroy(&obj->reception);


}


/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);

 * int param_2 = myQueuePop(obj);

 * int param_3 = myQueuePeek(obj);

 * bool param_4 = myQueueEmpty(obj);

 * myQueueFree(obj);
*/
