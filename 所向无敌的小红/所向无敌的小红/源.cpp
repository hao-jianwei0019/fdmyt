#include <algorithm>
#include <iostream>
using namespace std;

int main() {
    int k1, k2;
    int hp1, hp2;
    cin >> k1 >> hp1 >> k2 >> hp2;
    long long ans = 0;
    long long k3 = k1 + k2;
    int num = min(hp1 / k2, hp2 / k1);
    ans += k3 * num;
    hp1 -= num * k2;
    hp2 -= num * k1;
    while (hp1 > 0 && hp2 > 0)
    {
        ans += k1 + k2;
        hp1 -= k2;
        hp2 -= k1;
    }
    if (hp1 > 0)ans += k1 * 10;
    if (hp2 > 0)ans += k2 * 10;
    cout << ans;
    return 0;
}
// 64 λ������� printf("%lld")