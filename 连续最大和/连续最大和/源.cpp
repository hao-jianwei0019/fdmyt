#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<int> nums;
    int n;
    cin >> n;

    for (int i = 0; i < n; i++) {
        int tmp;
        cin >> tmp;
        nums.push_back(tmp);

    }
    vector<int>dp1(n, 0);
    vector<int>dp2(n, 0);
    dp1[0] = nums[0];

    int max = nums[0];
    for (int i = 1; i < n; i++)
    {
        dp1[i] = std::max(dp1[i - 1] + nums[i], nums[i]);
        dp2[i] = std::max(dp1[i - 1], dp2[i - 1]);
        max = std::max(max, nums[i]);
    }
    if (max < 0)cout << max;
    else cout << std::max(dp1[n - 1], dp2[n - 1]);
    return 0;

}