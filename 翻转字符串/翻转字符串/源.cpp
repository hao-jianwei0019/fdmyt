#include <string>
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 旋转字符串
     * @param A string字符串
     * @param B string字符串
     * @return bool布尔型
     */
    bool solve(string A, string B) {
        // write code here
        if (A.size() != B.size())return false;
        A += A;
        int n = B.size();
        for (int i = 0; i < n; i++)
        {
            string tmp(A.begin() + i, A.begin() + n + i);
            if (tmp == B)
            {
                return true;
            }
        }
        return false;
    }
};