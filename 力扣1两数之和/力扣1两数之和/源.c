/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* twoSum(int* nums, int numsSize, int target, int* returnSize) {
    int key1 = 0;
    int key2 = 1;
    int* key = (int*)malloc(sizeof(int) * 2);
    *returnSize = 2;
    while (key1 < numsSize)
    {
        key2 = key1 + 1;
        while (key2 < numsSize)
        {
            if (nums[key1] + nums[key2] == target)
            {
                key[0] = key1;
                key[1] = key2;
                return key;
            }
            key2++;
        }
        key1++;
    }
    return NULL;
}