#include <functional>
#include <iostream>
using namespace std;

class Date {
public:
    Date(int a, int b, int c)
    {
        _year = a;
        _mounth = b;
        _day = c;
    }
    int GetMonthDay(int year, int month)
    {
        static int MonthDay[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
        if (month == 2 && (year % 400 == 0 || year % 4 == 0 && year % 100 != 0))
        {
            return 29;
        }
        return MonthDay[month];
    }
    int GetDays()
    {
        int i = 1;
        int sum = 0;
        while (i < _mounth)
        {
            sum += GetMonthDay(_year, i);
            i++;
        }
        sum += _day;
        return sum;
    }

private:
    int _year;
    int _mounth;
    int _day;
};
int main() {
    int a, b, c;
    cin >> a >> b >> c;
    Date d1(a, b, c);
    cout << d1.GetDays();
    return 0;
}