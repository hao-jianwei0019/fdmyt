#include <cstring>
#include <iostream>
#include <queue>
#include <utility>
using namespace std;
int dx[4] = { -1,0,1,0 };
int dy[4] = { 0,1,0,-1 };
int mins, ans;
const int N = 35;
char board[N][N];
bool vis[N][N];

int n, l;
int x, y;
void bfs()
{

    queue<pair<int, int>>m;
    m.push({ x,y });
    memset(vis, false, sizeof vis);
    vis[x][y] = true;
    int bushu = 0;
    while (!m.empty())
    {
        bushu++;
        int count = m.size();
        while (count--)
        {
            int x1 = m.front().first, y1 = m.front().second;
            m.pop();
            for (int i = 0; i < 4; i++)
            {
                int x2 = x1 + dx[i];
                int y2 = y1 + dy[i];
                if (x2 > 0 && x2 <= n && y2 > 0 && y2 <= l && vis[x2][y2] == false)
                {
                    if (board[x2][y2] == 'e')
                    {
                        ans++;
                        mins = min(bushu, mins);
                    }
                    else if (board[x2][y2] == '*')
                    {
                        vis[x2][y2] = true;
                    }
                    else if (board[x2][y2] == '.')
                    {
                        vis[x2][y2] = true;
                        m.push({ x2,y2 });
                    }
                }
            }
        }
    }
}
int main() {
    mins = 100;
    ans = 0;
    cin >> n >> l;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= l; j++)
        {
            cin >> board[i][j];
            if (board[i][j] == 'k')x = i, y = j;
        }
    bfs();
    cout << ans << " " << mins << endl;;
    return 0;
}
// 64 λ������� printf("%lld")