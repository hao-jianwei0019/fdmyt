#include <stdio.h>
#include <assert.h>
char* my_strcat(char* e1,const char* e2,size_t n)
{
	char* start = e1;
	assert(e1 && e2);
	while (*e1 != '\0')
	{
		e1++;
	}
	int i;
	for(i=0;i<n;i++)
	{
		if (*e2 == '\0')
		{
			//break;
			*e1 = *e2;
			return start;
		}
		*e1++ = *e2++;

	}
	*e1 = '\0';
	return start;
}
int main()
{
	char arr[20] = "hello ";
	char arr1[] = "world";
	my_strcat(arr, arr1,3);
	printf("%s", arr);
	return 0;
}