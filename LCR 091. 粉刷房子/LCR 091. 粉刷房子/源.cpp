class Solution {
public:
    int minCost(vector<vector<int>>& costs) {
        int n = costs.size();
        vector<int> dpred(n, 0);
        auto dpblue = dpred;
        auto dpgreen = dpred;
        dpred[0] = costs[0][0];
        dpblue[0] = costs[0][1];
        dpgreen[0] = costs[0][2];
        for (int i = 1; i < n; i++)
        {
            dpred[i] = costs[i][0] + min(dpblue[i - 1], dpgreen[i - 1]);
            dpblue[i] = costs[i][1] + min(dpred[i - 1], dpgreen[i - 1]);
            dpgreen[i] = costs[i][2] + min(dpblue[i - 1], dpred[i - 1]);
        }
        int rul = dpred[n - 1];
        if (rul > dpblue[n - 1])rul = dpblue[n - 1];
        if (rul > dpgreen[n - 1])rul = dpgreen[n - 1];
        return rul;

    }
};
