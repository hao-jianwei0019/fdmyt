int missingNumber(int* nums, int numsSize) {
    int sum = 0;
    int i;
    for (i = 0; i < numsSize; i++)
    {
        sum ^= *(nums + i);
    }
    for (i = 0; i < 1 + numsSize; i++)
    {
        sum ^= i;
    }
    return sum;
}