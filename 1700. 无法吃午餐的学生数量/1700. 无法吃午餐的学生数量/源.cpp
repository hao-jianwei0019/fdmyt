class Solution {

public:
    int countStudents(vector<int>& students, vector<int>& sandwiches) {

        int num1 = 0;
        int num0 = 0;
        for (auto& ch : students)
        {
            if (ch == 1)num1++;
            else num0++;
        }
        for (auto& ch : sandwiches)
        {
            if (ch == 1)
            {
                if (num1 > 0)num1--;
                else break;
            }
            else
            {
                if (num0 > 0)num0--;
                else break;
            }
        }
        return num1 + num0;
    }
};
};