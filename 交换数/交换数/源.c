#define  _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
    int a, b;
    scanf("a=%d,b=%d", &a, &b);
    int t = a;
    a = b;
    b = t;
    printf("a=%d,b=%d\n", a, b);
    return 0;
}