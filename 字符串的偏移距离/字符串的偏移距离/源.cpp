#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main() {
    string s1;
    string s2;
    cin >> s1 >> s2;
    int n = s1.size(), m = s2.size();
    vector<vector<int>>dp(n + 1, vector<int>(m + 1));

    for (int i = 1; i <= n; i++)dp[i][0] = dp[i - 1][0] + 1;
    for (int j = 1; j <= m; j++)dp[0][j] = dp[0][j - 1] + 1;


    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
        {

            if (s1[i - 1] == s2[j - 1])dp[i][j] = dp[i - 1][j - 1];
            else
                dp[i][j] = min(min(dp[i - 1][j], dp[i][j - 1]), dp[i - 1][j - 1]) + 1;
        }


    cout << dp[n][m] << endl;
    return 0;
}
// 64 λ������� printf("%lld")