#include <stdio.h>
void Sort(int arr[], int sz)
{
	//趟数
	int i = 0;
	for (i = 0; i < sz - 1; i++)
	{
		//一趟冒泡排序,决定了一趟排序进行多少对比较
		int j = 0;
		for (j = 0; j < sz-1-i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
}
int main()
{
	//整型数据
	int arr[] = { 5,7,9,5,1,2,0,3,6,4 };
	//写一个函数对数组进行排序
	int sz = sizeof(arr) / sizeof(arr[0]);

	Sort(arr, sz);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);//打印数组
	}

	return 0;
}

