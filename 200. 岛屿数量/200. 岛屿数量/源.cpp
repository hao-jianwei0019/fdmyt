class Solution {
public:
    vector<vector<char>> _grid;
    int vis[305][305];
    int _x[4] = { 0, 0, -1, 1 }; // ��������
    int _y[4] = { -1, 1, 0, 0 };
    int n, m;
    int ans;
    void bfs(int i, int j) {
        if (_grid[i][j] == '0' || vis[i][j] == 1)
            return;
        int x, y;
        vis[i][j] = 1;
        for (int a = 0; a < 4; a++) {
            x = i + _x[a];
            y = j + _y[a];
            if (x >= 0 && x < m && y >= 0 && y < n) {
                if (_grid[x][y] == '1')bfs(x, y);
            }
        }
    }
    int numIslands(vector<vector<char>>& grid) {
        _grid = grid;
        m = grid.size();
        n = grid[0].size();
        ans = 0;
        memset(vis, 0, sizeof vis);
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
            {
                if (grid[i][j] == '1' && vis[i][j] == 0)
                {
                    ans++;
                    bfs(i, j);
                }
            }
        return ans;
    }
};