#include <stdio.h>
#include <assert.h>
struct A
{
	char b;
	int _a : 2;
	int _b : 5;
	int _c : 10;
	int _d : 30;
};
//一个例子
struct S
{
	char a:3;
	char b : 4;
	char c : 5;
	char d : 4;
}s;
s = { 0 };

//空间是如何开辟的？
int main()
{
	s.a = 10;
	s.b = 12;
	s.c = 3;
	s.d = 4;
	
	printf("%d", sizeof(struct A));
}