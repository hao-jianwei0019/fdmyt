#include <iostream>
#include <string>
#include<cstring>
using namespace std;

int main() {
    string s;
    cin >> s;
    int n = s.size();
    int l = 0;
    int r = 0;
    int hash[27];
    memset(hash, 0, sizeof hash);
    int count = 0;
    int ans = 0;
    while (r < n)
    {
        while (r < n && ((hash[s[r] - 'a'] == 0 && count < 2) || (hash[s[r] - 'a'] != 0 && count <= 2)))//窗口扩大
        {
            if (hash[s[r] - 'a'] == 0)count++;
            hash[s[r] - 'a']++;
            r++;
        }
        ans = max(ans, r - l);
        if (r >= n)break;
        while (l < r && count == 2)
        {
            if (hash[s[l] - 'a']-- == 1)count--;
            l++;
        }
    }
    cout << ans << endl;
    return 0;
}
// 64 位输出请用 printf("%lld")