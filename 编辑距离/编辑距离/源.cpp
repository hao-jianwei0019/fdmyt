#include <algorithm>
#include <iostream>
using namespace std;
const int N = 1010;
int dp[N][N];
int main() {
    string a, b;
    cin >> a >> b;
    int n = a.size();
    int l = b.size();
    for (int i = 0; i <= n; i++)
    {
        dp[i][0] = i;

    }
    for (int i = 0; i <= l; i++)
    {
        dp[0][i] = i;

    }
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= l; j++)
        {
            if (a[i - 1] == b[j - 1])dp[i][j] = dp[i - 1][j - 1];
            else
            {
                dp[i][j] = min(min(dp[i - 1][j - 1] + 1, dp[i - 1][j] + 1), dp[i][j - 1] + 1);
            }
        }
    cout << dp[n][l] << endl;
    return 0;
}
// 64 λ������� printf("%lld")