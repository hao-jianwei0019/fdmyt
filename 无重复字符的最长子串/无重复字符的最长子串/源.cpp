#include<iostream>
#include<vector>
#include<string>
using namespace std;
class Solution {
public:
    int lengthOfLongestSubstring(const string s) {
        vector<int> nums(130, 0);
        int l = 0, r = 0;
        int ans = 0;
        int n = s.size();
        while (r < n)
        {
            while (nums[s[r]] ==0&&r<n)
            {
                nums[s[r]]++;
                r++;
            }
            ans = max(ans, r - l);
            while (nums[s[r]] !=0)
            {
                nums[s[l]]--;
                l++;
            }
        }
        return ans;
    }
}O;
int main()
{
    O.lengthOfLongestSubstring("bbbbbb");
    return 0;

}