#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
using namespace std;
const int N = 1010;
int f[N][N];
int main() {
    string s;
    cin >> s;
    int ans = 0;
    int n = s.size();
    memset(f, 0, sizeof f);
    for (int i = n - 1; i >= 0; i--)
        for (int j = i; j < n; j++)
        {
            if (i == j)f[i][j] = 1;
            else
            {
                if (s[i] == s[j])
                {
                    f[i][j] = f[i + 1][j - 1] + 2;
                }
                else {
                    f[i][j] = max(f[i + 1][j], f[i][j - 1]);
                }
            }

        }
    cout << f[0][n - 1];
    return 0;
}
// 64 λ������� printf("%lld")