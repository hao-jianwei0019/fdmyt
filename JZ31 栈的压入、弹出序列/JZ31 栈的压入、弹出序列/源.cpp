#include<iostream>
#include<vector>
#include<stack>
#include<queue>
using namespace  std;
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param pushV int整型vector
     * @param popV int整型vector
     * @return bool布尔型
     */
    bool IsPopOrder(vector<int> pushV, vector<int> popV) {
        // write code here
        int n = pushV.size();
        vector<int >tmp;

        int j = 0;
        int i = 0;
        while (i < n)
        {
            while (tmp.empty() || j < n && popV[i] != *tmp.rbegin())
            {
                tmp.push_back(pushV[j]);
                j++;
            }
            if (popV[i] == *tmp.rbegin())
            {
                tmp.pop_back();
                i++;
            }
            else return false;
        }
        return true;

    }
};
