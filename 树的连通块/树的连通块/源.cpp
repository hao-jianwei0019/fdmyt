#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

vector<vector<int>> l;
vector<bool> vis;
unordered_map<int, int> map;
long long ret;

int dfs(int pos) {
    vector<int>& childs = l[pos];
    vis[pos] = true;//��������
    int tmp = 1;
    for (int child : childs) {
        if (!vis[child]) {
            tmp += dfs(child);
            if (child % 2 == pos % 2) {
                tmp -= 1;
            }
        }
    }
    ret += tmp;
    return tmp;
}

int main() {
    int n, root;
    cin >> n >> root;

    vis.resize(n + 1, false);
    ret = 0;
    l.resize(n + 1);

    //for (int i = 0; i <= n; i++) {
    //    vector<int> v;
    //    l[i] = v;
    //}

    for (int i = 0; i < n - 1; i++) {
        int a, b;
        cin >> a >> b;
        l[a].push_back(b);
        l[b].push_back(a);
    }

    dfs(root);
    cout << ret << endl;
    return 0;
}

