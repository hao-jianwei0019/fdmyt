#include <memory>
const int N = 20100;
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param numProject int整型
     * @param groups int整型vector<vector<>>
     * @return int整型vector
     */
    int h[N], e[N], ne[N], idx, in[N];

    // 添加一条边a->b
    void add(int a, int b) {
        e[idx] = b, ne[idx] = h[a], h[a] = idx++;
    }
    vector<int> findOrder(int numProject, vector<vector<int> >& groups) {
        // write code here
        idx = 0;
        memset(h, -1, sizeof h);
        memset(in, 0, sizeof in);

        for (int i = 0; i < groups.size(); i++)
        {
            add(groups[i][1], groups[i][0]);//0受制于1
            in[groups[i][0]]++;
        }
        vector<int >ret;
        queue<int >q;
        for (int i = 0; i < numProject; i++)
        {
            if (in[i] == 0)
            {
                q.push(i);
            }
        }
        while (!q.empty())
        {
            int a = q.front();
            q.pop();
            ret.push_back(a);
            while (h[a] != -1)
            {
                in[e[h[a]]]--;
                if (in[e[h[a]]] == 0)q.push(e[h[a]]);
                h[a] = ne[h[a]];
            }
        }
        if (ret.size() == numProject)return ret;

        else return {};

    }
};