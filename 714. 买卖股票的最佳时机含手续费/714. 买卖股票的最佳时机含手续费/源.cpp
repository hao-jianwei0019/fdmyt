#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    int maxProfit(vector<int>& prices, int fee) {
        int n = prices.size();
        vector<vector<int>>dp(2, vector<int>(n, 0));
        dp[0][0] = -prices[0];
        for (int i = 1; i < n; i++)
        {
            dp[0][i] = max(dp[1][i - 1] - prices[i], dp[0][i - 1]);
            dp[1][i] = max(prices[i] +dp[0][i - 1] - fee, dp[1][i - 1]);

        }
        return dp[1][n - 1];
    }
}o;
int main()
{
    vector<int> p({ 1,3,2,8,4,9 });
    o.maxProfit(p,2);
}