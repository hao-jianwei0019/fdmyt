#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 计算两个数之和
     * @param s string字符串 表示第一个整数
     * @param t string字符串 表示第二个整数
     * @return string字符串
     */
    string solve(string s, string t) {
        // write code here
        string ans;
        int l = s.size() - 1;
        int r = t.size() - 1;
        int n = min(l, r);
        int tmp = 0;
        int i;
        for (i = 0; i <= n; i++)
        {
            tmp += s[l - i] - '0' + t[r - i] - '0';
            ans += tmp % 10 + '0';
            tmp /= 10;
        }
        while (i <= l)
        {
            tmp += s[l - i] - '0';
            ans += tmp % 10 + '0';
            tmp /= 10;
            i++;
        }
        while (i <= r)
        {
            tmp += t[r - i] - '0';
            ans += tmp % 10 + '0';
            tmp /= 10;
            i++;
        }
        while (tmp)
        {
            ans += tmp % 10 + '0';
            tmp /= 10;
        }
        string ret(ans.rbegin(), ans.rend());
        return ret;
    }
}O;
int main()
{
    O.solve("1", "99");
    return 0;
}