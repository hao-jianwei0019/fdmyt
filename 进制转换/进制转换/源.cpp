#include <iostream>
#include <string>
using namespace std;

int main() {
    int a, b;
    cin >> a >> b;
    string ret;
    int res = 1;
    if (a < 0)
    {
        res = -1;
        a *= -1;
    }
    while (a >= b)
    {
        int tmp = a % b;
        if (tmp >= 10)
        {
            ret += (tmp - 10 + 'A');
        }
        else {
            ret += (tmp + '0');
        }
        a -= tmp;
        a /= b;

    }
    if (a >= 10)ret += (a - 10 + 'A');
    else ret += (a + '0');
    if (res == -1)cout << '-';
    for (int i = ret.size() - 1; i >= 0; i--)
    {
        cout << ret[i];
    }
    return 0;
}
// 64 λ������� printf("%lld")