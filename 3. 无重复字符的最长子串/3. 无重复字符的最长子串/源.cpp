class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int n = s.size();
        int l = 0;
        int r = 0;
        int ans = 0;
        int hash[180];

        while (r < n)
        {
            while (hash[s[r]] < 1 && r < n)
            {
                hash[s[r]]++;
                r++;

            }
            hash[s[r]]++;
            ans = max(r - l, ans);
            while (hash[s[r]] > 1 && l < r)
            {
                hash[s[l]]--;
                l++;
            }
            r++;
            //cout<<r<<" "<<l<<endl;

        }
        return ans;
    }
};