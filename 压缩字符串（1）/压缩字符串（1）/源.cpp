class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param param string字符串
     * @return string字符串
     */
    string compressString(string param) {
        // write code here
        int i = 0;
        int n = param.size();
        param += ' ';
        string ret;
        while (i < n)
        {
            int k = 1;
            char tmp = param[i];
            while (tmp == param[i + 1])
            {
                k++;
                i++;
            }
            ret += tmp;
            if (k != 1)
            {
                string tmp1;
                while (k)
                {
                    tmp1 += k % 10 + '0';
                    k /= 10;
                }
                string tmp2(tmp1.rbegin(), tmp1.rend());
                ret += tmp2;
            }
            i++;
        }
        return ret;
    }
};