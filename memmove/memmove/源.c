#include <stdio.h>
#include <assert.h>
void* my_memmove(void* e1,const void* e2, size_t num)
{
	void* ret = e1;
	assert(e1 && e2);
	while (num--)
	{
		if (e1 < e2)
		{
			*(char*)e1 = *(char*)e2;
			e1 = (char*)e1+1;
			e2= (char*)e2+1;
		}
		else
		{
			*((char*)e1 + num) = *((char*)e2 + num);
		}
	}
}
int main()
{
	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
	my_memmove(arr1+2, arr1, 20);
	return 0;
}