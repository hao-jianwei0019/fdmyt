#include <cstring>
#include <iostream>
#include <string>
#include <vector>
using namespace std;
bool ans[128];

bool pand(string l, string r)
{
    memset(ans, false, sizeof ans);
    for (int i = 0; i < l.size(); i++)
    {
        ans[l[i]] = true;
    }
    for (int i = 0; i < r.size(); i++)
    {
        if (ans[r[i]])return true;
    }
    return false;
}
int main() {

    int t;
    cin >> t;
    while (t--)
    {
        int n;
        cin >> n;
        if (n == 0)
        {
            cout << "No" << endl;
            break;
        }
        vector<string>s(n);
        for (int i = 0; i < n; i++)
        {
            cin >> s[i];
        }
        int l = 0, r = n - 1;
        bool fg = true;
        while (l < n / 2)
        {
            if (pand(s[l], s[r]))
            {
                l++; r--;
            }
            else
            {
                fg = false;
                break;
            }
        }
        if (fg)cout << "Yes" << endl;
        else cout << "No" << endl;
    }
    return 0;

}
// 64 λ������� printf("%lld")