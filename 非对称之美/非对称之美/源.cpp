#include <algorithm>
#include <iostream>
using namespace std;

int main()
{
    string str;
    cin >> str;

    for (int i = str.length() / 2 - (str.length() % 2); i >= 0; i--)
    {
        if (str[i] != str[str.length() - i - 1])
        {
            cout << str.length() << endl;
            return 0;
        }
    }
    for (int i = 1; i < str.length(); i++)
    {
        if (str[i] != str[i - 1])
        {
            cout << str.length() - 1 << endl;
            return 0;
        }
    }
    cout << "0" << endl;
    return 0;
}