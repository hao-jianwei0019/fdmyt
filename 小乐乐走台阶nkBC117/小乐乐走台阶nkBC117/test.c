#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//小乐乐上课需要走n阶台阶，因为他腿比较长，所以每次可以选择走一阶或者走两阶，那么他一共有多少种走法？
//从题可知小乐乐在n台阶时他到这一台阶要不1从n-1上来，要不2从n-2阶上来
//假设到n节有f（n）方式，n-1节有f（n-1）方式，n-2节有f（n-2）方式
//易得f(n)=f（n-1）+f（n-2）

int way(num)
{
    if (num == 1)
        return 1;
    if (num == 2)
        return 2;
    else {
        return way(num - 1) + way(num - 2);
    }
}
int main()
{
    int num;
    scanf("%d", &num);
    printf("%d", way(num));
    return 0;
}