class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param numbers int整型vector
     * @return bool布尔型
     */
    bool IsContinuous(vector<int>& numbers) {
        // write code here
        sort(numbers.begin(), numbers.end());
        int k = 0;
        int i = 0;
        while (numbers[i] == 0)
        {
            k++, i++;
        }

        int j = numbers[i++];

        for (; i < 5; i++)
        {
            while (numbers[i] != j + 1)
            {
                k--;
                j++;
                if (k < 0)return false;
            }
            j++;
        }
        return true;
    }
};