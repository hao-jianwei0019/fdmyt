#include<iostream>
#include<vector>
using namespace std;

class Solution {
public:
	/**
	 *	正数数组中的最小不可组成和
	 *	输入：正数数组arr
	 *	返回：正数数组中的最小不可组成和
	 */
	int getFirstUnFormedNum(vector<int> arr, int len) {
		int maxs = 0, mins = arr[0];
		for (auto a : arr)
		{
			maxs += a;
			mins = min(a, mins);
		}
		vector<int>dp(maxs + 1, 0);
		int ret = mins;
		for (int n = 0; n < len; n++)
		{
			for (int j = mins; j <= maxs; j++)
			{
				if (dp[j] + arr[n] <= j)dp[j] += arr[n];
				while (dp[ret] == ret && ret < maxs)ret++;
			}
		}
		if (ret == maxs)return maxs + 1;
		return ret;


	}
}O;
int main()
{
	cout<<O.getFirstUnFormedNum({ 3,2,5 }, 3);
	return 0;

}

