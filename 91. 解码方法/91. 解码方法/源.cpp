#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    int numDecodings(string s) {
        if (s[0] == '0')return 0;
        int n = s.size();
        if (n == 1)return s[0] != 0;
        vector<int> dp(n, 0);

        dp[0] = 1;

        if (s[1] != '0')dp[1]++;
        int i = 1;
        int t = s[i] - '0' + (s[i - 1] - '0') * 10;
        if (t <= 26 && t >= 10)dp[1]++;
        //如果s[1]是0，就会让dp[1]=0或1

        i = 2;
        for (; i < n; i++)
        {
            if (s[i] != '0')dp[i] += dp[i - 1];
            t = s[i] - '0' + (s[i - 1] - '0') * 10;
            if (t <= 26 && t >= 10)dp[i] += dp[i - 2];

        }
        return dp[n - 1];
    }
    
}o;
int main()
{
    o.numDecodings("10");
}