class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int n = prices.size();
        int minp = INT_MAX;
        int ans = 0;
        for (int i = 0; i < n; i++)
        {
            minp = min(minp, prices[i]);
            ans = max(ans, prices[i] - minp);
        }
        return ans;


    }
};