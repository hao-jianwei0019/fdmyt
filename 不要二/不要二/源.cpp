#include <iostream>
#include <vector>
using namespace std;

int main() {
    int w, h;
    cin >> w >> h;
    
    vector<vector<int>> tmp(w, vector<int>(h, 1));
    int ret = 0;
    for (int i = 0; i < w; i++)
    {
        for (int j = 0; j < h; j++)
        {
            if (tmp[i][j] == 1)
            {
                ret++;
                if (j + 2 < h)tmp[i][j + 2] = 0;

                if (i + 2 < w)tmp[i + 2][j] = 0;
            }
        }
    }
    cout << ret;
    return 0;

}
// 64 λ������� printf("%lld")