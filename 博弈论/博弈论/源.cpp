#include<map>
#include<cstdio>
int init() {
    char c = getchar();
    int x = 0, f = 1;
    for (; c < '0' || c > '9'; c = getchar())
        if (c == '-') f = -1;
    for (; c >= '0' && c <= '9'; c = getchar())
        x = (x << 1) + (x << 3) + (c ^ 48);
    return x * f;
}
void print(int x) {
    if (x < 0) x = -x, putchar('-');
    if (x > 9) print(x / 10);
    putchar(x % 10 + '0');
}
std::map<int, bool>map;
const int N = (int)1e3 + 5;
int d[N];
int main() {
    int n = init();
    for (int i = 1; i <= n; ++i)
        d[i] = init();
    for (int i = 1; i <= n; ++i) {
        int x = 0;
        for (int j = i; j <= n && j <= i + 6; ++j) {
            x = (x << 1) + (x << 3) + d[j];
            map[x] = 1;
        }
    }
    for (int k = 0;; ++k)
        if (!map.count(k)) {
            print(k); return 0;
        }
}