#include<bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    vector<int> dp(n + 1);
    for (int i = 1; i <= n; ++i) {
        dp[i] = i;
    }
    for (int i = 2; i <= (int)sqrt(n); ++i) {
        for (int j = 0; j <= n - i * i; ++j) {
            dp[j + i * i] = min(dp[j + i * i], dp[j] + 1);
        }
    }
    cout << dp[n] << endl;
    return 0;
}
