#include<iostream>
#include<string>
using namespace std;
namespace bhb
{
	class Quote
	{
	public:
		Quote() = default;//要求编译器自己生成
		Quote(const string &book,double sales_price)
			:bookNo(book),price(sales_price)
		{}
		string  isben()const
		{
			return bookNo;
		}
		virtual double net_price(size_t n)const
		{
			return price * n;
		}
		~Quote() = default;
	private:
		string bookNo;//书名代号
	protected:
		double price = 0.0;//原价
	};
	class Bulk_quote :public Quote//团购
	{
	public: 
		Bulk_quote() = default;
		Bulk_quote(const string& book, double p, size_t qty, double disc)
			:Quote(book,p),min_qty(qty),discount(disc)
		{}
		/*double net_price(size_t cnt)const
		{
			if (cnt >= min_qty)
				return cnt * (1 - discount) * price;
			else return cnt * price;
		}*/
		virtual void fun1()
		{
			cout << endl;
		}
	private:
		size_t min_qty = 0;		//最低数量
		double discount = 0.0;

	};
	class Test:public Bulk_quote
	{
		virtual void fun1()
		{
			cout << endl;
		}
	};
}

int main()
{
	bhb::Bulk_quote q1;
	bhb::Quote q2;
	bhb::Test q3;
	q1.fun1();
	cout << endl;
	return 0;

}