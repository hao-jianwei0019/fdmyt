#include <stdio.h>
#include <stdlib.h>


struct Node 
{     int val;
    struct Node *next;
    struct Node *random;
 };


struct Node* copyRandomList(struct Node* head) {
    struct Node* cur = head;
    while (cur)
    {
        struct Node* newtile = (struct Node*)malloc(sizeof(struct Node));
        if (newtile == NULL)
        {
            perror("mallooc");
            return;
        }
        struct Node* tile = cur->next;
        newtile->val = cur->val;
        cur->next = newtile;
        newtile->next = tile;
        cur = tile;

    }
    cur = head;
    while (cur)
    {
        struct Node* newtile = cur->next;
        if (cur->random != NULL)
            newtile->random = cur->random->next;
        else
            newtile->random = NULL;
        cur = newtile->next;

    }
    cur = head;
    struct Node* newtile = (struct Node*)malloc(sizeof(struct Node));

    struct Node* tile = newtile;
    newtile->next = NULL;
    while (cur)
    {
        tile->next = cur->next;
        cur->next = cur->next->next;
        cur = cur->next;
        tile = tile->next;
    }
    struct Node* newhead = newtile->next;
    free(newtile);
    return newhead;
}