class Solution {
public:
    vector<string> generateParenthesis(int n) {
        vector<vector<string>>dp(n + 1);
        dp[0].push_back("");
        dp[1].push_back("()");
        for (int i = 2; i <= n; i++)
            for (int j = 0; j < i; j++)
            {
                for (auto& s1 : dp[j])
                {
                    string tmp = "(" + s1 + ")";
                    for (auto& s2 : dp[i - 1 - j])
                        dp[i].push_back(tmp + s2);
                }

            }
        return dp[n];
    }
};