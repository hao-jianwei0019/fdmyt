#include <stdio.h>
int main() {
	    //   int arr1[1000];
	    //   int arr2[1000];
	    //   int arr3[2000];
	    int n = 0;
	    int m = 0;
	    //输入
	    scanf("%d %d", &n, &m);
	    int arr1[n];
	    int arr2[m];
	    int arr3[n + m];
	    //arr1的输入
	    int i = 0;
	    for (i = 0; i < n; i++)
	    {
	        scanf("%d", &arr1[i]);
	    }
	    //arr2的输入
	    for (i = 0; i < m; i++)
	    {
	        scanf("%d", &arr2[i]);
	    }
	    //合并数组
	    i = 0;
	    int j = 0;
	    int k = 0;
	    while (i < n && j < m)
	    {
	        if (arr1[i] < arr2[j])
	        {
	            arr3[k] = arr1[i];
	            i++;
	            k++;
	        }
	        else
	        {
	            arr3[k] = arr2[j];
	            j++;
	            k++;
	        }
	    }
	    if (i == n)
	    {
	        //把arr2中剩余的元素放在arr3中
	        while (j < m)
	        {
	            arr3[k] = arr2[j];
	            j++;
	            k++;
	        }
	    }
	    else
	    {
	        //把arr1中剩余的元素放在arr3中
	        while (i < n)
	        {
	            arr3[k] = arr1[i];
	            i++;
	            k++;
	        }
	    }
	    //输出
	    for (k = 0; k < n + m; k++)
	    {
	        printf("%d ", arr3[k]);
	    }
	    return 0;
	}
