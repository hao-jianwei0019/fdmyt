#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct Stu
{
	char name[20];
	int age;
};
int int_com(const void* e1, const void* e2)
{
	return *(int*)e1 - *(int*)e2;//升序，逆序则这两个数反一下
}
int stu_com(const void* e1, const void* e2)
{
	return strcmp(((struct Stu*)e1)->name,((struct Stu*)e2)->name);
}
void test1()
{
	int arr[10] = { 3,456,123,65,23,12,3,5,4,5 };
	qsort(arr, 10, sizeof(arr[0]), int_com);
	int i;
	for (i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
}
void test2()
{
	struct Stu b[3]= { {"张三",20}, {"李四", 50}, {"王五", 33} };
	qsort(b, 3, sizeof(b[0]), stu_com);
	
}
int main()
{
	//test1();
	test2();
	return 0;
}                   