#include <iostream>
#include<vector>
using namespace std;

bool max(int a, int b)
{
    return a >= b;
}
bool min(int a, int b)
{
    return a <= b;
}

int main() {
    int num = 0;
    vector<int> nums;
    cin >> num;
    if (num == 1 || num == 2)return 1;
    for (int i = 0; i < num; i++)
    {
        int tmp;
        cin >> tmp;
        nums.push_back(tmp);
    }
    int ret = 1;
    bool (*tmp)(int, int) = max;
    if (nums[0] < nums[1])tmp = min;
    int i = 2;
    while (i < num)
    {
        if (!tmp(nums[i - 1], nums[i]))
        {
            ret++;
            i++;
            while (i < num && nums[i] == nums[i - 1])i++;

            if (i >= num)break;

            if (max(nums[i - 1], nums[i]))tmp = max;
            else tmp = min;
        }
        else {
            i++;
        }
    }
    cout << ret;
    return 0;
}
// 64 λ������� printf("%lld")