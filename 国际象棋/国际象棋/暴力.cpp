#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
const int mod = 1e9 + 7;
int N, M, K;
int vis[11][105];
int ans = 0;
void fangzhi(int x, int y)
{
    vis[x][y]++;
    vis[x + 1][y + 2]++;
    vis[x + 1][y - 2]++;
    vis[x - 1][y + 2]++;
    vis[x - 1][y - 2]++;
    vis[x + 2][y + 1]++;
    vis[x + 2][y - 1]++;
    vis[x - 2][y + 1]++;
    vis[x - 2][y - 1]++;
}
void huishou(int x, int y)
{
    vis[x][y]--;
    vis[x + 1][y + 2]--;
    vis[x + 1][y - 2]--;
    vis[x - 1][y + 2]--;
    vis[x - 1][y - 2]--;
    vis[x + 2][y + 1]--;
    vis[x + 2][y - 1]--;
    vis[x - 2][y + 1]--;
    vis[x - 2][y - 1]--;
}

void dfs(int x, int y, int k)//当前点是x，y，之前放了k个马
{
    if (k == K)
    {
        ans = (ans + 1) % mod;
        return;
    }

    if (y > M + 1)
    {
        x++, y = 2;
        if (x > N + 1)return;
    }
    dfs(x, y + 1, k);//当前（x,y）不放

    if (!vis[x][y])//放
    {
        fangzhi(x, y);
        dfs(x, y + 1, k + 1);
        huishou(x, y);
    }

}
int main()
{
    cin >> N >> M >> K;
    memset(vis, 0, sizeof vis);
    dfs(2, 2, 0);
    cout << ans;
    return 0;
}