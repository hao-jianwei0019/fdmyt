#include <fstream>
#include <iostream>
#include <queue>
#include <stack>
#include <utility>
using namespace std;
const int N = 1010;
char board[N][N];
bool vis[N][N];
int x1, x2, y1, y2;
int n, m;
int dx[4] = { -1,0,1,0 };
int dy[4] = { 0,1,0,-1 };
int bfs()
{

    queue<pair<int, int>> f;
    f.push({ x1,y1 });
    vis[x1][y1] = true;
    int ans = 0;
    while (!f.empty())
    {
        ans++;
        int k = f.size();
        for (int i = 0; i < k; i++)
        {
            int x = f.front().first;
            int y = f.front().second;
            f.pop();
            for (int i = 0; i < 4; i++)
            {
                int a = x + dx[i], b = y + dy[i];
                //cout<<a<<' '<<b<<endl;
                if (a == x2 && b == y2)
                {
                    return ans;
                }
                if (a > 0 && a <= n && b > 0 && b <= m && board[a][b] == '.' && !vis[a][b])
                {
                    vis[a][b] = true;
                    f.push({ a,b });
                }
            }
        }
    }
    return -1;
}
int main() {
    cin >> n >> m;
    cin >> x1 >> y1 >> x2 >> y2;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
        {
            cin >> board[i][j];
        }
    if (board[x1][y1] == '*' || board[x2][y2] == '*')
    {
        cout << -1 << endl;
        return 0;
    }
    int ret = bfs();
    cout << ret << endl;
    return 0;
}
// 64 λ������� printf("%lld")