#include <iostream>
#include<string>
#include<vector>

using namespace std;

const int N = 501, M = 50001; // 点和边上界
int head[N];  // 前向星表示

struct Edge {
    int v;
    int next;
}edge[M];
int edgeCnt; //边数

void addEdge(int u, int v) {
    Edge& e = edge[edgeCnt];
    e.v = v;
    e.next = head[u];
    head[u] = edgeCnt++;
}
void init(int n) {
    edgeCnt = 0;
    memset(head, -1, n * sizeof(*head));
}

int ylink[N];        //右侧匹配的左侧元素
bool vis[N];      //右侧元素是否已被访问过
bool match(int u) {
    for (int e = head[u]; e != -1; e = edge[e].next) {
        int v = edge[e].v;
        if (vis[v]) continue;
        vis[v] = true;
        if (ylink[v] == -1 || match(ylink[v])) {
            ylink[v] = u;    //当前左侧元素成为当前右侧元素的新匹配
            return true; //匹配成功
        }
    }
    return false; //循环结束，仍未找到匹配，匹配失败
}
// n，m左右边的点数。标号从0开始
int Hungarian(int n, int m) {
    int maxMatchCnt = 0;
    memset(ylink, -1, m * sizeof(*ylink));
    for (int i = 0; i < n; i++) {
        memset(vis, 0, m * sizeof(*vis)); //重置vis数组
        if (match(i)) maxMatchCnt++;
    }
    return maxMatchCnt;
}
int main() {
    std::ios::sync_with_stdio(false);
    cin.tie(0); cout.tie(0);
    int n, m, c;
    cin >> n >> m >> c;
    init(n);
    while (c--) {
        int u, v;
        cin >> u >> v;
        --u; --v;
        addEdge(u, v);
    }
    cout << Hungarian(n, m) << endl;
    return 0;
}