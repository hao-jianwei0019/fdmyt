class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {
        if (cost.size() == 2)return cost[0] > cost[1] ? cost[1] : cost[0];

        vector<int> mincost(cost.size() + 1);
        mincost[0] = cost[0];
        mincost[1] = cost[1];
        int i = 2;
        for (; i < cost.size(); i++)
        {
            mincost[i] = (mincost[i - 1] > mincost[i - 2] ? mincost[i - 2] : mincost[i - 1]) + cost[i];
        }
        mincost[i] = mincost[i - 1] > mincost[i - 2] ? mincost[i - 2] : mincost[i - 1];
        return mincost[i];
    }
};
class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {

        vector<int> mincost(cost.size() + 1);
        int i = 2;
        for (; i <= cost.size(); i++)
        {
            mincost[i] = min(mincost[i - 1] + cost[i - 1], mincost[i - 2] + cost[i - 2]);
        }

        return mincost[i - 1];
    }
};