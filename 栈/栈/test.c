#include "stack.h"
int main()
{
	ST stail;
	STInit(&stail);
	STPush(&stail, 1);
	STPush(&stail, 2);
	STPush(&stail, 3);
	STPush(&stail, 4);
	STPush(&stail, 5);
	while (!STEmpty(&stail))
	{
		printf("%d", STTop(&stail));
		STPop(&stail);
	}
	STDestroy(&stail);
	return 0;
}