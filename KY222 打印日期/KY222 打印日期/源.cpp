#include <iostream>

using namespace std;
int GetMonthDay(int year, int month)
{
    static int MonthDay[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
    if (month == 2 && (year % 400 == 0 || year % 4 == 0 && year % 100 != 0))
    {
        return 29;
    }
    return MonthDay[month];
}
int main() {
    int year, month, day;
    int sum = 0;
    while (cin >> year >> day) {
        month = 1;
        while (day > GetMonthDay(year, month))
        {
            day -= GetMonthDay(year, month);
            month++;
        }

        string s;
        s += year / 1000 + '0';
        s += year / 100 % 10 + '0';
        s += year / 10 % 10 + '0';
        s += year % 10 + '0';
        s += '-';
        s += month / 10 + '0';
        s += month % 10 + '0';
        s += '-';
        s += day / 10 + '0';
        s += day % 10 + '0';
        cout << s << endl;
    }

