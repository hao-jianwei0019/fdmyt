#include<iostream>
#include<queue>
#include<vector>
using namespace std;
typedef long long ll;
int n;
int main()
{
	cin >> n;
	priority_queue<ll, vector<ll>, greater<ll>> heap;
	while (n--)
	{
		ll x;
		cin >> x;
		heap.push(x);
	}
	ll ret = 0;
	while (heap.size() > 1)
	{
		ll t1 = heap.top(); heap.pop();
		ll t2 = heap.top(); heap.pop();
		heap.push(t1 + t2);
		ret += t1 + t2;
	}
	cout << ret<<endl;
	return 0;

}