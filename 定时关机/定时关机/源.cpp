#include <iostream>
#include <windows.h>

using namespace std;

bool isCurrentTimeGreaterThanInputTime(int inputHour, int inputMinute) {
    SYSTEMTIME sysTime;
    GetLocalTime(&sysTime);

    int currentHour = sysTime.wHour;
    int currentMinute = sysTime.wMinute;

    if (currentHour > inputHour || (currentHour == inputHour && currentMinute >= inputMinute)) {
        return true;
    }
    else {
        return false;
    }
}
void down()
{
    system("shutdown -s -t 30");
    printf("请注意您的电脑将在30s内关机\n");
}
int main() {
    int inputHour, inputMinute;
    cout << "请输入时间（小时 分钟）：";
    cin >> inputHour >> inputMinute;

    while (true) {
        if (isCurrentTimeGreaterThanInputTime(inputHour, inputMinute)) {
            cout << "当前时间已经大于等于输入时间" << endl;
            down();
            break;
        }
        else {
            cout << "当前时间未达到输入时间，等待1分钟..." << endl;
            Sleep(60000); // Sleep for 1 minute
        }
    }

    return 0;
}
