class Solution {
public:
    int hIndex(vector<int>& citations) {
        sort(citations.begin(), citations.end());
        int n = citations.size();
        int ans = 1;
        for (int i = n - 1; i >= 0; i--)
        {
            if (ans <= citations[i])ans++;
            else break;
        }
        return ans - 1;
    }
};