#include <stdio.h>
void find_single_dog(int p1[], int sz, int p2[2])
{
	int ret = 0;
	int i=0;
	for (i = 0; i < sz; i++)
	{
		ret  ^= p1[i];
	}
	int pos = 0;
	for (i = 0; i < 32; i++)
	{
		if (((ret >> i) & 1) == 1)
		{
			pos = i;
			break;
		}
	}
	for (i = 0; i < sz; i++)
	{
		if (((p1[i] >> pos) & 1) == 1)
		{
			p2[0] ^= p1[i];
		}
		else
		{
			p2[1] ^= p1[i];
		}
	}

}
int main()
{
	int arr1[] = { 1,2,3,4,5,1,2,3,4,6};
	int single_dog[2] = { 0 };
	int sz = sizeof(arr1) / sizeof(arr1[0]);
	find_single_dog(arr1,sz, single_dog);
	printf("%d  %d", single_dog[0], single_dog[1]);
	return 0;
}