#include<iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode* pushfront(ListNode* head, int x)
    {
        ListNode* tmp = new ListNode(x, head);
        return tmp;
    }
    ListNode* reverseList(ListNode* head) {
        if (!head) return nullptr;
        ListNode* myhead = head;
        ListNode* newhead = nullptr;
        while (myhead != nullptr)
        {
            newhead = pushfront(newhead, myhead->val);
            myhead = myhead->next;

        }
        return newhead;

    }
};