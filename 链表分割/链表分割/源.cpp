#include <iostream>

struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};
#include <cstddef>
class Partition {
public:
    ListNode* partition(ListNode* pHead, int x) {
        ListNode* shead1 = (ListNode*)malloc(sizeof(ListNode));
        ListNode* shead2 = (ListNode*)malloc(sizeof(ListNode));

        shead1->next = NULL;
        shead2->next = NULL;

        ListNode* end1 = shead1;
        ListNode* end2 = shead2;
        ListNode* cur = pHead;


        while (cur)
        {
            if (cur->val < x)
            {
                end1->next = cur;
                cur = cur->next;
                end1 = end1->next;
            }
            else {
                end2->next = cur;
                cur = cur->next;
                end2 = end2->next;
            }
        }
        end1->next = shead2->next;
        end2->next = NULL;
        ListNode* newhead = shead1->next;
        free(shead1);
        free(shead2);
        return newhead;

        // write code here
    }
};