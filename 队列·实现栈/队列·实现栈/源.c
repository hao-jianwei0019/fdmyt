typedef char QDatatype;

typedef struct QueueNode
{
	struct QueueNode* next;
	QDatatype data;
}QNode;

typedef struct Queue
{
	QNode* head;
	QNode* tail;
	int size;
}Queue;

void QueueInit(Queue* pq);
void QueueDestroy(Queue* pq);
void QueuePush(Queue* pq, QDatatype x);
void QueuePop(Queue* pq);
int QueueSize(Queue* pq);
bool QueueEmpty(Queue* pq);
QDatatype QueueFront(Queue* pq);
QDatatype QueueBack(Queue* pq);


void QueueInit(Queue* pq)
{
	assert(pq);

	pq->head = pq->tail = NULL;
	pq->size = 0;

}
void QueueDestroy(Queue* pq)
{
	assert(pq);
	QNode* cur = pq->head;
	while (cur)
	{
		QNode* next = cur->next;
		free(cur);
		cur = next;
	}
	pq->head = pq->tail = NULL;
	pq->size = 0;

}
void QueuePush(Queue* pq, QDatatype x)
{
	assert(pq);
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (newnode == NULL)
	{
		perror("malloc fail");
		return;
	}
	newnode->data = x;
	newnode->next = NULL;

	if (QueueEmpty(pq))
	{
		assert(pq->tail == NULL);//判不判断都可以，正常来说要为空全是空
		pq->head = pq->tail = newnode;
	}
	else
	{
		pq->tail->next = newnode;
		pq->tail = newnode;
	}
	pq->size++;
}
void QueuePop(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	if (pq->head->next == NULL)
	{
		free(pq->head);
		pq->head = pq->tail = NULL;
	}
	else
	{
		QNode* tmp = pq->head->next;
		free(pq->head);
		pq->head = tmp;
	}
	pq->size--;


}
int QueueSize(Queue* pq)
{
	assert(pq);
	return pq->size;
}
bool QueueEmpty(Queue* pq)
{
	return pq->head == NULL;
}
QDatatype QueueFront(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));

	return pq->head->data;
}
QDatatype QueueBack(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));

	return pq->tail->data;
}



typedef struct {
	Queue cur1;
	Queue cur2;
} MyStack;


MyStack* myStackCreate() {
	MyStack* obj = (MyStack*)malloc(sizeof(MyStack));
	if (obj == NULL)
	{
		perror("malloc fail");
		return NULL;
	}
	QueueInit(&obj->cur1);
	QueueInit(&obj->cur2);
	return obj;
}

void myStackPush(MyStack* obj, int x)
{
	if (!QueueEmpty(&obj->cur1))
	{
		QueuePush(&obj->cur1, x);
	}
	else {
		QueuePush(&obj->cur2, x);
	}
}

int myStackPop(MyStack* obj) {
	Queue* emptyQ = &obj->cur1;
	Queue* noemptyQ = &obj->cur2;
	if (!QueueEmpty(&obj->cur1))
	{
		emptyQ = &obj->cur2;
		noemptyQ = &obj->cur1;
	}
	while (QueueSize(noemptyQ) > 1)
	{
		QueuePush(emptyQ, QueueFront(noemptyQ));
		QueuePop(noemptyQ);
	}
	int top = QueueFront(noemptyQ);
	QueuePop(noemptyQ);

	return top;

}

int myStackTop(MyStack* obj) {
	if (!QueueEmpty(&obj->cur1)) {
		return QueueBack(&obj->cur1);
	}
	else {
		return QueueBack(&obj->cur2);
	}
}

bool myStackEmpty(MyStack* obj) {
	return QueueEmpty(&obj->cur1) && QueueEmpty(&obj->cur2);

}

void myStackFree(MyStack* obj) {
	QueueDestroy(&obj->cur1);
	QueueDestroy(&obj->cur2);
	free(obj);
}

/**
 * Your MyStack struct will be instantiated and called as such:
 * MyStack* obj = myStackCreate();
 * myStackPush(obj, x);

 * int param_2 = myStackPop(obj);

 * int param_3 = myStackTop(obj);

 * bool param_4 = myStackEmpty(obj);

 * myStackFree(obj);
*/