#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
void reverse(char* arr, int n)
{
	int left = 0;
	int right = n - 1;
	while (right > left)
	{
		char tmp = *(arr + left);
		*(arr + left) = *(arr + right);
		*(arr + right) = tmp;
		right--;
		left++;
	}
}
char* left_reverse(char* arr, int len ,int k)
{
	if (k == 0)
	{
		return ' ';
	}
	reverse(arr, len);
	reverse(arr, len - k);
	reverse((arr + len - k), k);
	return arr;

}
int test(char* b, int len)
{
	char c[5];
	int i;
	for (i = 0; i < len; i++)
	{
		scanf("%c", &c[i]);
	}
	for (i = 0; i < len; i++)
	{
		if (strncmp(left_reverse(b,5, 1), c, 5) == 0)
		{
			return 1;
		}
	}
	return 0;
}
int main()
{
	char b[] = { 'A','A','B','C','D'};
	/*reverse(b, 5);*/
	const int a = sizeof(b)/sizeof(b[0]);
	int c=test(b,a);
	printf("%d", c);
	return 0;
}