#include <iostream>
using namespace std;
int feibo(int n)
{
	if (n == 1 || n == 2)
		return 1;
	else {
		return feibo(n - 1) + feibo(n - 2);
	}
}
int main()
{
	int i = 0;
	int arr[20] = { 0 };
	for (i = 0; i < 20; i++)
	{
		arr[i] = feibo(i + 1);
	}
	for (i = 0; i < 20; i++)
	{
		cout << arr[i] << "  ";
	}
	return 0;
}