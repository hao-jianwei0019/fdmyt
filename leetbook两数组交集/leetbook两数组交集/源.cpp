#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    void ShellSort(vector<int>& a)
    {
        int gap = a.size();
        while (gap > 1)
        {
            gap = gap / 2;
            for (int i = 0; i < a.size() - gap; i++)
            {
                int end = i;
                int tmp = a[i + gap];
                while (end >= 0)
                {
                    if (tmp < a[end])
                    {
                        a[end + gap] = a[end];
                        end -= gap;
                    }
                    else
                    {
                        break;
                    }
                }
                a[end + gap] = tmp;
            }
        }
    }
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        ShellSort(nums1);
        ShellSort(nums2);
        vector<int> v;
        int i = 0, j = 0;
        while (i < nums1.size() && j < nums2.size())
        {
            if (nums1[i] < nums2[j])
            {
                i++;
            }
            else if (nums1[i] == nums2[j])
            {
                v.push_back(nums1[i]);
                j++;
                i++;
            }
            else {
                j++;
            }

        }

        return v;
    }
};