#include <iostream>
#include<algorithm>
using namespace std;
const int N = 1e8 + 10;
int sum[N];
int n, x;
int ans = N;
int ansl = 0;
int main() {
    cin >> n >> x;
    sum[0] = 0;

    int tmp;
    for (int i = 1; i <= n; i++)
    {
        cin >> tmp;
        sum[i] = sum[i - 1] + tmp;
    }
    int l = 0, r = 1;
    while (r < n)
    {
        
        while (sum[r] - sum[l] < x)
        {
            r++;
            if (r > n)break;
        }
        if (r - l < ans)
        {
            ans = r - l;
            ansl = l + 1;
        }
        while (sum[r] - sum[l] >= x)
        {
            l++;
        }
        if (r - l + 1 < ans)
        {
            ans = r - l+1;
            ansl = l;
        }
    }
    cout << ansl << ' ' << ansl + ans - 1;
    return 0;
}
// 64 λ������� printf("%lld")