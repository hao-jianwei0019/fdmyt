#include <stdio.h>
#include <assert.h>
unsigned int my_strlen(const char* str )
{
	assert(str != NULL);
	unsigned int count = 0;
	while (*str != '\0')
	{
		count++;
		str++;
	}
	return count;
}
int main()
{
	char arr[] = "hello";
	printf("%d", my_strlen(arr));
	return 0;
}