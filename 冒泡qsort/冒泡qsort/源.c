#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct Stu
{
	char name[20];
	int age;
};
void Swap(char* buf1, char* buf2, int width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tmp = *buf1;
		*buf1 = *buf2;
		*buf2 = tmp;
		buf1++;
		buf2++;
	}
}

//改造冒泡排序函数，使得这个函数可以排序任意指定的数组
void bubble_sort(void* base, size_t sz, size_t width, int (*cmp)(const void* e1, const void* e2))
{
	//趟数
	size_t i = 0;
	for (i = 0; i < sz - 1; i++)
	{
		//一趟冒泡排序的过程
		size_t j = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
			{
				//交换
				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
			}
		}
	}
}
int int_com(const void* e1, const void* e2)
{
	return *(int*)e1 - *(int*)e2;//升序，逆序则这两个数反一下
}
int stu_com(const void* e1, const void* e2)
{
	return strcmp(((struct Stu*)e1)->name, ((struct Stu*)e2)->name);
}
void test1()
{
	int arr[10] = { 3,456,123,65,23,12,3,5,4,5 };
	bubble_sort(arr, 10, sizeof(arr[0]), int_com);
	int i;
	for (i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
}
void test2()
{
	struct Stu b[3] = { {"张三",20}, {"李四", 50}, {"王五", 33} };
	bubble_sort(b, 3, sizeof(b[0]), stu_com);

}

int main()
{
	test1();
	test2();
	return 0;
}