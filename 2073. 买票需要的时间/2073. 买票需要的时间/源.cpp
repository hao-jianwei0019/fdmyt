class Solution {
public:
    int timeRequiredToBuy(vector<int>& tickets, int k) {
        int ans = 0;
        int val = tickets[k];
        for (int i = 0; i <= k; i++)
        {
            if (tickets[i] > val)ans += val;
            else ans += tickets[i];
        }
        for (int i = k + 1; i < tickets.size(); i++)
        {
            if (tickets[i] >= val)ans += val - 1;
            else ans += tickets[i];
        }
        return ans;
    }
};