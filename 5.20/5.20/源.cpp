#include <iostream>
#include <iomanip>
using namespace std;
bool isPythagoreanTriple(int a, int b, int c)
{
    int hypotenuse = c;
   
        return (a * a + b * b == hypotenuse * hypotenuse);
}
int main()
{
    int counter{ 0 };
    cout << "All Pythagorean Triples up to sides of 500" << endl;
    cout << "Number" << setw(9) << "Side1 " << setw(9) << "Side2" << setw(9) << "Side3" << endl;
    for (int i = 1; i <= 500; ++i)
    {
        for (int j = i + 1; j <= 500; ++j)
        {
            for (int k = j + 1; k <= 500; ++k)
            {
                if (isPythagoreanTriple(i, j, k))
                {
                    ++counter;
                    cout << setw(3) << counter << setw(9) << i << setw(9) << j << setw(9) << k << endl;
                }
            }
        }
    }
    cout << endl << endl << "Total " << counter << " Pythagorean Triples found!!" << endl;
    return 0;
}
//设三角形的边为a,b,c.
//首先让a=1.b=2,然后c=3-500
//然后a=2,b=3,然后c=4-500
//然后a=3,b=4,然后c=5-500
//然后a=4,b=5,然后c=6-500
//好处是始终存在a<b<c
//斜边始终为c，而且也不会重复
