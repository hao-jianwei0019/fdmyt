#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        int i = 1;
        while (i <= digits.size())
        {
            digits[digits.size() - i]++;
            if (digits[digits.size() - i] == 10)
            {
                digits[digits.size() - i] = 0;
                i++;
            }
            else
            {
                break;
            }
        }
        if (i > digits.size())
        {
            digits.insert(digits.begin(), 1);
        }
        return digits;
    }
};

