/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> reversePrint(ListNode* head) {

        int i = 0;
        int nums = 0;
        ListNode* cur = head;
        while (cur != NULL)
        {
            nums++;
            cur = cur->next;
        }
        vector<int> num(nums, 0);
        cur = head;
        while (i < nums)
        {
            num[nums - i - 1] = cur->val;
            cur = cur->next;
            i++;
        }
        return num;
    }
};