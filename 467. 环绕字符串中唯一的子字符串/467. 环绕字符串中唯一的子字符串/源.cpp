#include<iostream>
#include<vector>
#include<string>
using namespace std;
class Solution {
public:
    int findSubstringInWraproundString(string s) {
        int n = s.size();
        vector<int>dp(n, 1);
        for (int i = 1; i < n; i++)
        {
            if (s[i] - 1 == s[i-1] || s[i - 1] == 'z' && s[i] == 'a')
                dp[i] = dp[i - 1] + 1;
        }
        //s中可能有重复的字符
        vector<int> hash(26, 0);
        for (int i = 0; i < n; i++)
        {
            hash[s[i] - 'a'] = max(hash[s[i] - 'a'], dp[i]);

        }
        int ret = 0;
        for (auto ch : hash)
        {
            ret += ch;
        }
        return ret;
    }
}o;
int main()
{
    o.findSubstringInWraproundString("zab");
}