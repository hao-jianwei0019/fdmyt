class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        unordered_map<int, int> freq;
        for (int num : nums) {
            ++freq[num];
        }
        vector<int> ret;
        for (auto& ch : freq)
        {
            if (ch.second == 1)
                ret.push_back(ch.first);
        }
        return ret;

    }
};