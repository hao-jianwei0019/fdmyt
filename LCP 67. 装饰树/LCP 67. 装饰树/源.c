/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */


struct TreeNode* expandBinaryTree(struct TreeNode* root) {
    if (root == NULL)
        return NULL;
    if (root->left == NULL && root->right == NULL)
    {
        return root;
    }
    expandBinaryTree(root->left);
    if (root->left)
    {
        struct TreeNode* cur = (struct TreeNode*)malloc(sizeof(struct TreeNode));
        cur->left = root->left;
        cur->right = NULL;
        cur->val = -1;
        root->left = cur;
    }
    expandBinaryTree(root->right);
    if (root->right)
    {
        struct TreeNode* cur = (struct TreeNode*)malloc(sizeof(struct TreeNode));
        cur->right = root->right;
        cur->left = NULL;
        cur->val = -1;
        root->right = cur;
    }
    return root;

}