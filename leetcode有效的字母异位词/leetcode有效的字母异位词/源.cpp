#include<iostream>
using namespace std;
class Solution {
public:
    bool isAnagram(string s, string t) {
        if (s.size() != t.size() || s.size() == 0)
            return false;
        int i = 0;
        while (i < s.size())
        {
            int j = t.find(s[i]);
            if (j == -1)
                return false;
            t.erase(j,1);
            i++;
        }
        return true;
    }
};
int main()
{
    Solution s;
    int t=s.isAnagram("anagram","nagaram");
    cout << t;

}