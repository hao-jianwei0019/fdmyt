#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    void duplicateZeros(vector<int>& arr) {
        if (arr.size() == 0)return;
        /* int star = 0;
       int dest = arr.size() - 1;
        while (star < dest)
        {
            if (arr[star] == 0)dest--;

            star++;
        }
        

        if (arr[dest] == 0&&star==dest)
        {
            dest--;
            arr[arr.size() - 1] = 0;
            star = arr.size() - 2;

        }
        else
        star = arr.size() - 1;*/
        int star = -1;
        int dest = 0;
        while(star < (int)arr.size())
        {
            if (arr[dest] == 0)star+=2;
            else star ++;
            if (star >= arr.size() - 1)break;
            dest++;
        }
        if (star == arr.size())
        {
            arr[arr.size() - 1] = 0;
            dest--; star -= 2;
        }
            
        while(dest >= 0)
        {
            if (arr[dest] == 0)
            {
                arr[star] = 0;
                star--; 
            }
            arr[star] = arr[dest];
            star--; dest--;
        }

    }
}O;
int main()
{
    vector<int> arr = { 1, 5, 2, 0, 6, 8, 0, 6, 0 };
    O.duplicateZeros(arr);
    return 0;
}