class Solution {
public:




    string replaceWords(vector<string>& dictionary, string sentence) {
        unordered_set<string> dic;
        for (auto& ch : dictionary)dic.emplace(ch);
        vector<string> ans;
        int j = 0;
        int i = j;
        while (i != sentence.size())
        {

            while (i != sentence.size() && sentence[i] != ' ')i++;
            string tmp;
            if (i == sentence.size())
                tmp = sentence.substr(j);
            else tmp = sentence.substr(j, i - j);
            string tmp1;
            int x;
            for (x = 0; x < tmp.size(); x++)
            {
                tmp1 += tmp[x];
                if (dic.count(tmp1))
                {
                    ans.emplace_back(tmp1);
                    break;
                }
            }

            if (x == tmp.size())ans.push_back(tmp);
            if (i == sentence.size())break;
            i++;
            j = i;
        }
        string cur;
        for (int i = 0; i < ans.size(); i++)
        {
            cur += ans[i];
            cur += ' ';
        }
        cur.pop_back();
        return cur;

    }
};