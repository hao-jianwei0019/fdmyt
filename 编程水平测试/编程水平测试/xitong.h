#pragma once
#include <iostream>
#include <string>
#include <istream>
#include <fstream>
#include <vector>
using namespace std;

//定义一个student的结构体类型
struct Stu
{
	char number[5];
	char name[10];
	char sex[6];
	char hometown[10];
	char date[12];
};
Stu stu[];

void menu1();//初始菜单
void sexclass(int total);//男女生人数统计
void monthclass(int total);//11月找人数
void find(int total);//找人
void   modify(int total);//修改信息
void change(int i, int total);//改
void save(int total);//存数据
void add(int total);//添加数据
void sort(int total);
