#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <math.h>
int main()
{
	int num;
	for (num = 0; num < 100000; num++)
	{
		int i = 1;
		int b = num;

		while (b>9)
		{ 
			b /= 10;
			i++;
		}
		int a=0;
		b = num;
		int mid = 0;
		for (a = 0; a < i; a++)
		{
			mid += (int)pow(b % 10, i);
			b /= 10;
		}
		if (mid == num)
		{
			printf("%d  ", mid);
		}
	}
	return 0;
}