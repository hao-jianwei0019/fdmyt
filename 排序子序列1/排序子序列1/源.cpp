#include <iostream>
#include <string>
using namespace std;
const int N = 1e5 + 10;
int nums[N];
int main() {
    int n;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cin >> nums[i];
    }
    int i = 0;
    int ret = 0;
    while (i < n)
    {
        if (i == n - 1)
        {
            ret++;
            break;
        }
        if (nums[i] > nums[i + 1])
        {
            while (i + 1 < n && nums[i] >= nums[i + 1])i++;
            ret++;
        }
        else if (nums[i] < nums[i + 1])
        {
            while (i + 1 < n && nums[i] <= nums[i + 1])i++;
            ret++;

        }
        else
        {
            while (i + 1 < n && nums[i] == nums[i + 1])i++;
        }
        i++;
    }
    cout << ret << endl;
    return 0;
}
// 64 λ������� printf("%lld")