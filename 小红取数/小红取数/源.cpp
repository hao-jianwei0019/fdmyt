#include<bits/stdc++.h>
using namespace std;
#define ll long long
ll a[100100];
ll dp[1010][1010];
int main() {
    long long n, k, i, j;
    cin >> n >> k;
    for (i = 1; i <= n; i++)cin >> a[i];
    for (i = 0; i <= n; i++) {
        for (j = 0; j < k; j++) {
            dp[i][j] = -1e18;
        }
    }
    dp[0][0] = 0;

    for (i = 1; i <= n; i++) {
        for (j = 0; j < k; j++) {
            dp[i][(j + a[i]) % k] = max(dp[i - 1][j] + a[i], dp[i - 1][(j + a[i]) % k]);
        }
    }
    if (dp[n][0] <= 0)cout << -1;
    else cout << dp[n][0];
}

