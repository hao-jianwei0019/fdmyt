
#include <functional>
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param str string字符串
     * @return string字符串vector
     */

    set<string >ans;
    bool vis[11];
    int n;
    string st;
    void dfs(string s)
    {
        if (s.size() == n)
        {
            ans.insert(s);
            return;
        }
        for (int i = 0; i < n; i++)
        {
            if (vis[i] == false)
            {
                s += st[i];
                vis[i] = true;
                dfs(s);
                s.pop_back();
                vis[i] = false;
            }
        }
    }
    vector<string> Permutation(string str) {
        // write code here
        st = str;
        n = st.size();
        if (n == 0)return {};
        memset(vis, false, sizeof vis);
        string s;
        dfs(s);
        vector<string> ret;
        for (auto ch : ans)
        {
            ret.push_back(ch);
        }
        return ret;
    }
};