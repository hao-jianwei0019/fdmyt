#include <stdio.h>
#include <assert.h>
char* my_strstr(const char* e1, const char* e2)
{
	assert(e1 && e2);
	char* a1 = NULL;
	char* a2 = NULL;
	char* cp = e1;
	while (*cp != '\0')
	{
		a1 = cp;
		a2 = e2;
		while (*a1== *a2)
		{
			a1++;
			a2++;

		}
		if (*a2 == '\0')
		{
			return cp;
		}
		cp++;
	}
	return NULL;
}
int main()
{
	char arr[] = "addetfge";
	char str[] = "det";
	char str1[] = "fsf";
	char* b = my_strstr(arr, str);
	char* c = my_strstr(arr, str1);
	printf("%s\n%s", b, c);
	return 0;
}