void deleteNode(struct ListNode* node) {
    struct ListNode* nextnode = node->next;
    while (nextnode->next != NULL)
    {
        node->val = nextnode->val;
        node = node->next;
        nextnode = node->next;
    }
    node->val = nextnode->val;
    node->next = NULL;
    free(nextnode);

}


//https ://leetcode.cn/leetbook/read/top-interview-questions-easy/xnarn7/