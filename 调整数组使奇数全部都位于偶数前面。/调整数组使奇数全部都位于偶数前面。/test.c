#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
void move(int arr[], int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left<right)
	{

		//找偶数的过程
		while ((left<right) && (arr[left] % 2 == 1))
		{
			left++;
		}
		//找奇数的过程
		while ((left < right) && (arr[right] % 2 == 0))
		{
			right--;
		}
		if (left < right)
		{
			int tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
			left++;
			right--;
		}
	}
}

int main()
{
	int arr[10] = { 0 };
	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	//输入
	for (i = 0; i < sz; i++)
	{
		scanf("%d", &arr[i]);
	}
	//调整
	move(arr, sz);

	//输出
	for (i = 0; i < sz; i++)
	{

		printf("%d ", arr[i]);
	}

	return 0;
}