#include<iostream>
#include<algorithm>
#include<vector>
#include<cstring>
#include<queue>
using namespace std;
bool vis[5010];
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param n int整型
     * @param m int整型
     * @return int整型
     */
    int LastRemaining_Solution(int n, int m) {
        // write code here
        memset(vis, false, sizeof vis);
        int ans = n - 1;
        int star = 0;
        while (ans--)
        {
            int cur = m - 1;
            while (cur--)
            {
                star++;
                if (star == n)star = 0;

                while (vis[star] == true)
                {
                    star++;
                    if (star == n)star = 0;
                }
            }
            vis[star] = true;
            while (vis[star] == true)
            {
                star++;
                if (star == n)star = 0;
            }
        }
        return star;

    }
}O;
int main()
{
    O.LastRemaining_Solution(5, 3);
    return 0;
}