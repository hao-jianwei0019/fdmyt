#include <vector>
#include <iostream>
using namespace std;
class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        int tire = matrix.size();
        if (tire % 2 == 0)
        {
            int j = tire / 2 - 1;
            int n = 1;
            while (j >= 0)
            {
                int i = 2 * n - 1;
                for (int ch = 0; ch < i; ch++)
                {
                    int tmp = matrix[j + ch][j];
                    matrix[j + ch][j] = matrix[j + i][j + ch];
                    matrix[j + i][j + ch] = matrix[j + i - ch][j + i];
                    matrix[j + i - ch][j + i] = matrix[j][j + i - ch];
                    matrix[j][j + i - ch] = tmp;
                }
                j--;
                n++;
            }

        }
        else
        {
            int n = 1;
            int j = tire / 2 - 1;
            while (j >= 0)
            {
                int i = 2 * n;
                for (int ch = 0; ch < i; ch++)
                {
                    int tmp = matrix[j + ch][j];
                    matrix[j + ch][j] = matrix[j + i][j + ch];
                    matrix[j + i][j + ch] = matrix[j + i - ch][j + i];
                    matrix[j + i - ch][j + i] = matrix[j][j + i - ch];
                    matrix[j][j + i - ch] = tmp;
                }
                j--;
                n++;
            }
        }

    }
    Solution()
    {
        matrix = { {2,29,20,26,16,28},{12,27,9,25,13,21},{32,33,32,2,28,14},{13,14,32,27,22,26},{33,1,20,7,21,7},{4,24,1,6,32,34} };
    }
    void rotate1(vector<vector<int>>& matrix) {
        int n = matrix.size();
        // ��ת��
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                swap(matrix[i][j], matrix[j][i]);
            }
        }
        // �����Ҿ���Գ�
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n / 2; j++) {
                swap(matrix[i][j], matrix[i][n - j - 1]);
            }
        }

    }


public:
    vector<vector<int>> matrix;
};
int main()
{
    Solution s;
    s.rotate1(s.matrix);
}