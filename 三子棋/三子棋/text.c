#include "game.h"
void menu();
void game()
{
	int board[ROW][COL];
	int ret = 0;
	init_board(board, ROW, COL);
	print_board(board, ROW, COL);

	while(1)//不知道咋循环直接死循环靠break往出跳
	{
		player_move(board, ROW, COL);
		print_board(board, ROW, COL);
		ret = is_win(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
		computer_move(board, ROW, COL);
		print_board(board, ROW, COL);
		ret = is_win(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
	}
	if (ret == '#')
		printf("电脑赢了\n");
	else if (ret == '*')
		printf("玩家赢了\n");
	else if (ret == 'Q')
		printf("平局\n");
}
//判断输赢
//判断输赢的代码要告诉我：电脑赢了？玩家赢了？玩家赢？游戏继续？
//电脑赢:#
//玩家赢:*
//平局：Q
//游戏继续：C

void text()
{
	srand((unsigned int)time(NULL));//time函数返回的是一个整形，但是srand要的是unsigned int
	int a = 0;
	do {
		menu();
		scanf("%d", &a);
		switch (a)
		{
		case 1:
			game();
			break;
		case 0:
			printf("游戏结束");
			break;
		default:
			printf("输入错误，请重新输入\n");

			break;
		}
	} while (a);
}
int main()
{
	text();
	return 0;
}