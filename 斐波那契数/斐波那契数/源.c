#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int fwibo(int n)
{
	if (n <= 2)
	{
		return 1;
	}
	else {
		return fwibo(n - 1) + fwibo(n - 2);

	}
}
int feibo(int m)
{
	int i=1;
	int a = 1, b = 1,c=0 ;
	if (m <= 2)
	{
		return 1;
	}
	else{
		for (i=0;i<m-2;i++)
		{
			c = a + b;
			a = b;
			b = c;
		}
		return c;
	}
}
int main()
{
	int n;
	scanf("%d", &n);
	int c=fwibo(n)		;
	printf("%d\n", c);

	int m;
	scanf("%d", &m);
	int d = feibo(m);
	printf("%d", d);

	return 0;
}