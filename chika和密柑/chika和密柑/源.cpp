#include <iostream>
#include <queue>
#include <vector>
#include <algorithm>
using namespace std;

typedef long long ll;

int n, k;
class cmp {
    bool operator()(pair<int, int>& a, pair<int, int>& b) {
        if (a.second != b.second)return a.second > b.second;
        else return a.first < b.first;
    }
};

int main() {
    cin >> n >> k;
    vector<pair<int, int>>a(n);
    //priority_queue<pair<int, int>, vector<pair<int, int>>, cmp>f;

    for (int i = 0; i < n; i++)
        cin >> a[i].first;
    for (int i = 0; i < n; i++)
        cin >> a[i].second;
    sort(a.begin(), a.end(), [&](pair<int, int>& a, pair<int, int>& b)
        {
            if (a.second != b.second)return a.second > b.second;
            else return a.first < b.first;
        });

    ll r1 = 0, r2 = 0;
    for (int i = 0; i < k; i++) {
        r1 += a[i].first;
        r2 += a[i].second;
    }
    cout << r1 << ' ' << r2;
    return 0;


}