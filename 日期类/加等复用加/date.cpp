#include"date.h"

//ostream& operator<<(ostream& out,const Date& d)
//{
//	out << d._year << "-" << d._month << "-" << d._day ;
//	return out;
//
//}
Date::Date(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;
}
int Date:: GetMonthDay(int year, int month)
{
	static int MonthDay[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	if (month == 2 && (year % 400 == 0 || year % 4 == 0 && year % 100 != 0))
	{
		return 29;
	}
	return MonthDay[month];
}
int Date::GetYearDay(int year)
{
	if (year % 400 == 0 || year % 4 == 0 && year % 100 != 0)
		return 366;
	else
		return 365;
}

bool Date::operator<(const Date& x)const
{
	if (_year < x._year)
	{
		return true;
	}
	else if (_year == x._year && _month < x._month)
	{
		return true;
	}
	else if (_year == x._year && _month == x._month && _day < x._day)
	{
		return true;
	}

	return false;
}
bool Date::operator==(const Date& x)const
{
	return _year == x._year
		&& _month == x._month
		&& _day == x._day;
}
bool Date::operator<=(const Date& x)const
{
	return *this < x || *this == x;
}
bool Date::operator>(const Date& x)const
{
	return !(*this <= x);
}
bool Date::operator>=(const Date& x)const
{
	return !(*this < x);
}
bool Date::operator!=(const Date& x)const
{
	return !(*this == x);
}

Date& Date::operator-=(int day)
{
	if (day < 0)
	{
		return *this += -day;
	}

	_day -= day;
	while (_day <= 0)
	{
		--_month;
		if (_month == 0)
		{
			_month = 12;
			--_year;
		}

		_day += GetMonthDay(_year, _month);
	}

	return *this;
}

Date Date::operator-(int day) const
{
	Date tmp = *this;
	tmp -= day;
	return tmp;
}

Date& Date::operator+=(int day)
{
	_day += day;
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		++_month;
		if (_month == 13)
		{
			++_year;
			_month = 1;
		}
	}

	return *this;
}
Date Date::operator+(int day)
{
	Date tmp(*this);
	tmp += day;
	return tmp;
}
Date& Date::operator++()
{
	*this += 1;
	return *this;
}

Date Date::operator++(int)
{
	Date tmp(*this);
	*this += 1;

	return tmp;
}
Date& Date::operator--()
{
	*this -= 1;
	return *this;
}

Date Date::operator--(int)
{
	Date tmp = *this;
	*this -= 1;
	return tmp;
}
int  Date::operator-(const Date& x)const
{
	Date max = *this;
	Date min = x;
	int flag = 1;
	if (*this < x)
	{
		flag = -1;
		max = x;
		min = *this;

	}
	
	int days = 0;
	while (min != max)
	{
		++min;
		++days;
	}
	return days*flag;
}
ostream& operator<<(ostream& out, const Date& d)
{
	if(d._month>=10)
	{
		if(d._day>10)
			out << d._year << "-" << d._month << "-" << d._day << endl;
		else
			out << d._year << "-" << d._month << "-0" << d._day << endl;
	}
	else
	{
		if (d._day > 10)
			out << d._year << "-0" << d._month << "-" << d._day << endl;
		else
			out << d._year << "-0" << d._month << "-0" << d._day << endl;
	}

	return out;
}
istream& operator>>(istream& in, Date& d)
{
	int year, month, day;
	in >> year >> month >> day;

	if (month > 0 && month < 13
		&& day > 0 && day <= d.GetMonthDay(year, month))
	{
		d._year = year;
		d._month = month;
		d._day = day;
	}
	else
	{
		cout << "�Ƿ�����" << endl;
		assert(false);
	}

	return in;
}
