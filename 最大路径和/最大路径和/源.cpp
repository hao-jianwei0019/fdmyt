/**
 * struct TreeNode {
 *	int val;
 *	struct TreeNode *left;
 *	struct TreeNode *right;
 *	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 * };
 */
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param root TreeNode类
     * @return int整型
     */
    int ans = -1010;
    int maxzishu(TreeNode* root)
    {
        if (root == nullptr)return 0;
        int left = max(0, maxzishu(root->left));
        int right = max(0, maxzishu(root->right));
        ans = max(ans, root->val + left + right);
        return root->val + max(left, right);
    }

    int maxPathSum(TreeNode* root) {
        // write code here
        maxzishu(root);
        return ans;
    }
};
};