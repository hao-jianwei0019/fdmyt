class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int n = nums.size();
        k %= n;
        vector<int>tmp(nums.end() - k, nums.end());
        for (int i = 0; i < n - k; i++)
        {
            tmp.push_back(nums[i]);
        }
        nums = tmp;

    }
};