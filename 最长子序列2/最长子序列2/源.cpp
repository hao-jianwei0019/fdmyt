class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 该数组最长严格上升子序列的长度
     * @param a int整型vector 给定的数组
     * @return int整型
     */
    int  dp[100010] = { 0 };
    int pos = 0;
    int LIS(vector<int>& a) {
        // write code here
        for (auto x : a) {
            if (pos == 0 || x >= dp[pos]) {
                dp[++pos] = x;
            }
            else {
                int l = 1, r = pos;
                while (l < r)
                {
                    int mid = (l + r) / 2;
                    if (dp[mid] >= x)r = mid;
                    else l = mid + 1;
                }
                dp[l] = x;
            }
        }
        return pos;
    }
};