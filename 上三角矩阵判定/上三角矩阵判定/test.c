#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
	int arr[11][11] = {0};
	int side=0;
	scanf("%d", &side);
	int i=0;
	int sum=0;
	for (i = 0; i < side; i++)
	{
		int j = 0;
		for (j = 0; j < side; j++)
		{
			scanf("%d", &arr[i][j]);
		}
	}
	for (i = 0; i < side; i++)
	{
		int j = 0;
		for (j = 0; j < side; j++)
		{
			if (i > j)
			{
				if(arr[i][j])
				sum += 1;
			}
			else
			{
				break;
			}
		}
	}

	if (sum!=0)
	{
		printf("NO");
	}
	else 
	{
		printf("YES");

	}
	return 0;
}