#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <string>
using namespace std;
int nums[128];
bool issu(int x)
{
    if (x <= 1)return false;
    for (int i = 2; i <= sqrt(x); i++)
    {
        if (x % i == 0)return false;
    }
    return true;
}
int main() {
    memset(nums, 0, sizeof nums);
    string s;
    cin >> s;
    for (int i = 0; i < s.size(); i++)
    {
        nums[s[i]]++;
    }
    int minn = 110;
    int maxn = -1;
    for (int i = 0; i < 128; i++)
    {
        if (nums[i])
        {
            minn = min(minn, nums[i]);
            maxn = max(maxn, nums[i]);
        }

    }
    int k = maxn - minn;
    if (issu(k))
    {
        cout << "Lucky Word" << endl;
        cout << k << endl;
    }
    else {
        cout << "No Answer" << endl;
        cout << 0 << endl;
    }
    return 0;

}
// 64 λ������� printf("%lld")