class RandomizedSet {
public:
    vector<int> nums;
    unordered_map<int, int>hash;
    RandomizedSet() {
        srand(time(0));
    }

    bool insert(int val) {
        if (hash.find(val) != hash.end())return false;
        int index = nums.size();
        hash[val] = index;
        nums.push_back(val);
        return true;
    }

    bool remove(int val) {
        if (!hash.count(val))return false;

        int index = hash[val];
        hash[nums[nums.size() - 1]] = index;
        nums[index] = nums[nums.size() - 1];
        nums.pop_back();
        hash.erase(val);
        return true;
    }

    int getRandom() {
        int k = rand() % nums.size();
        return nums[k];
    }
};

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet* obj = new RandomizedSet();
 * bool param_1 = obj->insert(val);
 * bool param_2 = obj->remove(val);
 * int param_3 = obj->getRandom();
 */