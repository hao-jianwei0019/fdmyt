#include<iostream>
#include<vector>
#include<functional>
namespace bit
{
#include<vector>
#include<functional>
	template <class T, class Container = std::vector<T>, class Compare = std::less<T> >
	class priority_queue
	{
	public:
		priority_queue()
		{}

		template <class InputIterator>
		priority_queue(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				c.push_back(*first);
				first++;
			}
			for (int i = (c.size() - 2) / 2; i >= 0; i++)//-1是数组下标里面-1，再-1  /2是找最后一个孩子的爹
			{
				AdjustDown(i);
			}
		}

		//向下调整
		void AdjustDown(int parent)
		{
			size_t child = parent * 2 + 1;
			while (child < c.size())
			{
				if (child + 1 < c.size() && comp(c[child], c[child + 1]))child++;//找到最大的左右孩子之一
				if (comp(c[parent], c[child]))
				{
					std::swap(c[parent], c[child]);
					parent = child;
					child = parent * 2 + 1;
				}
				else break;
			}
		}
		bool empty() const
		{
			return c.empty();
		}

		size_t size() const
		{
			return c.size();
		}

		const T& top() const
		{
			return c[0];
		}

		void push(const T& x)
		{
			c.push_back(x);
			AdjustUp(c.size() - 1);
		}
		void AdjustUp(int child)
		{
			int parent = (child - 1) / 2;
			while (child > 0)
			{
				if(comp(c[parent], c[child]))
				{
					std::swap(c[parent], c[child]);
					child=parent ;
					parent = (child - 1) / 2;
				}
				else break;

			}
		}
		void pop()
		{
			std::swap(c[0], c[c.size() - 1]);
			c.pop_back();
			AdjustDown(0);
		}
	private:
		Container c;
		Compare comp;
	};
};
void test_priority_queue1()
{
	// 默认是大堆 -- less
	//priority_queue<int> pq;

	// 仿函数控制实现小堆
	bit::priority_queue<int, std::vector<int>> pq;

	pq.push(3);
	pq.push(5);
	pq.push(1);
	pq.push(4);

	while (!pq.empty())
	{
		std::cout << pq.top() << " ";
		pq.pop();
	}
	std::cout << std::endl;
}
int main()
{
	test_priority_queue1();
	return 0;
}