/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool _isfind;
    vector<TreeNode*> path_p;
    vector<TreeNode*> path_q;
    void _pathfind(const TreeNode* root, const TreeNode* a, vector<TreeNode*>& _path)
    {
        if (root == nullptr)return;
        if (root == a)
        {
            _isfind = true;
            return;
        }
        _path.push_back(root->left);
        _pathfind(root->left, a, _path);
        if (_isfind)return;
        _path.pop_back();

        _path.push_back(root->right);
        _pathfind(root->right, a, _path);
        if (_isfind)return;
        _path.pop_back();
    }
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        _isfind = false;
        path_p.push_back(root);
        path_q.push_back(root);

        _pathfind(root, p, path_p);
        _isfind = false;
        _pathfind(root, q, path_q);
        int i = 0;
        while (i < path_p.size() && i < path_q.size())
        {
            if (path_p[i] != path_q[i])break;
            i++;
        }
        //cout<<path_p.size()<<" ";


        if (i == path_p.size()) return p;
        if (i == path_q.size()) return q;
        return path_p[i - 1];

    }
};