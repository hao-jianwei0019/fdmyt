#include<vector>
using namespace std;
class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int i = 0;
        int quantity = 0;
        while (i < nums.size())
        {
            if (nums[i] == 0)
            {
                nums.erase(nums.begin() + i);
                quantity++;
            }
            else
            {
                i++;
            }

        }
        while (quantity > 0)
        {
            nums.push_back(0);
            quantity--;
        }
    }
};