class Solution {
public:
    int StrToInt(string str) {
        int num = 0;
        int i = 0;
        int size = 1;
        if (str[i] == '+' || str[i] == '-')
        {
            if (str[i] == '-')
            {
                size = -1;
            }
            i++;
        }
        while (i < str.size())
        {
            if (str[i] < '0' || str[i]>'9')
            {
                return 0;
            }
            num = num * 10 + str[i] - '0';
            i++;
        }
        return num * size;
    }
};