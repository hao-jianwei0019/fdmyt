#include <iostream>
#include <string>
using namespace std;

class Date {
public:
    Date(string c)
    {
        _year = (c[0] - '0')*1000 + (c[1] - '0')*100 + (c[2] - '0')*10 + c[3] - '0';
        _month = 10*(c[4] - '0') + c[5] - '0';
        _day = (c[6] - '0')*10 + c[7] - '0';
    }
    Date(const Date& d)   // 拷贝若传入Date则会死循环
    {
        _year = d._year;
        _month = d._month;
        _day = d._day;
    }
    int GetMonthDay(int year, int month)
    {
        static int MonthDay[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
        if (month == 2 && (year % 400 == 0 || year % 4 == 0 && year % 100 != 0))
        {
            return 29;
        }
        return MonthDay[month];
    }
    int GetYearDay(int year)
    {
        if (year % 400 == 0 || year % 4 == 0 && year % 100 != 0)
            return 366;
        else
            return 365;
    }
    int GetDays()
    {
        int i = 1;
        int sum = 0;
        while (i < _month)
        {
            sum += GetMonthDay(_year, i);
            i++;
        }
        sum += _day;
        return sum;
    }
    bool operator<(const Date& x)const
    {
        if (_year < x._year)
        {
            return true;
        }
        else if (_year == x._year && _month < x._month)
        {
            return true;
        }
        else if (_year == x._year && _month == x._month && _day < x._day)
        {
            return true;
        }

        return false;
    }
    bool operator==(const Date& x)const
    {
        return _year == x._year
            && _month == x._month
            && _day == x._day;
    }
    bool operator!=(const Date& x)const
    {
        return !(*this == x);
    }
    Date& operator+=(int day)
    {
        _day += day;
        while (_day > GetMonthDay(_year, _month))
        {
            _day -= GetMonthDay(_year, _month);
            ++_month;
            if (_month == 13)
            {
                ++_year;
                _month = 1;
            }
        }

        return *this;
    }
    int  operator-(const Date& x)const
    {
        if (*this == x)
            return 0;
        Date max(*this);
        Date min(x);
        int flag = 1;
        if (*this < x)
        {
            flag = -1;
            max = x;
            min = *this;

        }

        int days = 1;
        while (min != max)
        {
            min += 1;
            ++days;
        }
        return days * flag;
    }

private:
    int _year;
    int _month;
    int _day;
};
int main() {
    string c1, c2;
    cin >> c1 >> c2;
    Date d1(c1);
    Date d2(c2);
    cout << d2 - d1;

}
// 64 位输出请用 printf("%lld")