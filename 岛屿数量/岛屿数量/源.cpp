#include <utility>
#include <iostream>
#include<string>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
using namespace std;
const int N = 201;
bool vis[N][N];
int ans;
int dx[4] = { -1,0,1,0 };
int dy[4] = { 0,1,0,-1 };
int h, l;
vector<vector<char>> goard;
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 判断岛屿数量
     * @param grid char字符型vector<vector<>>
     * @return int整型
     */
    void  bfs(int x, int y)
    {
        ans++;
        queue<pair<int, int>> s;
        s.push({ x,y });
        while (!s.empty())
        {
            int x1 = s.front().first;
            int y1 = s.front().second;
            vis[x1][y1] = true;
            s.pop();

            for (int i = 0; i < 4; i++)
            {
                int x2 = x1 + dx[i];
                int y2 = y1 + dy[i];
                if (x2 >= 0 && x2 < h && y2 >= 0 && y2 < l && !vis[x2][y2])
                {
                    if (goard[x2][y2] == '1')
                    {
                        s.push({ x2,y2 });
                    }
                }
            }
        }
    }
    int solve(vector<vector<char> > grid) {
        // write code here
        goard = grid;
        ans = 0;
        memset(vis, false, sizeof vis);
        h = grid.size();
        l = grid[0].size();
        for (int i = 0; i < h; i++)
            for (int j = 0; j < l; j++)
            {
                if (goard[i][j] == '1' && !vis[i][j])
                {
                    bfs(i, j);
                }
            }
        return ans;
    }
}O;
int main()
{
    cout<<O.solve({ {1, 1, 0, 0, 0},{0, 1, 0, 1, 1},{0, 0, 0, 1, 1},{0, 0, 0, 0, 0},{0, 0, 1, 1, 1} });
    return 0;
}