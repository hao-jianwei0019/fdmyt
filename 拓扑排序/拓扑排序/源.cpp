#include <cstring>
#include <iostream>
#include <queue>
#include<vector>
#include <algorithm>
using namespace std;
const int N = 200010;
int e[N], nx[N], itx, h[N];
int vis[N];//��
vector<int>ans;
int n, m;

void addb(int a, int b)
{
    e[itx] = b;
    nx[itx] = h[a];
    h[a] = itx;
    itx++;
    vis[b]++;
}
void bfs()
{
    queue<int>q;
    for (int i = 1; i <= n; i++)
    {
        if (vis[i] == 0)q.push(i);
    }

    while (!q.empty())
    {
        int k = q.front();
        vis[k] = true;
        q.pop();
        ans.push_back(k);
        int n = h[k];
        while (n != -1)
        {
            int num = e[n];
            vis[num]--;
            if (!vis[num])q.push(num);
            n = nx[n];
        }
    }
}
int main() {
    int star = N;
    cin >> n >> m;
    itx = 0;
    memset(h, -1, sizeof h);

    for (int i = 0; i < m; i++)
    {
        int a, b;
        cin >> a >> b;
        addb(a, b);
        star = min(star, min(a, b));
    }
    bfs();
    if (ans.size() == n)
    {
        for (int i = 0; i < n - 1; i++)
        {
            cout << ans[i] << ' ';
        }
        cout << ans[n - 1];
        cout << endl;
    }
    else cout << -1;
    return 0;
}
// 64 λ������� printf("%lld")