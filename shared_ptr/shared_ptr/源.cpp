#include<iostream>
using namespace std;
namespace bit
{
	template<class T>
	class shared_ptr
	{
	public:
		shared_ptr(T* ptr)
			:_ptr(ptr)
			,_pcount(new int(1))

		{}
		shared_ptr(shared_ptr<T>& sp)
			:_ptr(sp._ptr)
			,_pcount(sp._pcount)
		{
			++(*_pcount);
		}
		shared_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			if (sp._ptr != _ptr)
			{
				--(*_pcount);
				if (*pcount == 0)
				{
					delete _ptr;
					delete _pcount;

				}
				_ptr = sp._ptr;
				_pcount = sp._pcount;
				++(*_pcount);
			}
			return *this;
		}
		~shared_ptr()
		{
			if (sp != *this)
			{
				--(*_pcount);
				if (*pcount == 0)
				{
					delete _ptr;
					delete _pcount;

				}
			}
		}
		// 像指针一样使用
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr;
		int* _pcount;
	};
}