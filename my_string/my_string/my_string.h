#pragma once
#include<iostream>
#include<assert.h>
using namespace std;
namespace bit
{

    class string
    {
    public:
        typedef char* iterator;
        typedef const char* const_iterator;
        iterator begin()
        {
            return _str;
        }
        iterator end()
        {
            return _str + _size;
        }
        const_iterator begin() const
        {
            return _str;
        }

        const_iterator end() const
        {
            return _str + _size;
        }
        string(const char* str = "")
        {
            _size = strlen(str);
            _capacity = _size;
            _str = new char[_capacity + 1];
            memcpy(_str, str, _size + 1);
        }
        string(const string& s)
        {
            _str = new char[s._capacity + 1];
            _size = s._size;
            _capacity = s._capacity;
            memcmp(_str, s._str, _size + 1);
        }
        ~string()
        {
            _size = 0;
            _capacity = 0;
            delete[] _str;
            _str = nullptr;
        }
        const char* c_str() const
        {
            return _str;
        }

        size_t size() const
        {
            return _size;
        }

        void swap(string& s)
        {
            std::swap(_str, s._str);
            std::swap(_size, s._size);
            std::swap(_capacity, s._capacity);
        }
        string& operator=(string tmp)
        {
            swap(tmp);
            return *this;
        }
        char& operator[](size_t pos)
        {
            assert(pos < _size);
            return _str[pos];
        }
        const char& operator[](size_t pos) const
        {
            assert(pos < _size);

            return _str[pos];
        }
        void reserve(size_t n)
        {
            if (n > _capacity)
            {
                char* tmp = new char[n + 1];
                memcpy(tmp, _str, _size + 1);
                delete[] _str;
                _str = tmp;
                _capacity = n;
            }
            //理论上这个函数也能缩小capacity
            if (n < _capacity)
            {
                char* tmp = new char[n + 1];
                memcpy(tmp, _str, n);
                tmp[n] = '\0';
                delete[] _str;
                _str = tmp;
                _capacity = n;
            }
        }

        void resize(size_t n, char ch = '\0')
        {
            if (n < _size)
            {
                _size = n;
                _str[_size] = '\0';
            }
            else
            {
                reserve(n);
                for (size_t i = _size; i < n; i++)
                {
                    _str[i] = ch;
                }
                _size = n;
                _str[_size] = '\0';
            }
        }
        void push_back(char c)
        {
            if (_size == _capacity)
            {
                reserve(_capacity == 0 ? 4 : _capacity * 2);
            }
            _str[_size] = c;
            ++_size;
            _str[_size] = '0'; 
        }
        //在字符串尾添加字符串
        void append(const char* str)
        {
            size_t len = strlen(str);
            if (_size + len > _capacity)
            {
                // 至少扩容到_size + len
                reserve(_size + len);
            }

            //strcpy(_str + _size, str);
            memcpy(_str + _size, str, len + 1);

            _size += len;
        }
        string& operator+=(char ch)
        {
            push_back(ch);
            return *this;
        }

        string& operator+=(const char* str)
        {
            append(str);
            return *this;
        }
        //在pos位置插入n个字符
        void insert(size_t pos, size_t n, char ch)
        {
            assert(pos <= _size);
            if (_size + n > _capacity) 
            {
                reserve(_size + n);
            }
            size_t end = _size;
            while (end > pos && end != npos)
            {
                _str[end + n] = _str[end];
                end--;
            }
            for (size_t i = 0; i < n; i++)
            {
                _str[pos + i] = ch;
            }
            _size += n;
        }
        //在pos位置插入字符串
        void insert(size_t pos,  char* ch)
        {
            assert(pos <= _size);
            size_t n = strlen(ch);

            if (_size + n > _capacity)
            {
                reserve(_size + n);
            }
            size_t end = _size;
            while (end > pos && end != npos)
            {
                _str[end + n] = _str[end];
                end--;
            }
            for (size_t i = 0; i < n; i++)
            {
                _str[pos + i] = ch[i];
            }
            _size += n;
        }
        //从pos向后删除len个字符
        void esase(size_t pos,size_t len=npos)
        {
            assert(pos <= _size);
            if (len == npos || pos + len >= _size)
            {
                //_str[pos] = '\0';
                _size = pos;

                _str[_size] = '\0';
            }
            else
            {
                size_t end = pos + len;
                while (end <= _size)
                {
                    _str[pos++] = _str[end++];
                }
                _size -= len;
            }
        }
        void clear()
        {
            _str[0] = '\0';
            _size = 0;
        }
        //从pos开始查找ch
        size_t find(char ch, size_t pos = 0)
        {
            assert(pos < _size);

            for (size_t i = pos; i < _size; i++)
            {
                if (_str[i] == ch)
                {
                    return i;
                }
            }

            return npos;
        }
        size_t find(const char* str, size_t pos = 0)
        {
            assert(pos < _size);

            const char* ptr = strstr(_str + pos, str);
            if (ptr)
            {
                return ptr - _str;
            }
            else
            {
                return npos;
            }
        }
        bool operator==(const string& s)const
        {
            return _size == s._size && memcmp(_str, s._str, _size) == 0;
        }
        bool operator<(const string& s) const
        {
            int ret = memcmp(_str, s._str, _size < s._size ? _size : s._size);

            // "hello" "hello"   false
            // "helloxx" "hello" false
            // "hello" "helloxx" true
            return ret == 0 ? _size < s._size : ret < 0;
        }
        bool operator<=(const string& s) const
        {
            return *this < s || *this == s;
        }

        bool operator>(const string& s) const
        {
            return !(*this <= s);
        }

        bool operator>=(const string& s) const
        {
            return !(*this < s);
        }

        bool operator!=(const string& s) const
        {
            return !(*this == s);
        }
    private:
        size_t _size;
        size_t _capacity;
        char* _str;
    public:
        //const static size_t npos = -1; // 虽然可以这样用，但是不建议
        const static size_t npos;

        //const static double x;
    };
    const size_t string::npos = -1;
};
ostream& operator<<(ostream& out, const string& s)
{
    /*for (size_t i = 0; i < s.size(); i++)
    {
        out << s[i];
    }*/

    for (auto ch : s)
    {
        out << ch;
    }

    return out;
}

istream& operator>>(istream& in, string& s)
{
    s.clear();
    char ch = in.get();
    // 处理前缓冲区前面的空格或者换行
    while (ch == ' ' || ch == '\n')
    {
        ch = in.get();
    }

    //in >> ch;
    char buff[128];
    int i = 0;

    while (ch != ' ' && ch != '\n')
    {
        buff[i++] = ch;
        if (i == 127)
        {
            buff[i] = '\0';
            s += buff;
            i = 0;
        }

        //in >> ch;
        ch = in.get();
    }

    if (i != 0)
    {
        buff[i] = '\0';
        s += buff;
    }

    return in;
}