class Solution {
public:
    int maxProduct(vector<int>& nums) {
        int n = nums.size();
        vector<int>f(n + 1, 0);//���
        vector<int>g(n + 1, 0);//min
        g[0] = 1, f[0] = 1;
        int ret = INT_MIN;
        for (int i = 1; i <= n; i++)
        {
            if (nums[i - 1] >= 0)
            {
                f[i] = max(f[i - 1] * nums[i - 1], nums[i - 1]);
                g[i] = min(g[i - 1] * nums[i - 1], nums[i - 1]);

            }
            else
            {
                f[i] = max(g[i - 1] * nums[i - 1], nums[i - 1]);
                g[i] = min(f[i - 1] * nums[i - 1], nums[i - 1]);
            }
            ret = max(f[i], ret);
        }
        return ret;
    }
};