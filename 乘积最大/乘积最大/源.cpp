#include<iostream>
#include<algorithm>
using namespace std;
const int SIZE = 100010;
const int mod = 1000000009;
int d1[SIZE];//+
int d2[SIZE];//-
int n, k;
int cnt1, cnt2;
bool flag;
long long res = 1;
int main()
{
    cin >> n >> k;
    for (int i = 0; i < n; i++)
    {
        int num;
        cin >> num;
        if (num > 0)d1[cnt1++] = num;
        else if (num < 0)d2[cnt2++] = num;
        else flag = true;
    }
    sort(d1, d1 + cnt1);
    sort(d2, d2 + cnt2);
    if (cnt1 == 0 && k % 2)//全为-且k为基
    {
        for (int i = cnt2 - 1; i > cnt2 - 1 - k; i--) res = (res * d2[i]) % mod;
        if (flag) res = 0;  //有0，最大乘积为0
    }
    else
    {
        int num1 = cnt1 - 1, num2 = 0;
        int cnt = k;
        while (cnt > 1)
        {
            if (num1 >= 1 && (d1[num1] * d1[num1 - 1]) <= (d2[num2] * d2[num2 + 1]))
            {
                res = (res * d2[num2++]) % mod;
                res = (res * d2[num2++]) % mod;
                cnt -= 2;
            }
            else if (num1 == 0 && d1[num1] <= (d2[num2] * d2[num2 + 1]))
            {
                res = (res * d2[num2++]) % mod;
                res = (res * d2[num2++]) % mod;
                cnt -= 2;
            }
            else if (num1 < 0)
            {
                for (; cnt > 0; cnt--)
                {
                    res = (res * d2[num2++]) % mod;
                }
            }
            else
            {
                res = (res * d1[num1--]) % mod;
                cnt -= 1;
            }
        }
        if (cnt == 1) res = (res * d1[num1]) % mod;
    }
    if (res >= 0) cout << res % mod;
    else   cout << 0 - ((0 - res) % mod);

    return 0;
}