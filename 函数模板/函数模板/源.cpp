#include <iostream>
using namespace std;
template<typename T>
T sum(T a, T b)
{
	return a + b;
}
//其实就是重新定义了一种数组类型，他可以是任意一种形式
//这里将T定义为一种新的类型

int main()
{ 
	cout << sum(2, 4) << "  " << sum(2.31, 6.70);
	return 0;
}