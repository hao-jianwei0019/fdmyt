#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
#include<stack>
#include<unordered_map>

typedef long long ll;
class lei
{
public:
    lei(ll x1, ll y1, ll z1)
        :x(x1), y(y1), r(z1)
    {}
    ll x;
    ll y;
    int r;
};
using namespace std;
int ans = 0;
int N, M;
unordered_map<string, int> nums;

string change1(ll x, ll y)
{
    string tmp = to_string(x) + 'Q' + to_string(y);
    return tmp;
}
bool check1(int x1, int y1, int r, int x, int y) {
    int d = (x1 - x) * (x1 - x) + (y1 - y) * (y1 - y);
    return d <= r * r;
}
bool check2(ll x, ll y)
{
    string tmp = change1(x, y);
    return nums.count(tmp) > 0;
}


void bfs(ll x, ll y, int z)
{
    stack<lei> q;
    q.push({ x,y,z });
    while (!q.empty())
    {
        int a = q.top().x;
        int b = q.top().y;
        int c = q.top().r;
        q.pop();
        for (int i = a - c; i <= a + c; i++)
            for (int j = b - c; j <= b + c; j++)
            {
                if (check1(i, j, c, a, b))
                {
                    string tmp = change1(i, j);
                    if (nums.count(tmp) > 0)
                    {
                        if (nums[tmp])
                        {
                            q.push({ i,j,nums[tmp] });
                            nums.erase(tmp);
                            //nums[tmp] = 0;
                            ans++;
                        }

                    }
                }
            }


    }


}
int main()
{
    cin >> N >> M;
    for (int i = 0; i < N; i++)
    {
        string a, b;
        int c;
        cin >> a >> b >> c;
        string s1;
        s1 += a + 'Q' + b;
        nums[s1] = max(nums[s1],c);
    }
    ll x, y;
    int r;
    for (int i = 0; i < M; i++)
    {
        cin >> x >> y >> r;
        bfs(x, y, r);
    }
    cout << ans;
    return 0;
}