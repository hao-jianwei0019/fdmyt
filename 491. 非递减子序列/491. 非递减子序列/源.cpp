class Solution {
public:
    vector<vector<int>> findSubsequences(vector<int>& nums) {
        int n = nums.size();
        vector<vector<int>>tmp;
        vector<vector<int>>ans;

        for (int i = 0; i < n; i++)
        {
            int val = nums[i];
            int z = tmp.size();
            for (int j = 0; j < z; j++)
            {
                if (*(tmp[j].end() - 1) <= val)
                {
                    vector<int>tmp1 = tmp[j];
                    tmp1.push_back(val);
                    ans.push_back(tmp1);
                    tmp.push_back(tmp1);
                }
            }
            tmp.push_back(vector<int>(1, val));
        }
        sort(ans.begin(), ans.end());
        auto last = unique(ans.begin(), ans.end());
        ans.erase(last, ans.end());
        return ans;
    }
};