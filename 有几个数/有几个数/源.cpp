#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<iostream>
using namespace std;
const int maxn = 1e4 + 10;
int n, q, w;
int a[maxn];
int dp[maxn];
int last[maxn], sumDis[maxn], sumPostfix;
int main()
{
	scanf("%d%d", &n, &q) != EOF;
	
		memset(last, -1, sizeof(last));
		memset(sumDis, 0, sizeof(sumDis));
		dp[1] = n; 
		sumPostfix = 0; 
		for (int i = 0; i < n; i++)
		{
			scanf("%d", &a[i]); 
			sumDis[i - last[a[i]]]++;
			last[a[i]] = i;

		}
		for (int i = n - 1; i; i--)
			sumDis[i] += sumDis[i + 1];
		for (int i = 2; i <= n; i++)
		{
			sumPostfix += last[a[n - i + 1]] == n - i + 1;
			dp[i] = dp[i - 1] + sumDis[i] - sumPostfix;
		}
	
		while (q--)
		{
			scanf("%d", &w);
			printf("%d\n", dp[w]);
		}
	
		
	return 0;
}