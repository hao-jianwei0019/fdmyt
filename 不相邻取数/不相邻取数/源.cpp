#include<string>
#include <algorithm>
#include <iostream>
using namespace std;
typedef long long  ll;
const int N = 1e6 + 10;
ll f[N][2];
int main() {
    int n;
    cin >> n;
    f[0][0] = f[0][1] = 0;
    for (int i = 1; i <= n; i++)
    {
        int tmp;
        cin >> tmp;
        f[i][0] = max(f[i - 1][0], f[i - 1][1]);
        f[i][1] = f[i - 1][0] + tmp;
    }
    cout << max(f[n][0], f[n][1]);
    return 0;
}
// 64 λ������� printf("%lld")