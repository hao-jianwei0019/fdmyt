#pragma once
#include<iostream>
using namespace std;

template <class K, class V>
struct BSTreeNode
{
	BSTreeNode<K,V>* _left;
	BSTreeNode<K,V>* _right;
	K _key;
	V _val;
	BSTreeNode(const K& key,const V& val)//可能存自定义类型，所以用引用节省空间提升效率
		:_left(nullptr),
		_right(nullptr),
		_key(key),
		_val(val)
	{}
};
template<class K,class V>
class BSTree
{
	typedef BSTreeNode<K,V> Node;
public:
	BSTree()
		:_root(nullptr)
	{}
	bool Insert(const K& key, const V& val)
	{
		if (_root == nullptr)
		{
			_root = new Node(key,val);
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_key > key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		} 

		cur = new Node(key,val);
		if (parent->_key < key)
		{
			parent->_right = cur;
		}
		else
		{
			parent->_left = cur;
		}

		return true;
	}
	Node* Find(const K& key)
	{
		Node* cur = _root;
		while (cur)
		{
			if (key < cur->_key)
			{
				cur = cur->_left;
			}
			else if(key>cur->_key)
			{
				cur = cur->_right;
			}
			else
			{
				return cur;
			}
		}
		return nullptr;
	}
	bool FindR(const K& key)
	{
		return _FindR(_root, key);
	}
	bool Erase(const K& key)
	{
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			//先找到这个要删除的元素的位置
			if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_key > key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else // 找到了
			{
				if (cur->_left == nullptr)
				{
					if (cur == _root)_root = _root->_right;
					else
					{
						if (parent->_right == cur)parent->_right = cur->_right;
						else parent->_left = cur->_right;
					}
				}
				if (cur->_right == nullptr)
				{
					if (cur == _root)_root = _root->_left;
					else
					{
						if (parent->_right == cur)parent->_right = cur->_left;
						else parent->_left = cur->_left;
					}
				}
				else
				{
					//找替换节点，左面最大或者右面最小
					parent = cur;
					Node* leftMax = cur->_left;
					while (leftMax->_right)
					{
						parent = leftMax;
						leftMax = leftMax->_right;
					}

					std::swap(cur->_key, leftMax->_key);
					std::swap(cur->_val, leftMax->_val);
					if (parent->_left == leftMax)parent->_left = leftMax->_left;
					else parent->_right= leftMax->_left;
				}
				delete cur;
				return true;
			}
		}
		return false;
	}
	
	void InOrder()//中序遍历
	{
		_InOrder(_root);
		cout << endl;
	}
private:
	bool _FindR(Node* root,const K& key)//带R表示是递归实现
	{
		if (root == nullptr)
		{
			return false;
		}
		if (root->_key < key)
		{
			return _FindR(root->_right, key);
		}
		else if (root->_key>key)
		{
			return _FindR(root->_left, key);
		}
		else
		{
			return true;
		}
	}
	void _InOrder(Node* root)
	{
		if (root == NULL)
		{
			return;
		}

		_InOrder(root->_left);
		cout << root->_key << " :"<<root->_val;
		_InOrder(root->_right);
	}
private:
	Node* _root;
};
