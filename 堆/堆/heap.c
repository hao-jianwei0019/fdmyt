#include "heap.h"
// 堆的构建
void HeapCreate(Heap* hp,  int n)
{
	assert(hp);
	hp->_a =(HPDataType*) malloc(sizeof(HPDataType) * n);
	if (hp->_a == NULL)
	{
		perror("malloc");
		return;
	}
	hp->_size = 0;
	hp->_capacity = n;


}
// 堆的销毁
void HeapDestory(Heap* hp)
{
	assert(hp && hp->_a);
	free(hp->_a);
	hp->_capacity = hp->_size = 0;
}
//交换位置
void Swap(HPDataType* a, HPDataType* b)
{
	HPDataType tmp = *a;
	*a = *b;
	*b = tmp;
}
//向上调整
void Adjustup(HPDataType* a,int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}
//向下调整
void Adjustdown(HPDataType* a, int n,int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		// 选出左右孩子中大的那一个
		if (child + 1 < n && a[child + 1] > a[child])//确保右孩子不越界
		{
			++child;
		}

		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}
// 堆的插入
void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	//扩容

	if (hp->_capacity == hp->_size)
	{
		HPDataType* tmp =(HPDataType*) realloc(hp->_a, sizeof(HPDataType) * hp->_capacity * 2);
		if (tmp == NULL)
		{
			perror("realloc");
			return;
		}
		hp->_capacity *= 2;
		hp->_a = tmp;
	}
	hp->_a[hp->_size] = x;
	Adjustup(hp->_a, hp->_size);
	hp->_size++;

}
// 堆的删除
void HeapPop(Heap* hp)
{
	assert(hp);
	assert(!HeapEmpty(hp));
	Swap(&hp->_a[0], &hp->_a[hp->_size - 1]);
	hp->_size--;
	Adjustdown(hp->_a, hp->_size, 0);
}
// 取堆顶的数据
HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	assert(!HeapEmpty(hp));
	return hp->_a[0];
}
// 堆的数据个数
int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->_size;

}
// 堆的判空
bool HeapEmpty(Heap* hp)
{
	assert(hp);
	return hp->_size == 0;

}
