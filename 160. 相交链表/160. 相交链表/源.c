/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
    struct ListNode* tailA = headA;
    struct ListNode* tailB = headB;
    int lenA = 0, lenB = 0;
    while (tailA->next)
    {
        tailA = tailA->next;
        lenA++;

    }
    while (tailB->next)
    {
        tailB = tailB->next;
        lenB++;

    }
    if (tailA != tailB)
        return NULL;
    int gap = abs(lenA - lenB);
    struct ListNode* longList = headA, * shortList = headB;
    if (lenA < lenB)
    {
        longList = headB;
        shortList = headA;
    }
    while (gap--)
    {
        longList = longList->next;
    }
    while (longList != shortList)
    {
        longList = longList->next;
        shortList = shortList->next;


    }
    return longList;

}