#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void menu()//当void做函数返回类型时，vido指定函数不返回值
{
	printf("*************************\n");
	printf("****** 1.开始游戏  *****\n"); 
	printf("****** 0.结束游戏  *****\n");
	printf("************************\n");
}
void game()
{
	int num = rand() % 100 + 1;
	int guess = 0;
	while (1)
	{
		printf("请猜数字：");
		scanf("%d", &guess);
		if (guess < num)
		{
			printf("猜小了\n");
		}
		else if (guess > num)
		{
			printf("猜大了\n");
		}
		else
		{
			printf("恭喜你，猜对了\n");
			break;
		}
	}

}

int main()
{
	srand((unsigned int)time(NULL));
	int ant = 0;
	do {
		menu();//创建菜单
		printf("请选择\n");
		scanf("%d", &ant);
		switch (ant)
		{
		case 1:	
			game();
			break;
		case 0:
			printf("游戏结束\n");
			break;
		default:
			printf("输错了，请重新选择\n");
			break;
		}
	} while (ant);
	return 0;
}