#include <iostream>
using namespace std;

#define N 8   //设定待排序序列中的元素个数

void insertion_sort(int list[]) {
    int insert;//存放要插入的数
    int p;//存放要插入的位置下标position
    int i=0;

    //从第 2 个元素（下标为 1）开始遍历
    for (i = 1; i < N; i++) {
        insert = list[i];
        // 记录目标元素所在的位置，从此位置向前开始遍历
        p = i;

        // 从 position 向前遍历，找到目标元素的插入位置
        while (p > 0 && list[p - 1] > insert) {
            //position 处的元素向后移动一个位置
            list[p] = list[p - 1];
            p--;
        }
        //将目标元素插入到指定的位置
        if (p != i) {
            list[p] = insert;
        }
    }
}

int main() {
    int i;
    int list[N] = { 94,949,49,64,6,16,311,151};
    insertion_sort(list);
    //输出 list 数组中已排好序的序列
    for (i = 0; i < N; i++) {
        cout<<list[i]<<endl;
    }
}