class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param schedule int整型vector<vector<>>
     * @return bool布尔型
     */
    bool hostschedule(vector<vector<int> >& schedule) {
        // write code here
        //auto func=[](const vector<int>& x1,const vector<int>& x2)->bool
        //{
          //  return x1[0]>x2[0];
        //};
        sort(schedule.begin(), schedule.end());
        for (int i = 1; i < schedule.size(); i++)
        {
            if (schedule[i][0] < schedule[i - 1][1])return false;
        }
        return true;
    }
};