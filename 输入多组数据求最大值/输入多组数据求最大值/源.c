#define  _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
    int d1, d2;
    char ch;
    while (scanf("%d %d", &d1, &d2) != EOF)
    {
        getchar();
        if (d1 > d2)
            ch = '>';
        else if (d1 < d2)
            ch = '<';
        else
            ch = '=';
        printf("%d%c%d\n", d1, ch, d2);
    }
    return 0;
}