#include <stdio.h>
int is_leap_year(int n)
{
	if (n % 4 == 0 && n % 1000 != 0 || n % 400 == 0)
	{
		return 1;
	}
	return 0;
}
int main()
{
	int y = 0;
	int num = 0;
	for (y = 1000; y <= 2000; y++)
	{
		if (is_leap_year(y) == 1)
		{
			num++;
			printf("%d  ", y);
		}
	}
	printf("\n%d", num);
	return 0;
}