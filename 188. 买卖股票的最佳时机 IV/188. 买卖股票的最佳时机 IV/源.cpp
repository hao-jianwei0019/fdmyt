#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    int maxProfit(int k, vector<int> prices) {
        int n = prices.size();
        k = min(k, n / 2);
        vector<vector<int>> fdp(n, vector<int>(k + 1, -0x3f3f3f3f));//买入,最少买入0次，所以应该是k+1
        vector<vector<int>> gdp(n, vector<int>(k + 1, -0x3f3f3f3f));//卖出
        gdp[0][0] = 0;
        fdp[0][0] = -prices[0];
        for (int i = 0; i < k; i++)
        {
            for (int j = 1; j < n; j++)
            {
                fdp[j][i] = max(fdp[j - 1][i], gdp[j - 1][i] - prices[j]);
                gdp[j][i] = gdp[j - 1][i];
                if (i > 0) gdp[j][i] = max(gdp[j - 1][i], fdp[j - 1][i - 1] + prices[j]);
            }
        }
        int ret = 0;
        for (int i = 0; i <= k; i++)
        {
            ret = max(ret, gdp[n - 1][i]);
        }
        return ret;
    }
}o;
int main()
{
    o.maxProfit(2,{ 3,3,5,0,0,3,1,4 });
}