#include <iostream>
using namespace std;
typedef long long ll;
const int N = 1e6 + 10;
ll hang[N];
ll lie[N];
int main() {
    int a, b;
    cin >> a >> b;
    ll nums[a][b];

    for (int i = 0; i < a; i++)
        for (int j = 0; j < b; j++)
        {
            ll tmp;
            cin >> tmp;
            nums[i][j] = tmp;
            hang[i] += tmp;
            lie[j] += tmp;
        }
    for (int i = 0; i < a; i++)
    {
        for (int j = 0; j < b; j++)
        {
            cout << hang[i] + lie[j] - nums[i][j] << ' ';
        }
        cout << endl;
    }

    return 0;
}
// 64 λ������� printf("%lld")