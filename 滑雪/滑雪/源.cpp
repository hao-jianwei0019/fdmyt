#include <cstring>
#include <iostream>
#include <queue>
using namespace std;
const int N = 105;
int board[N][N];
int n, m;
int dp[N][N];
int ans;
int dx[4] = { -1, 0, 1, 0 };
int dy[4] = { 0, 1, 0, -1 };
int dfs(int a, int b) {
    if (dp[a][b] != -1)return dp[a][b];
    int ret = 0;

    for (int i = 0; i < 4; i++)
    {
        int x = a + dx[i], y = b + dy[i];
        if (x < 0 || x >= n || y < 0 || y >= m)continue;
        if (board[a][b] > board[x][y])
        {
            ret = max(ret, dfs(x, y));
        }

    }
    dp[a][b] = ret + 1;
    return ret + 1;
}
int main() {
    cin >> n >> m;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
        {
            cin >> board[i][j];
        }
    memset(dp, -1, sizeof dp);
    ans = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            ans = max(ans, dfs(i, j));
        }
    }
    cout << ans << endl;
    return 0;
}
// 64 λ������� printf("%lld")