#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;
int h, l;
const int N = 501;
int board[N][N];
int dp[N][N];
int main() {
    cin >> h >> l;
    for (int i = 0; i < h; i++)
        for (int j = 0; j < l; j++) {
            char tmp;
            cin >> tmp;
            if (tmp == 'l')board[i][j] = 4;
            else if (tmp == 'o')board[i][j] = 3;
            else if (tmp == 'v')board[i][j] = 2;
            else if (tmp == 'e')board[i][j] = 1;
            else board[i][j] = 0;
        }
    memset(dp, 0, sizeof dp);
    for (int i = 1; i <= h; i++)
        for (int j = 1; j <= l; j++) {
            dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + board[i - 1][j - 1];
        }
    cout << dp[h][l];
    return 0;
}
// 64 λ������� printf("%lld")