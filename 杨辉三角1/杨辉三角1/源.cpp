#include <cstdio>
#include <iostream>
#include<cstring>
using namespace std;
int n;
const int N = 31;
int board[N][N];
void solve()
{
    memset(board, 0, sizeof board);
    for (int i = 0; i < 31; i++)
    {
        board[i][0] = 1;
    }
    for (int i = 1; i < 31; i++)
        for (int j = 1; j <= i; j++)
        {
            board[i][j] = board[i - 1][j] + board[i - 1][j - 1];
        }

}
int main() {
    cin >> n;
    solve();
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j <= i; j++)
        {
            printf("%5d", board[i][j]);
        }
        cout << endl;
    }
    return 0;
}
// 64 λ������� printf("%lld")