#include <algorithm>
#include <iostream>
using namespace std;
const int N = 1e5 + 10;
int num1[N], num2[N];
int n, m;
int main() {
    cin >> n >> m;
    for (int i = 0; i < n; i++)
        cin >> num1[i];
    for (int i = 0; i < m; i++)
        cin >> num2[i];
    sort(num1, num1 + n);
    sort(num2, num2 + m);

    int l = 0;
    int r = 0;
    while (l < n && r < m) {
        if (num1[l] < num2[r]) {
            cout << num1[l] << ' ';
            l++;
        }
        else if (num1[l] > num2[r]) {
            cout << num2[r] << ' ';
            r++;
        }
        else {
            cout << num1[l] << ' ';
            l++, r++;
        }
    }
    while (l < n) {
        if (l + 1 == n) {
            cout << num1[l];
            break;
        }
        cout << num1[l] << ' ';
        l++;
    }
    while (r < m) {

        if (r + 1 == n) {
            cout << num2[r];
            break;
        }
        cout << num2[r] << ' ';
        r++;
    }
    return 0;
}
// 64 λ������� printf("%lld")