#include <stdio.h>
void All(int m);

int main()
{
	int x = 199;
	All(x);
	return 0;
}

void All(int m)
{
	int i = 0;
	i = 31;
	printf("二进制：");
	while (i >= 0)
	{
		printf("%d ", (m >> i) & 1);
		i--;
	}
	printf("\n奇数位：");
	for (i = 30; i >= 0; i -= 2)
	{
		printf("%d ", (m >> i) & 1);
	}
	printf("\n偶数位：");
	for (i = 31; i >= 1; i -= 2)
	{
		printf("%d ", (m >> i) & 1);
	}

}
