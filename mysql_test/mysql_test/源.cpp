#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>
#include <cppconn/exception.h>
#include <iostream>

int main() {
    try {
        // 创建驱动程序实例
        sql::mysql::MySQL_Driver* driver = sql::mysql::get_mysql_driver_instance();

        // 通过驱动程序创建连接
        std::unique_ptr<sql::Connection> conn(driver->connect("tcp://127.0.0.1:3306", "username", "password"));

        // 连接到具体的数据库
        conn->setSchema("test_db");

        // 创建语句对象
        std::unique_ptr<sql::Statement> stmt(conn->createStatement());

        // 执行查询并获取结果集
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery("SELECT id, name FROM test_table"));

        // 遍历结果集并输出结果
        while (res->next()) {
            std::cout << "ID: " << res->getInt("id");
            std::cout << ", Name: " << res->getString("name") << std::endl;
        }
    }
    catch (sql::SQLException& e) {
        std::cerr << "SQLException: " << e.what() << std::endl;
        std::cerr << "SQLState: " << e.getSQLState() << std::endl;
    }

    return 0;
}
