#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
	int a = 0, b = 0;
	printf("请输入两个整数：\n");
	scanf("%d%d", &a, &b);
	int max = a < b ? a : b;
	for (;; max--)
	{
		if (a % max == 0 && b % max == 0)
			break;
	}
	printf("最大共因数是%d\n", max);
	printf("最小公倍数是%d", a * b / max);

	return 0;
}