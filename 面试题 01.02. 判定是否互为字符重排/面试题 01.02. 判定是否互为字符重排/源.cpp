class Solution {
public:
    bool CheckPermutation(string s1, string s2) {
        if (s1.size() != s2.size())return false;
        int hash[128];
        for (auto& ch : s1)hash[ch]++;
        for (auto& ch : s2)
        {
            if (hash[ch]-- <= 0)return false;
        }
        return true;
    }
};