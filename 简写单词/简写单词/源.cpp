#include <iostream>
#include<cstring>
#include<string>
using namespace std;
string s;
string ans;
char to_big(char a)
{
    if (a >= 'A' && a <= 'Z')return a;
    else return a - ('a' - 'A');
}
void solve()
{
    int n = s.size();
    if (n == 0)return;
    ans += to_big(s[0]);
    int i = 0;
    while (i < n)
    {
        if (s[i] == ' ')
        {
            if (i + 1 >= n)return;
            ans += to_big(s[i + 1]);
        }
        i++;
    }
}
int main() {
    getline(cin, s);
    solve();
    cout << ans;
    return 0;

}
// 64 λ������� printf("%lld")