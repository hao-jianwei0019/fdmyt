void revolve(int* arr, int begin, int end)
{
    int sum;
    while (begin < end)
    {
        sum = *(arr + begin);
        *(arr + begin) = *(arr + end);
        *(arr + end) = sum;
        begin++;
        end--;
    }

}
void rotate(int* nums, int numsSize, int k) {
    if (numsSize < k)
        k %= numsSize;
    revolve(nums, 0, numsSize - k - 1);
    revolve(nums, numsSize - k, numsSize - 1);
    revolve(nums, 0, numsSize - 1);

}