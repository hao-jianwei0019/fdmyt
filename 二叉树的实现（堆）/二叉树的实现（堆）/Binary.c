#include "Binary.h"
BTNode* BinaryTreeCreate(BTDataType* a, int* pi)
{
	if (a[*pi] == '#')
	{
		(*pi)++;
		return NULL;
	}
	
	BTNode* root = (BTNode*)malloc(sizeof(BTNode));
	if (root == NULL)
	{
		perror("malloc");
		return;
	}
	root->_data = a[(*pi)++];
	root->_left=BinaryTreeCreate(a, pi);
	root->_right= BinaryTreeCreate(a,pi);
	return root;

}

// 二叉树销毁
void BinaryTreeDestory(BTNode** root)
{
	if (*root == NULL)
	{
		return;

	}
	BTNode* left = (*root)->_left;

	BTNode* right =(*root)->_right;
	free(*root);
	root = NULL;
	BinaryTreeDestory(&left);
	BinaryTreeDestory(&right);

}
// 二叉树节点个数
int BinaryTreeSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	int left = (BinaryTreeSize(root->_left)) ;
	int right = (BinaryTreeSize(root->_right)) ;
	return left + right + 1;
}
// 二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	if (root->_left == NULL && root->_right == NULL)
	{
		return 1;

	}
	int left = (BinaryTreeLeafSize(root->_left));
	int right = (BinaryTreeLeafSize(root->_right));
	return left + right ;
}
// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if (root == NULL);
		return NULL;
	if (k == 1)
	{
		return 1;
	}

	int left = BinaryTreeLevelKSize(root->_left,k-1);
	int right= BinaryTreeLevelKSize(root->_right, k - 1);
	return left + right;
	
}
// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
	{
		return NULL;
	}
	if (root->_data == x)
		return root;
	BTNode* left = BinaryTreeFind(root->_left, x);
	if (left != NULL)
		return left;
	BTNode* right = BinaryTreeFind(root->_right, x);
	if (right != NULL)
		return right;
	return NULL;
}
// 二叉树前序遍历 
void BinaryTreePrevOrder(BTNode* root)
{
	if (root == NULL)
		return;
	printf("%c ", root->_data);
	BinaryTreePrevOrder(root->_left);
	BinaryTreePrevOrder(root->_right);
}
// 二叉树中序遍历
void BinaryTreeInOrder(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreeInOrder(root->_left);
	printf("%c ", root->_data);

	BinaryTreeInOrder(root->_right);
}
// 二叉树后序遍历
void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
		return;
	printf("%c ", root->_data);
	BinaryTreePostOrder(root->_left);
	BinaryTreePostOrder(root->_right);

}
// 层序遍历
void BinaryTreeLevelOrder(BTNode* root)
{
	Queue q;
	QueueInit(&q);

	if (root)
		QueuePush(&q, root);
	while (!QueueEmpty(&q))
	{
		BTNode* ret = QueueFront(&q);
		QueuePop(&q);
		printf("%c ", ret->_data);
		if(ret->_left)
			QueuePush(&q, ret->_left);
		if (ret->_right)
			QueuePush(&q, ret->_right);
	}
	QueueDestroy(&q);
}
// 判断二叉树是否是完全二叉树
bool BinaryTreeComplete(BTNode* root)
{
	Queue q;
	QueueInit(&q);

	if (root)
		QueuePush(&q, root);
	while (!QueueEmpty(&q))
	{
		BTNode* ret = QueueFront(&q);
		
		QueuePop(&q);
		if (ret == NULL)
			break;
		QueuePush(&q, ret->_left);
		QueuePush(&q, ret->_right);
	}
	if (!QueueEmpty(&q))
	{
		BTNode* ret = QueueFront(&q);

		QueuePop(&q);
		if (ret)
		{
			QueueDestroy(&q);
			return false;
		}

	}
	QueueDestroy(&q);
	return true;
}