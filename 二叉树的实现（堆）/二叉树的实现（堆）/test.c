#include "Binary.h"

int main()
{
	char arr[100];
	scanf("%s", arr);
	int i = 0;

	BTNode* root = BinaryTreeCreate(arr, &i);
	int d = BinaryTreeLeafSize(root);
	printf("%d",d);
	printf("%p", BinaryTreeFind(root, 'D'));
	BinaryTreePrevOrder(root);
	printf("\n");
	BinaryTreeLevelOrder(root);
	printf("%d", BinaryTreeComplete(root));
	BinaryTreeDestory(&root);
}