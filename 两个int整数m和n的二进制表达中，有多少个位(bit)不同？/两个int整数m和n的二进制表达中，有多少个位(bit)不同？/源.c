#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
	int m;
	int n;
	int count=0;
	scanf("%d%d", &m, &n);
	int c = m ^ n;
	while (c != 0)
	{
		if ((c & 1) == 1)
			count++;
		c=c >> 1;
	}
	printf("%d", count);
	return 0;
}