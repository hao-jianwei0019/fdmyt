class Solution {
public:
    string removeKdigits(string num, int k) {
        int n = num.size();
        if (n <= k)return "0";
        int minc = 1;
        while (k--)
        {
            while (num.size() > 0 && num[0] == '0')num.erase(num.begin());
            n = num.size();
            //cout<<n<<' ';
            if (n == 0 || n == 1)return "0";
            //if(n==2&&num[1]=='0')return "0";

            while (minc < n && num[minc] >= num[minc - 1])minc++;

            if (num[minc] < num[minc - 1])
            {
                num.erase(num.begin() + minc - 1);

            }
            else num.erase(num.end() - 1);
            if (minc > 1)minc--;
        }

        while (num.size() > 0 && num[0] == '0')num.erase(num.begin());
        if (num.size() == 0)return "0";
        return num;
    }
};