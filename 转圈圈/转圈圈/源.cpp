#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
long long h[100010];
int main()
{
    int T;
    cin >> T;//输入次数
    while (T--)
    {
        int n;
        
        long long E, R;
        cin >> n;
        
        for (int i = 0; i < n; i++)
        {
            cin >> h[i];
        }
        cin >> E >> R;
        sort(h, h+n);
        long long s = 0;//累计伤害
        int cnt = 0;//转圈圈的数
        int ansE = 0,ansC=0;
        int middea = 0;//小于50
        while (s < h[n - 1])
        {
            if (cnt == 0)//不转圈
            {
                ansE++;
                s += E;
            }
            else
            {
                while (cnt--)
                {
                    s += R;
                    ansC++;
                    if (s >= h[n - 1])break;
                    
                }
            }

            int tmp1 = upper_bound(h, h + n, 2 * s) - h;//找到血量小于50%的
            cnt = tmp1 - middea;
            middea = tmp1;
        }
        cout << ansE << ' ' << ansC << endl;
    }

}
