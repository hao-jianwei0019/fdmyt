class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        if (nums.size() == 1)return nums.size();
        auto fast = nums.begin() + 1;
        auto slow = nums.begin();
        while (fast != nums.end())
        {
            if (*fast == *slow)
            {
                nums.erase(fast);
            }
            else
            {
                fast++;
                slow++;
            }
        }
        return nums.size();
    }
};