class Solution {
public:
    int massage(vector<int>& nums) {
        int n = nums.size();
        if (n == 0)return 0;
        vector<int>dpf(n);//选
        vector<int>dpg(n);//不选
        dpf[0] = nums[0];
        dpg[0] = 0;
        for (int i = 1; i < n; i++)
        {
            //如果选，则上一个必定不选，如果不选，上一个可能选可能不选
            dpf[i] = dpg[i - 1] + nums[i];
            dpg[i] = std::max(dpf[i - 1], dpg[i - 1]);
        }
        int max = std::max(dpf[n - 1], dpg[n - 1]);
        return max;
    }
    int deleteAndEarn(vector<int>& nums) {
        //map不能保证数字的连续还得判断是不是n+1;
        const int N = 10001;
        vector<int>dp(N, 0);
        for (auto& ch : nums)
        {
            dp[ch] += ch;
        }
        int max = massage(dp);
        return max;

    }
};