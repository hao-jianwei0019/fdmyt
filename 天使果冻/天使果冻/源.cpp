#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1e5 + 10;
int smax[N];
int fir[N];
int main() {


    int n;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        int tmp;
        cin >> tmp;
        if (i == 0)
        {
            fir[i] = tmp;
            smax[i] = -1;
        }
        else
        {
            int lfmax = fir[i - 1];
            fir[i] = max(fir[i - 1], tmp);
            smax[i] = max(smax[i - 1], min(tmp, fir[i - 1]));
        }
    }
    int q;
    cin >> q;
    while (q--)
    {
        int tmp;
        cin >> tmp;
        cout << smax[tmp-1] << endl;
    }
    return 0;
}
// 64 λ������� printf("%lld")