#define _CRT_SECURE_NO_WARNINGS 1
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <limits.h>
enum State
{
	VALID,
	INVALID
}state = INVALID;//默认表示非法

int my_atoi(const char* str)
{
	assert(str);
	if (*str == '\0')
	{
		return 0;
	}
	//跳过空白字符
	while (isspace(*str))
	{
		str++;
	}
	int flag = 1;
	if (*str == '+')
	{
		str++;
	}
	else if (*str == '-')
	{
		flag = -1;
		str++;
	}
	long long ret = 0;
	while (*str)
	{
		if (isdigit(*str))
		{
			ret = ret * 10 + flag * (*str - '0');
			if (ret > INT_MAX)
			{
				return INT_MAX;
			}
			else if (ret < INT_MIN)
			{
				return INT_MIN;
			}
		}
		else
		{
			return (int)ret;
		}
		str++;
	}

	state = VALID;
	return (int)ret;
}
int main()
{
	int ret = my_atoi("123411111111111111115");

	if (state == VALID)
		printf("%d\n", ret);
	else
		printf("非法字符串转换,%d\n", ret);

	return 0;
}