/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */
bool issame(struct TreeNode* root, int k)
{
    if (root == NULL)
        return true;
    if (root->val != k)
        return false;

    bool ret = issame(root->left, k);
    if (ret == false)
        return ret;
    ret = issame(root->right, k);
    return ret;
}

bool isUnivalTree(struct TreeNode* root) {
    if (root == NULL)
        return true;
    int k = root->val;
    bool ret = issame(root, k);
    return ret;
}