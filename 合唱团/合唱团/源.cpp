#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll INF = LLONG_MAX;
const ll MINF = LLONG_MIN;

void solve(int n, int K, int d, vector<int>& arr) {
    vector<vector<ll>> dpMAX(n, vector<ll>(K + 1, 0)); // 最大值
    vector<vector<ll>> dpMIN(n, vector<ll>(K + 1,
        0)); // 最小值，由于存在负数，否则直接计算最大值即可

    for (int i = 0; i < n;
        ++i) { // 初始化k=1时，以当前元素结尾的最大最小值
        dpMAX[i][1] = arr[i];
        dpMIN[i][1] = arr[i];
    }

    ll ans = MINF;

    for (int i = 0; i < n; ++i) {
        for (int k = 2; k <= min(i + 1, K); ++k) {
            for (int j = max(0, i - d); j < i;
                ++j) { // 再坐标差值<=d的范围内更新
                dpMAX[i][k] = max( // 更新最大值
                    dpMAX[i][k], max(dpMAX[j][k - 1] * arr[i], dpMIN[j][k - 1] * arr[i])
                );

                dpMIN[i][k] = min( // 更新最小值
                    dpMIN[i][k], min(dpMAX[j][k - 1] * arr[i], dpMIN[j][k - 1] * arr[i])
                );

            }
        }
        ans = max(ans, dpMAX[i][K]); // 更新结果
    }
    cout << ans << endl;
    return;
}

int main() {
    int n, k, d;
    cin >> n;
    cin >> k >> d;

    vector<int> arr(n);
    for (int i = 0; i < n; ++i) {
        cin >> arr[i];
    }
    solve(n, k, d, arr);
    return 0;
}
