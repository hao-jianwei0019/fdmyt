#include <iostream>
#include <string.h>
using namespace std;
const int maxx = 1e7;
bool judge[maxx];//判断是否为素数，若为素数则标记true；
int prime[maxx];//存放数组
//求[2-n]之间的素数个数
int is_prime(int n)
{
	int i = 0;
	memset(judge, true, n);//初始化数组，void *memset( void *dest, int c, size_t count );c为要初始化的值，count为初始化的数量
	for (i = 2; i < n; i++)//循环直接从2开始不管0，1.存放的时候也可以这样
	{
		if (judge[i])
		{
			for (int j = i * i; j <= n; j += i)
				judge[j] = false;
		}
	}
	int k = 0;//作为统计个数的变量
	for (int i = 2; i <= n; i++)
	{
		if (judge[i])
			prime[k++] = i;
	}
	return k;
}
int main()
{
	int n = 0;
	int m=is_prime(300);
	while (n++ < m)
		cout << prime[n]<<"   ";
	return 0;
}