#include <stdio.h>
////联合体的申明
//union Un
//{
//	char c;
//	int i;
//};
////联合变量的定义
//union Un un;
////计算连个变量的大小
//int main()
//{
//	un.c = 0x01;
//	if (un.i == 0x01)
//		printf("小端");
//	if (un.i == 0x01000000)
//		printf("大端");
//	return 0;
//}
union Un1
{
	char c[5];
	int i;
};
union Un2
{
	short c[7];
	int i;
};
//下面输出的结果是什么？
int main()
{
	printf("%d\n", sizeof(union Un1));
	printf("%d\n", sizeof(union Un2));
}