#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
void reverse(char* arr, int n)
{
	int left = 0;
	int right=n-1;
	while (right > left)
	{
		char tmp=*(arr+left);
		*(arr + left) = *(arr + right);
		*(arr + right) = tmp;
		right--;
		left++;
	}
}
char left_reverse(char* arr, int len)
{
	int k;
	scanf("%d", &k);
	if (k == 0)
	{
		return ' ';
	}
	reverse(arr, len);
	reverse(arr, len - k);
	reverse((arr+ len - k), k);
	return arr;

}
int main()
{
	char b[6]= "abcde";
	/*reverse(b, 5);*/
	left_reverse(b, 5);
	int i = 0;
	/*for (i = 0; i < 5; i++)
	{
		printf("%c", b[i]);


	}*/
	printf("%s", b);
	return 0;
}