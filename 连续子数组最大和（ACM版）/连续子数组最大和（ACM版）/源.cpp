#include <algorithm>
#include <climits>
#include <iostream>
using namespace std;
typedef long long ll;
int n;

int main() {
    cin >> n;
    ll x1 = 0;//上一步选还是不选
    ll ans = INT_MIN;
    for (int i = 0; i < n; i++)
    {
        ll tmp;
        cin >> tmp;
        x1 = max(x1 + tmp, tmp);
        ans = max(ans, x1);
    }
    cout << ans;
    return 0;
}
// 64 位输出请用 printf("%lld")