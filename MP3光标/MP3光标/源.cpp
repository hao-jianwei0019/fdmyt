#include <iostream>
#include <string>
using namespace std;

int main() {
    int n;
    string commands;
    while (cin >> n >> commands) {
        int num = 1;
        // first：当前屏幕显示页的第一首歌曲的编号
        int first = 1;
        // 歌曲总数不超过4时，不需翻页
        if (n <= 4) {
            for (int i = 0; i < commands.size(); i++) {
                // 特殊向上翻页
                if (num == 1 && commands[i] == 'U') {
                    num = n;
                    // 特殊向下翻页
                }
                else if (num == n && commands[i] == 'D') {
                    num = 1;
                }
                else if (commands[i] == 'U') {
                    num--;
                }
                else {
                    num++;
                }
            }
            for (int i = 1; i <= n - 1; i++) {//输出当前页
                cout << i << ' ';
            }
            cout << n << endl << num << endl;
        }
        else {// 歌曲总数大于4时，需要翻页
            for (int i = 0; i < commands.size(); i++) {
                // 特殊向上翻页
                if (num == 1 && commands[i] == 'U') {
                    first = n - 3;
                    num = n;
                }
                else if (num == n && commands[i] == 'D') {// 特殊向下翻页
                    first = 1;
                    num = 1;
                }
                else if (num == first && commands[i] == 'U')//一般向上翻页
                {
                    first--;
                    num--;
                }
                else if (num == first + 3 && commands[i] == 'D')//一般向下翻页
                {
                    first++;
                    num++;
                }
                else if (commands[i] == 'U') {//其他情况，不翻页，只移动光标
                    num--;
                }
                else {
                    num++;
                }
            }
            for (int i = first; i < first + 3; i++) {//输出当前页面
                cout << i << ' ';
            }
            cout << first + 3 << endl << num << endl;
        }
    }
    return 0;
}