const int N = 1e5 + 10;
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param arr int整型vector the array
     * @return int整型
     */

    int nums[N];

    int maxLength(vector<int>& arr) {
        // write code here

        int ans = 0;
        memset(nums, 0, sizeof nums);
        int left = 0, right = 0;
        int n = arr.size();
        if (n == 0)return 0;
        while (right < n)
        {
            while (!nums[arr[right]] && right < n)
            {
                nums[arr[right]]++;
                right++;
            }
            ans = max(ans, right - left);
            if (right == n)break;
            while (arr[left] != arr[right] && left < right)
            {
                nums[arr[left]]--;
                left++;
            }
            nums[arr[left]]--;
            left++;
        }
        return ans;
    }
};