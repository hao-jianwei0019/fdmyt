class Solution {
public:
    string tmp;
    vector<string> board = { "", "", "abc", "def", "ghi","jkl","mno","pqrs","tuv","wxyz" };
    vector<string>ret;
    void dfs(int pos, string digits)
    {
        if (pos == digits.size())//到尾部了
        {
            ret.push_back(tmp);
            return;
        }
        int num = digits[pos] - '0';//按倒的键
        for (int i = 0; i < board[num].size(); i++)
        {
            tmp += board[num][i];//加上这一层因该加的东西
            dfs(pos + 1, digits);//到下一层
            tmp.pop_back();//回溯
        }
    }
    vector<string> letterCombinations(string digits) {
        if (digits.empty())return ret;
        dfs(0, digits);
        return ret;
    }
};