#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;
const int N = 210, INF = 1e9;
int n, m;
int x, y;
double z,d[N][N];
void floyd()
{
    for (int k = 1; k <= n; k++) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
            }
        }
    }
}
int main()
{
    n = 7;
    m = 9;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (i != j)d[i][j] = INF;
            else d[i][j] = 0;
        }
    }
    while (m--)
    {
        cin >> x >> y >> z;
        d[x][y] = min(d[x][y], z);
        d[y][x] = min(d[y][x], z);
    }
    floyd();
    cout << "1 4 " << d[1][4] << endl;
    cout << "1 5 " << d[1][5] << endl;
    cout << "1 7 " << d[1][7] << endl;
    cout << "4 5 " << d[4][5] << endl;
    cout << "4 7 " << d[7][4] << endl;
    cout << "5 7 " << d[5][7] << endl;


    return 0;
}