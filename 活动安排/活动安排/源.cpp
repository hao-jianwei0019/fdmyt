#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

int main() {

    int n;
    cin >> n;
    vector<vector<int>>nums(n, vector<int>(2, 0));
    for (int i = 0; i < n; i++)
    {
        int a, b;
        cin >> a >> b;
        nums[i][0] = a, nums[i][1] = b;
    }
    sort(nums.begin(), nums.end());
    int ans = 0;
    int l = 0;
    while (l < n)
    {
        int k = nums[l][1];
        int tmp = l + 1;
        int sml = l;
        while (nums[tmp][0] < k && tmp < n)
        {
            if (nums[tmp][1] < k)
            {
                sml = tmp;
                k = nums[tmp][1];
            }
            tmp++;
        }
        l = tmp;
        ans++;
    }
    cout << ans << endl;
    return 0;
}

// 64 λ������� printf("%lld")