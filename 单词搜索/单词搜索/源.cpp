#include<iostream>
#include<vector>
#include<string>
using namespace std;
const int N = 110;
bool vis[N][N];
int h, l;
int w;
bool job;
int dx[4] = { -1,0,1,0 };
int dy[4] = { 0,1,0,-1 };
vector<string> board1;
string word1;
class Solution {
public:
    void dfs(int x, int y, int j)
    {
        vis[x][y] = true;
        if (j == w)
        {
            job = true;
            return;
        }
        for (int i = 0; i < 4; i++)
        {
            int x1 = x + dx[i];
            int y1 = y + dy[i];
            if (x1 >= 0 && x1 < h && y1 >= 0 && y1 < l && !vis[x1][y1])
            {
                if (board1[x1][y1] == word1[j])
                {
                    dfs(x1, y1, j + 1);
                    if (job)return;                    
                    vis[x1][y1] = false;

                }
            }
        }
    }
    bool exist(vector<string> board, string word) {
        // write code here
        board1 = board;
        word1 = word;
        h = board.size();
        l = board[0].size();
        w = word.size();
        job = false;
        for (int i = 0; i < h; i++)
            for (int j = 0; j < l; j++)
            {
                if (board[i][j] == word[0])
                {
                    memset(vis, false, sizeof vis);
                    dfs(i, j, 1);
                    if (job)
                    {
                        return true;
                    }
                }
            }
        return false;
    }
}O;
int main()
{

    O.exist({ "ABCE", "SFES", "ADEE" }, "ABCEFSADEESE");
}