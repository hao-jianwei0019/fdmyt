
#include<iostream>
#include<vector>
#include<queue>
using namespace std;
typedef pair<long long, int> PII;
auto compare = [](PII a, PII b) -> bool {
    if (a.first == b.first)return a.second > b.second;
    return a.first > b.first; };
priority_queue<PII, vector<PII>, decltype(compare)> Pqueue(compare);
int N, K;
long long arr[500010];

void solve()
{
    while (K--)
    {
        auto x = Pqueue.top().first;
        auto y = Pqueue.top().second;
        Pqueue.pop();
        if (x != arr[y])
        {
            K++;
            continue;
        }
        arr[y] = -1;
        if (x == 0)continue;
        int l = y - 1, r = y + 1;
        while (l >= 0 && arr[l] == -1)
        {
            l--;
        }
        if (l >= 0)Pqueue.push({ arr[l] += x,l });

        while (r < N && arr[r] == -1)r++;
        if (r < N) Pqueue.push({ arr[r] += x,r });
    }
    for (int i = 0; i < N; i++)
    {
        if (arr[i] != -1)cout << arr[i] << " ";
    }
}

int tmp;
int main()
{
    cin >> N >> K;
    for (int i = 0; i < N; i++)
    {
        cin >> tmp;
        arr[i] = tmp;
        Pqueue.push({ tmp,i });
    }
    solve();
    return 0;
}