#include <iostream>
using namespace std;
int main()
{
	int a;
	int b = 0;
	cin >> a;
	while (a != 0)
	{
		b += a % 10;
		a = a * 2;
		a /= 10;
	}
	cout << b;
	return 0;
}