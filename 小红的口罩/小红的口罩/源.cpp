#include <functional>
#include <iostream>
#include <queue>
using namespace std;
int main() {
    priority_queue<int, vector<int>, greater<int>>q;
    int n, k;
    cin >> n >> k;
    for (int i = 0; i < n; i++)
    {
        int tmp;
        cin >> tmp;
        q.push(tmp);
    }
    int ans = 0;
    int sum = 0;
    while (sum <= k)
    {
        int tmp = q.top();
        q.pop();
        q.push(tmp * 2);
        if (sum + tmp <= k)ans++;
        sum += tmp;
    }
    cout << ans << endl;
    return 0;
}
// 64 λ������� printf("%lld")