#include <iostream>
using namespace std;
void bucket(int a[], int b[][10], int n)
{
	int i;
	int bi, col[10] = { 0 };//col每个桶计数器
	int t = 1;
	do
	{
		//清空桶
		for (i = 0; i < 10; i++)
		{
			col[i] = 0;
		}
		//分桶
		for (i = 0; i < n; i++)
		{
			bi = a[i]/t % 10;
			b[col[bi]][bi] = a[i];//b[0][个位]
			col[bi]++;
		}
		//回收
		int cnt = 0;
		
		for (int j = 0; j < 10; j++)
		{
			for (i = 0; i < col[j]; i++)
			{
				a[cnt++] = b[i][j];
			}
		}
		t *= 10;
		
	} while (col[0]!=8);
}
int main()
{
	const int n = 8;
	int arr[n] = {10,4345,5,6,89,45,67,34};
	int i = 0;
	/*for (i = 0; i < n; i++)
	{
		cin >> arr[i];
	}*/
	int tong[n][10];//10代表桶的编号
	for (i = 0; i < n; i++)
	{
		cout << arr[i] << '\t';
	}
	cout << endl;
	bucket(arr, tong, n);
	for (i = 0; i < n; i++)
	{
		cout << arr[i] << '\t';
	}

	return 0;
}