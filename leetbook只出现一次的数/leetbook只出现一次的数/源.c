void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}
//����ȡ��
int GetMidNumi(int* a, int left, int right)
{
	int mid = (left + right) / 2;
	if (a[left] < a[mid])
	{
		if (a[mid] < a[right])
		{
			return mid;
		}
		else if (a[left] > a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
	else // a[left] > a[mid]
	{
		if (a[mid] > a[right])
		{
			return mid;
		}
		else if (a[left] < a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
}
int PartSort1(int* a, int left, int right)
{

	int mid = GetMidNumi(a, left, right);

	if (mid != left)
		Swap(&a[mid], &a[left]);

	int keyi = left;

	while (left < right)
	{

		while (left < right && a[right] >= a[keyi])
		{
			right--;
		}
		while (left < right && a[left] <= a[keyi])
		{
			left++;
		}
		Swap(&a[right], &a[left]);

	}
	Swap(&a[keyi], &a[left]);
	keyi = left;
	return keyi;

}
void QuickSort(int* a, int left, int right)
{
	if (left >= right)
		return;

	int keyi = PartSort1(a, left, right);
	QuickSort(a, left, keyi - 1);
	QuickSort(a, keyi + 1, right);
}
bool containsDuplicate(int* nums, int numsSize) {
	if (numsSize < 2)return false;
	QuickSort(nums, 0, numsSize - 1);
	int i = 0;
	while (i + 1 < numsSize)
	{
		if (nums[i] == nums[i + 1])
			return true;
		i++;

	}
	return false;

}