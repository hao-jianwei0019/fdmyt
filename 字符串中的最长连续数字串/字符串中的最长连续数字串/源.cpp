#include <iostream>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
const int N = 256;
string s;
int dp[N];
int ans;
int q = -1;
int main() {
    cin >> s;
    memset(dp, 0, sizeof dp);
    ans = 0;
    for (int i = 1; i <= s.size(); i++)
    {
        if (s[i - 1] >= '0' && s[i - 1] <= '9')
        {
            dp[i] = dp[i - 1] + 1;
            if (ans < dp[i])
            {
                ans = dp[i];
                q = i - dp[i];
            }
        }
    }
    string ret(s.begin() + q, s.begin() + q + ans);//不用-1因为end是虚无
    cout << ret;
    return 0;

}
// 64 位输出请用 printf("%lld")