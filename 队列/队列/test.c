#include "Queue.h"
int main()
{
	Queue tail;
	QueueInit(&tail);
	QueuePush(&tail, 1);
	QueuePush(&tail, 2);
	QueuePush(&tail, 3);
	QueuePush(&tail, 4);
	printf("%d ", QueueFront(&tail));
	QueuePop(&tail);
	printf("%d ", QueueFront(&tail));
	QueuePop(&tail);
	printf("%d ", QueueFront(&tail));
	QueuePop(&tail);
	printf("%d ", QueueFront(&tail));
	QueuePop(&tail);
	QueueDestroy(&tail);
}