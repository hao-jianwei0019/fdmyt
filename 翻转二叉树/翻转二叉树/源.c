/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode* left;
 *     struct TreeNode *right;
 * };
 */
void Swap(struct TreeNode* root)
{
    struct TreeNode* tmp = root->left;
    root->left = root->right;
    root->right = tmp;
}

struct TreeNode* invertTree(struct TreeNode* root) {
    if (root == NULL)
        return NULL;
    invertTree(root->left);
    Swap(root);
    invertTree(root->left);
    return root;
}