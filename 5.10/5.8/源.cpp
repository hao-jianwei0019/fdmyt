#include <iostream>
#include <iomanip>
using namespace std;
int factorial(int n)
{
    int f = 1;
    for (int i = 1; i <= n; i++)
    {
        f *= i;
    }
    return f;
}
int main()
{
    cout << "Factorials of 1 to 5\n" << endl;
    cout << "N" << std::setw(14) << "Factorial(N)" << endl;
    for (int i = 1; i <= 5; i++)
    {
        cout << i << setw(14) << factorial(i) << endl;
    }
    return 0;
}
