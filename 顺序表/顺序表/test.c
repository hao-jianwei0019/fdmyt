#include "seqlist.h"
void test1()
{
	SeqList list1;
	SeqListInit(&list1);
	SeqListPushBack(&list1, 1);
	SeqListPushBack(&list1, 2);
	SeqListPushBack(&list1, 3);
	SeqListPushBack(&list1, 4);
	SeqListPrint(&list1);

	SeqListPushFront(&list1, 1);
	SeqListPushFront(&list1, 2);
	SeqListPushFront(&list1, 3);
	SeqListPushFront(&list1, 4);
	SeqListPrint(&list1);
	SeqListPopFront(&list1);
	SeqListPopFront(&list1);
	SeqListPopFront(&list1);
	SeqListPrint(&list1);
	SeqListPopBack(&list1);
	SeqListPopBack(&list1);
	SeqListPopBack(&list1);
	SeqListPopBack(&list1);
	SeqListPrint(&list1);

	SeqListDestroy(&list1);


}

BTNode* BinaryTreeCreate(BTDataType* a, int* pi)
{
	if (a[*pi] == '#')
	{
		(*pi)++;
		return NULL;
	}

	BTNode* root = (BTNode*)malloc(sizeof(BTNode));
	if (root == NULL)
	{
		perror("malloc");
		return;
	}
	root->_data = a[(*pi)++];
	root->_left = BinaryTreeCreate(a, pi);
	root->_right = BinaryTreeCreate(a, pi);
	return root;

}

void  BinaryTreeInOrder(BTNode* root, SeqList* ps)
{
	if (root == NULL)return;
	BinaryTreeInOrder(root->_left, ps);
	SeqListPushBack(ps, root->_data);
	BinaryTreeInOrder(root->_right, ps);
}

bool isValidBST(BTNode* root) {
	if (root == NULL) return true;
	SeqList list1;
	SeqListInit(&list1);
	BinaryTreeInOrder(root, &list1);

	int i = 0;
	int n = list1.size;
	while (i + 1 < n)
	{
		if (list1.a[i] < list1.a[i + 1])
		{
			i++;
		}
		else
			break;
	}

	SeqListDestroy(&list1);
	if (i + 1 < n)
		return false;
	else
		return true;

}
void test2()
{
	char  arr[100];
	scanf("%s", arr);
	int i = 0;
	BTNode* root = BinaryTreeCreate(arr, &i);
	printf("%d", isValidBST(root));

}
int main()
{
	test2();

	return 0;
}