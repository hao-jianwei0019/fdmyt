#include "seqlist.h"
void SeqListInit(SeqList* ps)//��ʼ��
{
	assert(ps);
	ps->a =(SLDateType*) malloc(Initnum * sizeof(SLDateType));
	if (ps->a == NULL)
	{
		perror("malloc");
		return;
	}
	ps->size = 0;
	ps->capacity = Initnum;
}
void SeqListDestroy(SeqList* ps)
{
	assert(ps);
	ps->size = 0;
	ps->capacity = 0;
	free(ps->a);
	ps->a = NULL;

}
void SeqListPrint(SeqList* ps)
{
	assert(ps);
	int i = 0;
	for (; i < ps->size; i++)
	{
		printf("%d ", *((ps->a) + i));

	}
	printf("\n");

}
void DilaSeqList(SeqList* ps)
{
	assert(ps);
	SLDateType* ret=(SLDateType*)realloc(ps->a, 2*(ps->capacity) * sizeof(SLDateType));
	if (ret== NULL)
	{
		perror("realloc");
		return;
	}
	ps->a = ret;
	ps->capacity = ps->capacity *2;
}

void SeqListPushBack(SeqList* ps, SLDateType x)
{
	assert(ps);

	if (ps->capacity == ps->size)
	{
		DilaSeqList(ps);
	}
	*(ps->a + ps->size) = x;
	ps->size += 1;

}

void SeqListPushFront(SeqList* ps, SLDateType x)
{
	assert(ps);
	if (ps->capacity == ps->size)
	{
		DilaSeqList(ps);
	}
	int i = ps->size;
	while (i)
	{
		*(ps->a + i) = *(ps->a + i - 1);
		i--;
	}
	*(ps->a) = x;
	ps->size += 1;

}
void SeqListPopFront(SeqList* ps)
{
	assert(ps->size);
	for (int i = 0; i < ps->size-1; i++)
	{
		ps->a[i] = ps->a[i + 1];
	}
	ps->size -= 1;
}
void SeqListPopBack(SeqList* ps)
{
	assert(ps->size);
	ps->size--;
}
int SeqListFind(SeqList* ps, SLDateType x)
{
	assert(ps);
	for (int i = 0; i < ps->size; ++i)
	{
		if (ps->a[i] == x)
		{
			return i;
		}
	}

	return -1;
}
void SLInsert(SeqList* ps, int pos, SLDateType x)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);

	if (ps->capacity == ps->size)
	{
		DilaSeqList(ps);
	}

	int end = ps->size - 1;
	while (end >= pos)
	{
		ps->a[end + 1] = ps->a[end];
		--end;
	}

	ps->a[pos] = x;
	ps->size++;
}

void SeqListErase(SeqList* ps, int pos)
{
	assert(ps);
	assert(pos >= 0 && pos < ps->size);

	int begin = pos + 1;
	while (begin < ps->size)
	{
		ps->a[begin - 1] = ps->a[begin];
		++begin;
	}

	ps->size--;
}
