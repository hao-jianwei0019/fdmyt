#include <cstring>
#include <iostream>
using namespace std;
const int N = 13;
int num[N];
int n;
bool vis[N];
int ans;
void dfs(int j)
{
    if (j == n + 1)
    {
        ans++;
        return;
    }
    for (int i = 1; i <= n; i++)
    {
        if (!vis[i])
        {
            if (!vis[num[i]])
            {
                vis[i] = true;
                dfs(j + 1);
                vis[i] = false;
            }
        }
    }
}
int main() {
    cin >> n;
    for (int i = 1; i <= n; i++)cin >> num[i];
    ans = 0;
    memset(vis, false, sizeof vis);
    dfs(1);

    cout << ans;
    return 0;
}
// 64 λ������� printf("%lld")