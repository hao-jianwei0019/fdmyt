typedef int QDatatype;

typedef struct {
    QDatatype* val;
    int head;
    int rear;
    int k;
} MyCircularQueue;


MyCircularQueue* myCircularQueueCreate(int k) {
    MyCircularQueue* obj = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    if (obj == NULL)
    {
        return NULL;
    }
    obj->val = (QDatatype*)malloc(sizeof(QDatatype) * (k + 1));
    if (obj->val == NULL)
    {
        return NULL;
    }
    obj->head = obj->rear = 0;
    (obj->k) = k;
    return obj;

}
bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    return obj->head == obj->rear;
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
    return (obj->rear - obj->head + (obj->k) + 1) % (obj->k + 1) == (obj->k);
}

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value)
{
    if (myCircularQueueIsFull(obj))
    {
        return false;
    }
    else
    {
        obj->val[obj->rear] = value;
        obj->rear = (obj->rear + 1) % (obj->k + 1);
        return true;

    }
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
    {
        return false;
    }
    else
    {

        obj->head = (obj->head + 1) % (obj->k + 1);
        return true;
    }

}


int myCircularQueueFront(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
        return -1;
    return obj->val[obj->head];
}

int myCircularQueueRear(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
        return -1;
    return obj->val[(obj->rear + obj->k) % (obj->k + 1)];
}


void myCircularQueueFree(MyCircularQueue* obj) {
    assert(obj);
    free(obj->val);
    obj->head = obj->rear = (obj->k) = 0;
    free(obj);
}

/**
 * Your MyCircularQueue struct will be instantiated and called as such:
 * MyCircularQueue* obj = myCircularQueueCreate(k);
 * bool param_1 = myCircularQueueEnQueue(obj, value);

 * bool param_2 = myCircularQueueDeQueue(obj);

 * int param_3 = myCircularQueueFront(obj);

 * int param_4 = myCircularQueueRear(obj);

 * bool param_5 = myCircularQueueIsEmpty(obj);

 * bool param_6 = myCircularQueueIsFull(obj);

 * myCircularQueueFree(obj);
*/