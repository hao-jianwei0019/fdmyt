//鞍点指的是矩阵中的一个元素，它是所在行的最大值，并且是所在列的最小值。
//例如：在下面的例子中（第4行第1列的元素就是鞍点，值为8 ）。
//11 3 5 6 9
//12 4 7 8 10
//10 5 6 9 11
//8 6 4 7 2
//15 10 11 20 25
#include <iostream>
#include <array>
using namespace std;
int main()
{
    int i, j, k;
    int m1, n1, max, min;
    array<array<int,6>, 6>a = {0};
    int flag, s = 0;
    for (i = 0; i < 5; ++i) {//输入二维数组 
        for (j = 0; j < 5; ++j) {
            cin>>a[i][j];
        }
    }
    for (i = 0; i < 5; ++i)
    {
        max = a[i][0];//先取每行第一个数为最大值
        for (j = 0; j < 5; ++j)
        {
            if (a[i][j] >= max)//找到该行的最大值a[m1][n1] 
            {
                max = a[i][j];
                m1 = i;
                n1 = j;
            }
        }
        min = a[m1][n1];//假设每行的最大值为每列的最小值 
        flag = 0;
        for (k = 0; k < 5; ++k)
        {
            if (a[k][n1] < min)//如果发现该值不是所在列的最小值，flag置1； 
                flag = 1;
        }
        if (flag == 0)//所在列没有值比该值小，即为所在列的最小值 
        {
           cout<<"行：" << m1 + 1 <<"列：" << n1 + 1<<"值为" << a[m1][n1];
            s++;//鞍点数加1；
        }
    }
    if (s == 0)//如果鞍点数为0
        cout<<"not found";
    return 0;
}
