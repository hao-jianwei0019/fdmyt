// extract to string
#include <iostream>
#include <string>

int main()
{
	std::string name;

	std::cout << "Please, enter your full name: ";
	std::getline(std::cin, name);
	std::cout << "Hello, " << name << "!\n";
	//对于string类可以直接输入：
	char app[20];
	std::cin.getline(app, 20);
	//对与char的用法
	return 0;
}