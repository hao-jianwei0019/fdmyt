#include <iostream>
using namespace std;
void print(int queen[8][8])
{
	int a = 0;
	for (a = 0; a < 8; a++)
	{
		int b = 0;
		for (; b < 8; b++)
		{
			cout << queen[a][b] << "  ";
		}
		cout << endl;
	}
}
int main()
{
	int queen[8][8] = { 0 };
	int row = 0;
	int col = 0;
	int sum = 0;//总数
	
	while (1)
	{
		queen[row][col] = 1;
		if (queen[0][7] == 1 && queen[1][5] == 1)
		{
			break;
		}
		
		//判断皇后位置是否被扎
		int istrack = 0;
		int i = 0;
		for (i = 0; i < 8; i++)
		{
			//同一列有没有东西
			istrack += queen[i][col];
			//斜线上是否有东西存在
			if (i > 0)
			{
				if (row + i < 8 && col + i < 8)
					istrack += queen[row + i][col + i];
				if (row - i >= 0 && col - i >= 0)
					istrack += queen[row - i][col - i];
				if (row + i < 8 && col - i >= 0)
					istrack += queen[row + i][col - i];
				if (row - i >= 0 && col + i < 8)
					istrack += queen[row - i][col + i];
			}
		}

		if (istrack == 1)
		{
			
			row++;
			col = 0; 
			if (row == 8)
			{
				print(queen);
				cout << endl;cout << endl;
				sum++;
				do//回溯，找下一种
				{
					row--;
					int j = 0;
					for (j = 0; j < 8; j++)//找到上一行的皇后让她往后走一步
					{
						if (queen[row][j] == 1)
						{
							break;
						}
					}
					queen[row][j] = 0;
					col = j + 1;



				} while (col >= 8);


			}
		}
		else 
		{
			queen[row][col] = 0;
			col++;
			while (col >= 8)//回溯
			{
				row--;
				int j = 0;
				for (j = 0; j < 8; j++)//找到上一行的皇后让她往后走一步
				{
					if (queen[row][j] == 1)
					{
						break;
					}
				}
				queen[row][j] = 0;
				col = j + 1;

			}

		}
	}
	cout << sum << endl;
	return 0;
}