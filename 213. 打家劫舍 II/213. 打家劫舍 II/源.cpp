class Solution {
public:
    int massage(vector<int>& nums) {
        int n = nums.size();
        if (n == 0)return 0;
        vector<int>dpf(n);//选
        vector<int>dpg(n);//不选
        dpf[0] = nums[0];
        dpg[0] = 0;
        for (int i = 1; i < n; i++)
        {
            //如果选，则上一个必定不选，如果不选，上一个可能选可能不选
            dpf[i] = dpg[i - 1] + nums[i];
            dpg[i] = std::max(dpf[i - 1], dpg[i - 1]);
        }
        int max = std::max(dpf[n - 1], dpg[n - 1]);
        return max;
    }
    int rob(vector<int>& nums) {
        if (nums.size() == 1)return nums[0];
        if (nums.size() == 2)return std::max(nums[0], nums[1]);
        vector<int> a(nums.begin() + 2, nums.end() - 1);
        int stealone = massage(a) + nums[0];
        vector<int> b(nums.begin() + 1, nums.end());
        int nostealone = massage(b);
        int max = std::max(stealone, nostealone);
        return max;
    }
};