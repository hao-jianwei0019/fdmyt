#include<iostream>

template <typename T>
class SharedPtr
{
private:
	T* ptr;
	int* ref_count;

	void release()
	{
		if (--(*ref_count) == 0)
		{
			delete ptr;
			delete ref_count;
			ptr = nullptr;
			ref_count = nullptr;
		}
	}
public:
	explicit SharedPtr(T* p = nullptr) :ptr(p), ref_count(new int(1)) {}


	SharedPtr(const SharedPtr& other) :ptr(other.ptr), ref_count(other.ref_count)
	{
		++(*ref_count);
	}

	SharedPtr& operator=(const SharedPtr& other)
	{
		if (this != &other)
		{
			release();
			ptr = other.ptr;
			ref_count = other.ref_count;
			++(*ref_count);
		}
		return *this;
	}

	T& operator*()const { return *ptr; }
	T& operator->() const { return ptr; }
	~SharedPtr() { release(); }

	int use_const() { return *ref_count; }
};