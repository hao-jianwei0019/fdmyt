class Solution {
public:
    int maxProfit(vector<int>& prices) {

        int n = prices.size();
        if (n == 1)return 0;
        vector<int> dpf(n, 0);//����
        auto dpg = dpf;//�ɽ���
        auto dph = dpf;//�䶳
        dpf[0] = 0 - prices[0];
        for (int i = 1; i < n; i++)
        {
            dpf[i] = max(dpg[i - 1] - prices[i], dpf[i - 1]);
            dpg[i] = max(dpg[i - 1], dph[i - 1]);
            dph[i] = dpf[i - 1] + prices[i];
        }
        return max(dpg[n - 1], dph[n - 1]);
    }
};