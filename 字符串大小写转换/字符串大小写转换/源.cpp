#include <iostream>
#include <array>
#include <string>
#include <istream>
using namespace std;

int main()
{
	char a[200];
	cin.getline(a, 200);
	int i = 0; 
	while (a[i])
	{
		if (a[i] > 'a' && a[i] < 'z')
		{
			char b = a[i] - 32;
			cout << b;
		}
		else
		{
			cout << a[i];
		}
		i++;
	}
	return 0;
}