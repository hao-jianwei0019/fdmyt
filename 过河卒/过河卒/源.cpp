#include <iostream>
using namespace std;
const int N = 25;
long long dp[N][N];
int n, m, x, y;
int dx[8] = { -1,-2,-2,-1,1,2,2,1 };
int dy[8] = { -2,-1,1,2,2,1,-1,-2 };
int main() {
    cin >> n >> m >> x >> y;
    n++, m++, x++, y++;
    dp[x][y] = -1;
    for (int i = 0; i < 8; i++)
    {
        int x1 = x + dx[i], y1 = y + dy[i];
        if (x1 > 0 && x1 <= n && y1 > 0 && y1 <= m)
            dp[x1][y1] = -1;
    }
    dp[0][1] = 1;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
        {
            if (dp[i][j] == -1)dp[i][j] = 0;

            else dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
        }

    cout << dp[n][m];
    return 0;
}
// 64 λ������� printf("%lld")