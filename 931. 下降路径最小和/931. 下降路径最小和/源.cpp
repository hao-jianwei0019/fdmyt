class Solution {
public:
    int min(int a, int b, int c)
    {

        if (a < b)b = a;
        if (b < c)c = b;
        return c;
    }
    int minFallingPathSum(vector<vector<int>>& matrix) {
        int n = matrix.size();
        int m = matrix[0].size();
        vector<vector<int>>dp(n + 1, vector<int>(m + 2, 0));
        for (int i = 1; i <= n; i++)
            for (int j = 1; j <= m; j++)
            {
                if (j == 1)
                {
                    dp[i][j] = matrix[i - 1][j - 1] + std::min(dp[i - 1][j], dp[i - 1][j + 1]);
                }
                else if (j == m)
                {
                    dp[i][j] = matrix[i - 1][j - 1] + std::min(dp[i - 1][j - 1], dp[i - 1][j]);
                }
                else
                {
                    dp[i][j] = matrix[i - 1][j - 1] + min(dp[i - 1][j - 1], dp[i - 1][j], dp[i - 1][j + 1]);
                }
            }
        int min = dp[n][1];
        for (int j = 2; j <= m; j++)
        {
            if (dp[n][j] < min)min = dp[n][j];
        }
        return min;
    }
};
