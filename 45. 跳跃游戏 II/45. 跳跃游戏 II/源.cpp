class Solution {
public:
    int dp[10010];
    int jump(vector<int>& nums) {
        int n = nums.size();
        if (nums.size() == 0 || nums.size() == 1)return 0;
        memset(dp, 10010, sizeof dp);
        dp[0] = 0;
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j < i; j++)
            {

                if (j + nums[j] >= i)dp[i] = min(dp[j] + 1, dp[i]);
            }
        }
        //for(int i=0;i<n;i++)cout<<dp[i]<<' ';
        return dp[n - 1];

    }
};


class Solution {
public:
    int jump(vector<int>& nums) {
        int n = nums.size();
        int step = 0;
        int end = 0;
        int maxPos = 0;
        for (int i = 0; i < n - 1; i++)
        {
            if (maxPos >= i)
            {
                maxPos = max(maxPos, i + nums[i]);
                if (i == end)
                {
                    end = maxPos;
                    ++step;
                    if (end >= n - 1)return step;
                }
            }
        }
        return step;

    }
};