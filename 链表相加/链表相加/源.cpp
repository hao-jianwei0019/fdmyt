/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 *	ListNode(int x) : val(x), next(nullptr) {}
 * };
 */
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param head1 ListNode类
     * @param head2 ListNode类
     * @return ListNode类
     */
    ListNode* addInList(ListNode* head1, ListNode* head2) {
        // write code here
        string s, t;
        while (head1 != nullptr)
        {
            s += head1->val + '0';
            head1 = head1->next;
        }
        while (head2 != nullptr)
        {
            t += head2->val + '0';
            head2 = head2->next;
        }
        string ans;
        int l = s.size() - 1;
        int r = t.size() - 1;
        int n = min(l, r);
        int tmp = 0;
        int i;
        for (i = 0; i <= n; i++)
        {
            tmp += s[l - i] - '0' + t[r - i] - '0';
            ans += tmp % 10 + '0';
            tmp /= 10;
        }
        while (i <= l)
        {
            tmp += s[l - i] - '0';
            ans += tmp % 10 + '0';
            tmp /= 10;
            i++;
        }
        while (i <= r)
        {
            tmp += t[r - i] - '0';
            ans += tmp % 10 + '0';
            tmp /= 10;
            i++;
        }
        while (tmp)
        {
            ans += tmp % 10 + '0';
            tmp /= 10;
        }
        ListNode* ret = nullptr;
        for (i = 0; i < ans.size(); i++)
        {
            ListNode* tmp = new ListNode(ans[i] - '0');
            tmp->next = ret;
            ret = tmp;
        }
        return ret;
    }
};