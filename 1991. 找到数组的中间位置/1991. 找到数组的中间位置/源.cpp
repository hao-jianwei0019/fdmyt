class Solution {
public:
    int findMiddleIndex(vector<int>& nums) {
        int total = 0;
        for (int i = 0; i < nums.size(); i++)
        {
            total += nums[i];
        }
        int left = 0;
        int mid = 0;
        while (mid < nums.size())
        {
            if (left * 2 + nums[mid] == total)
                return mid;
            left += nums[mid];
            mid++;
        }
        return -1;
    }
};