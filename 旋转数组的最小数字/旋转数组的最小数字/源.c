
/**
 *
 * @param rotateArray int整型一维数组
 * @param rotateArrayLen int rotateArray数组长度
 * @return int整型
 */
int minNumberInRotateArray(int* rotateArray, int rotateArrayLen) {
    if (rotateArrayLen == 0)
    {
        return 0;
    }
    int i = 0;
    for (i = 0; i < rotateArrayLen; i++)
    {
        scanf("%d", &rotateArray[i]);
    }
    int left = 0;
    int right = rotateArrayLen - 1;
    int mid = 0;
    while (left < right)
    {
        if (rotateArray[left] < rotateArray[right])
        {
            return rotateArray[left];
        }
        mid = (left + right) / 2;
        if (rotateArray[left] < rotateArray[mid])
        {
            left = mid + 1;
        }
        else if (rotateArray[right] > rotateArray[mid])
        {
            right = mid;
        }
        else
        {
            left++;
        }
    }
    return rotateArray[left];
    // write code here
}


//6 7 8  0 1 1  2 3 4 5
//left=1  mid=4
