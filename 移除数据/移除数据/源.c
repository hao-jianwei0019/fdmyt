#include <stdio.h>
int removeElement(int* nums, int numsSize, int val) {
    int e1 = 0;
    int e2 = 0;
    while (e1<numsSize)
    {
        if (nums[e1]!= val)
        {
            nums[e2] = nums[e1];
            e2++;
        }
        e1++;
    }
    return e2;
}
int main()
{
    int arr[5] = { 3,2,2,2,3 };
    int len=removeElement(arr, 5, 3);
    int i = 0;
    for (; i < len; i++)
    {
        printf("%d", arr[i]);
    }
}