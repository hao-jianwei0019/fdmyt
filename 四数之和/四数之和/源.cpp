#include <iostream>
#include <algorithm>
#include<vector>
using namespace std;
class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        vector<vector<int>>ret;
        sort(nums.begin(), nums.end());
        int n = nums.size();
        int a = 0, b = 1;
        while (a < n - 3)
        {
            while (b < n - 2)
            {
                int left = b + 1;
                int right = n - 1;
                long long tar = (long long)target - nums[a] - nums[b];
                while (left < right)
                {
                    int sum = nums[left] + nums[right];
                    if (sum > tar)right--;
                    else if (sum < tar)left++;
                    else {
                        ret.push_back({ nums[a],nums[b],nums[left],nums[right] });
                        left++, right--;
                        while (left < right && nums[left - 1] == nums[left])left++;
                        while (right > left && nums[right] == nums[right + 1])right--;
                    }
                }
                b++;
                while (b < n && nums[b] == nums[b - 1])b++;
            }
            a++;
            while (a < n && nums[a] == nums[a - 1])a++;
            b = a + 1;
        }
        return ret;
    }
}Q;
int main()
{
    vector<int>tmp = { -1,0,-5,-2,-2,-4,0,1,-2 };
    Q.fourSum(tmp, -9);
}