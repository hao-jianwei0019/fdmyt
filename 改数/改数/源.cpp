#include <iostream>
#include <string>
using namespace std;

int main() {
    string nums;
    long long ans = 0;
    cin >> nums;
    int n = nums.size();
    for (int i = 0; i < n; i++)
    {
        int tmp = nums[i] - '0';
        if (tmp % 2 == 0)
            ans = 0 + ans * 10;
        else
            ans = 1 + ans * 10;
    }
    cout << ans;
    return 0;

}
// 64 λ������� printf("%lld")