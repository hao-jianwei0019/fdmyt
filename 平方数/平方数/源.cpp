#include <iostream>
#include<cmath>
#include <algorithm>
using namespace std;
typedef long long ll;
const int N = 1e6 + 10;
long long nums[N];
int main() {
    for (ll i = 0; i < N; i++)
    {
        nums[i] = i * i;
    }
    long long x;
    cin >> x;
    auto l = lower_bound(nums, nums + N, x);
    auto r = l - 1;
    //cout<<*l<<" "<<*r<<endl;
    long long ans;
    ans = x - *r > *l - x ? *l : *r;
    cout << ans;
}
// 64 λ������� printf("%lld")