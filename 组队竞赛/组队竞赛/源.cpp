#include <iostream>
#include <queue>
using namespace std;
int n;
priority_queue<int> nums;

int main() {
    cin >> n;
    int tmp;
    for (int i = 0; i < 3 * n; i++)
    {
        cin >> tmp;
        nums.push(tmp);
    }
    long long ans = 0;
    for (int i = 0; i < n; i++)
    {
        nums.pop();
        ans += nums.top();
        nums.pop();
    }
    cout << ans;
    return 0;
}
// 64 λ������� printf("%lld")