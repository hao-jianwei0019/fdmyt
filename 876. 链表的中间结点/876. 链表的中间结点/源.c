/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
#include <stdio.h>
struct ListNode 
{
    int val;
    struct ListNode* next;
};

struct ListNode* middleNode(struct ListNode* head) {
    struct ListNode* fast = head;
    struct ListNode* slow = head;
    while (fast->next)
    {
        if (fast->next->next == NULL)
        {
            fast = fast->next;
            slow = slow->next;
        }
        else
        {
            fast = fast->next->next;
            slow = slow->next;
        }
    }
    return slow;
}