/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* l = l1;
        ListNode* r = l2;

        ListNode* ret = nullptr;
        int tmp = 0;
        while (l && r)
        {
            tmp += l->val + r->val;
            l = l->next, r = r->next;
            ListNode* ans = new ListNode(tmp % 10, ret);
            tmp /= 10;
            ret = ans;
        }
        while (l)
        {
            tmp += l->val;
            l = l->next;
            ListNode* ans = new ListNode(tmp % 10, ret);
            tmp /= 10;
            ret = ans;
        }
        while (r)
        {
            tmp += r->val;
            r = r->next;
            ListNode* ans = new ListNode(tmp % 10, ret);
            tmp /= 10;
            ret = ans;
        }
        while (tmp)
        {
            ListNode* ans = new ListNode(tmp % 10, ret);
            tmp /= 10;
            ret = ans;
        }
        ListNode* a = nullptr;
        while (ret)
        {
            ListNode* tmp1 = ret;
            ret = ret->next;
            tmp1->next = a;
            a = tmp1;
        }
        return a;
    }
};