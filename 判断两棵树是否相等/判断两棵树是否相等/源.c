/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */


bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
    if (p == NULL || q == NULL)
    {
        if (q == p)
            return true;
        else
            return false;

    }
    if (p->val != q->val)
        return false;
    bool ret = isSameTree(p->left, q->left);
    if (ret == false)
        return ret;
    ret = isSameTree(p->right, q->right);
    return ret;

}