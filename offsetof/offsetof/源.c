#include <stdio.h>
#include <stddef.h>
#define OFFSETOF(type,member) (size_t)&(((type*)0 )->member)
//先将0转换成一个地址然后0地址就是一个结构体指针，用->访问他对应的成员，
//取地址后（减去初始地址，但是这里初始地址为0）再强制转换成整形
#define SWAP(x) (((x&0x55555555)<<1)+((x&0xaaaaaaaa)>>1))
struct S
{
	char c1;
	int b;
	char c2;
};
int main()
{
	printf("%d\n", offsetof(struct S, b));//求结构体内部成员的偏移量
	printf("%d\n", OFFSETOF(struct S, b));
	printf("%d\n", SWAP(10));

	return 0;

}