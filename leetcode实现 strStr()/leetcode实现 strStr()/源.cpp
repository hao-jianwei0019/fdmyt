#include<iostream>
using namespace std;

class Solution {
public:
    int strStr(string haystack, string needle) {
        if (haystack.size() < needle.size())
            return -1;
        int i = 0;
        while (i < haystack.size() - needle.size() + 1)
        {
            if (haystack[i] == needle[0])
            {
                string cur(haystack, i, needle.size());
                if (cur == needle)
                {
                    return i;
                }
            }
            i++;
        }
        return -1;
    }
};
void Test()
{
    Solution s;
    int d=s.strStr("hello", "ll");
    cout << d;
}
int main()
{
    Test();
}