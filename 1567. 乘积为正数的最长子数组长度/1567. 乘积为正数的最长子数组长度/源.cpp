class Solution {
public:
    int getMaxLen(vector<int>& nums) {
        int n = nums.size();
        vector<int>f(n + 1, 0);//������
        vector<int>g(n + 1, 0);//������
        g[0] = 0, f[0] = 0;
        int ret = 0;
        for (int i = 1; i <= n; i++)
        {
            if (nums[i - 1] > 0)
            {
                f[i] = f[i - 1] + 1;
                if (g[i - 1] > 0)g[i] = g[i - 1] + 1;

            }
            else if (nums[i - 1] < 0)
            {
                if (g[i - 1] > 0)f[i] = g[i - 1] + 1;
                g[i] = f[i - 1] + 1;
            }
            else {
                g[i] = f[i] = 0;
            }
            ret = max(f[i], ret);
        }
        return ret;
    }
};