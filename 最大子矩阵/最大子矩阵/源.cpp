#include <algorithm>
#include <cstring>
#include <iostream>
using namespace std;
const int N = 110;
int sum[N][N];
int main() {
    int n;
    cin >> n;
    int ret = -200;
    memset(sum, 0, sizeof sum);
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= n; j++)
        {
            int tmp;
            cin >> tmp;
            sum[i][j] = sum[i - 1][j] + sum[i][j - 1] + tmp - sum[i - 1][j - 1];
        }
    for (int i1 = 1; i1 <= n; i1++)
        for (int j1 = 1; j1 <= n; j1++)
            for(int i2=i1;i2<=n;i2++)
                for (int j2 = j1; j2 <= n; j2++)
                {
                    ret = max(ret, sum[i2][j2] -sum[i2][j1-1] - sum[i1-1][j2] + sum[i1-1][j1-1]);
                }
    cout << ret;
    return 0;

}
// 64 λ������� printf("%lld")