#include <iostream>
#include<queue>
using namespace std;
typedef long long ll;
ll ans = 0, k;
int n;
int main() {
    priority_queue<int> nums;
    ll tmp;
    cin >> n >> k;
    for (int i = 0; i < n; i++)
    {
        cin >> tmp;
        ans += tmp;
        if (tmp % 2 == 0)
            nums.push(tmp);
    }
    for (int i = 0; i < k; i++)
    {
        if (nums.empty())break;
        tmp = nums.top() / 2;
        nums.pop();
        ans -= tmp;
        if (tmp % 2 == 0)
            nums.push(tmp);
    }
    cout << ans;
    return 0;

}
// 64 λ������� printf("%lld")