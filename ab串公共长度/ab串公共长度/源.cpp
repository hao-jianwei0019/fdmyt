#include <cstring>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main() {
    string s1;
    string s2;
    cin >> s1 >> s2;
    if (s1.size() > s2.size())swap(s1, s2);//s1为小串
    int n = s1.size(), m = s2.size();
    vector<vector<int>>dp(n + 1, vector<int>(m + 1));
    s1 = " " + s1;
    s2 = " " + s2;
    pair<int, int>ret;
    ret.first = 0;
    ret.second = 0;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
        {
            if (s1[i] == s2[j])dp[i][j] = dp[i - 1][j - 1] + 1;
            if (dp[i][j] > ret.first)
            {
                ret.first = dp[i][j];
                ret.second = i;
            }
        }
    auto c = s1.begin() + ret.second - ret.first + 1;
    while (c <= s1.begin() + ret.second)
    {
        cout << *c;
        c++;
    }


}
// 64 位输出请用 printf("%lld")