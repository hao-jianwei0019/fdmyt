#include <iostream>
#include<vector>
#include<algorithm>
using namespace std;
typedef pair<int, int> Arr;

const int SIZE = 100010;
vector<int> cnt(SIZE, 0);//存赞

int main()
{

    int N, D, K;
    cin >> N >> D >> K;
    vector<Arr>arr(N);//存数据
    vector<bool>st(SIZE, false);
    for (int i = 0; i < N; i++)
    {
        int tmp1, tmp2;
        cin >> tmp1 >> tmp2;
        arr[i].first = tmp1;
        arr[i].second = tmp2;
    }
    sort(arr.begin(), arr.end());
    for (int l = 0, r = 0; r < N; r++)  //滑动窗口的左右
    {
        int t = arr[r].second;
        cnt[t]++;
        while (arr[r].first - arr[l].first >= D)
        {
            cnt[arr[l].second]--;
            l++;
        }
        if (cnt[t] >= K) st[t] = true;
    }
    for (int i = 0; i < SIZE; i++)
    {
        if (st[i])//如果为真，则把热帖的id打印出来
            cout << i << endl;
    }

    return 0;
}