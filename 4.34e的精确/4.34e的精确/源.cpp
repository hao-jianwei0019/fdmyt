#include <iostream>
#include <math.h>
#include<iomanip>
using namespace std;
int main()
{
	int i, j,n;
	double item, sum1,sum2,x,an;
	sum1 = 1.0;
	sum2 = 1.0;
	cout << "n的值和x的值\n";
	cin >> n>>x;
	for (i = 1; i <= n; i++) {
		item = 1;
		
		for (j = 1; j <= i; j++) {
			item = item * j;
		}
		an = pow(x, i) / item;//pow函数用法： pow（底数，指数）
		item = 1.0 / item;
		sum1 = sum1 + item;
		sum2 = sum2 +an;
	}
	cout <<"e的近似值" <<fixed << setprecision(6) << sum1<<endl;//setprecision()函数用法： 在cout中要输出数据前加<<fixed<<setprecision(n)，就是为小数点后保留n位有效数据
	cout <<"e的n次方近似值" << fixed << setprecision(6) << sum2<<endl;
	return 0;
}