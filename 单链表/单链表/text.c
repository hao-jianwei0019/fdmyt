#include "slist.h"
void test1()
{
	SListNode* list1 = NULL;
	SListPrint(list1);
	SListPushBack(&list1, 1);
	SListPushBack(&list1, 2);
	SListPushBack(&list1, 3);
	SListPushBack(&list1, 4);
	SListPushFront(&list1, 1);
	SListPushFront(&list1, 2);
	SListPushFront(&list1, 3);
	SListPushFront(&list1, 4);
	SListPopBack(&list1);
	SListPopBack(&list1);
	SListPopBack(&list1);
	SListPopFront(&list1);
	SListPopFront(&list1);
	SListPopFront(&list1);
	SListPopFront(&list1);
	SListPrint(list1);

	SListDestroy(list1);

}
int main()
{
	test1();
	return 0;
}