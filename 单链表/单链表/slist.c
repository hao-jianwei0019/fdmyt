#include "slist.h"
SListNode* BuySListNode(SLTDateType x)
{
	SListNode* head = (SListNode*)malloc(sizeof(SListNode));
	if (head == NULL)
	{
		perror("malloc");
		return;
	}
	head->data = x;
	head->next = NULL;
	return head;
}
void SListPrint(SListNode* plist)
{
	SListNode* cur = plist;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");

}
void SListPushBack(SListNode** pplist, SLTDateType x)
{
	SListNode* newnode = BuySListNode(x);
	if (*pplist == NULL)
	{
		*pplist = newnode;
	}
	else {
		SListNode* tail = *pplist;
		while (tail->next != NULL)
		{
			tail = tail->next;

		}
		tail->next = newnode;

	}
}
void SListPushFront(SListNode** pplist, SLTDateType x)
{


	SListNode* newnode = BuySListNode(x);
	if (*pplist == NULL)
	{
		*pplist = newnode;
	}
	else
	{
		newnode->next = *pplist;
		*pplist = newnode;
	}
}
void SListPopBack(SListNode** pplist)
{
	//没有节点
	assert(pplist);
	assert(*pplist);
	//只有一个节点
	if ((*pplist)->next == NULL)
	{
		free(*pplist);
		*pplist = NULL;
		
	}
	else
	{
		SListNode* tail = *pplist;
		SListNode* cur = *pplist;

		while (tail->next != NULL)
		{
			cur = tail;
			tail = tail->next;
		}
		free(tail);
		tail = NULL;
		cur->next = NULL;
	}
}
void SListPopFront(SListNode** pplist)
{
	//没有节点
	assert(*pplist);
	//只有一个节点
	if ((*pplist)->next == NULL)
	{
		free(*pplist);
		*pplist = NULL;
	}
	else
	{
		SListNode* tail = (*pplist)->next;
		free(*pplist);
		*pplist = tail;
		
	}
}
SListNode* SListFind(SListNode* plist, SLTDateType x)
{
	assert(plist);
	SListNode* tail = plist;
	while (tail != NULL)
	{
		if (tail->data == x)
			return tail;
		tail = tail->next;
	}
	return NULL;
}
void SListInsertAfter(SListNode* pos, SLTDateType x)
{
	assert(pos);
	SListNode* newnode = BuySListNode(x);
	SListNode* cur = pos->next;
	pos->next = newnode;
	newnode->next = cur;

}
void SListEraseAfter(SListNode** pplist, SListNode* pos)
{
	assert(pplist);
	assert(pos);
	//头
	if (*pplist == pos)
	{
		void SListPopFront(pplist);
	}
	else {
	
		SListNode* prev = *pplist;
		while (prev->next != pos)
		{
			prev = prev->next;
		}
		prev->next = pos->next;
		free(pos);

	}

}
void SListDestroy(SListNode* plist)
{
	SListNode* cur = plist;
	while (cur)
	{
		SListNode* tmp = cur->next;
		free(cur);
		cur = tmp;

	}

}