#include<iostream>
#include<vector>
#include<map>
#include<algorithm>
using namespace std;
const int SIZE = 10010;
int n, m;
int a[SIZE];
int b[SIZE];
int main()
{
    cin >> n>> m;
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    int num=0;
    for (int i = 0; i < m; i++)
    {
        cin >> b[i];
    }
    sort(a, a + n);
    sort(b, b + m);
    vector<int>a1;
    vector<int>b1;
    for (int i = 0; i < n; i++)
    {
        int  tmp = 1;
        while(i < n&&a[i] == a[i + 1])
        {
            i++,tmp++;
        }
        a1.push_back(tmp);
        
    }
    for (int i = 0; i < m; i++)
    {
        int  tmp = 1;
        while (i < n&&b[i] == b[i + 1])
        {
            i++,tmp++;
        }
        b1.push_back(tmp);     
    }
    vector<vector<int>>dp(a1.size()+1, vector<int>(b1.size()+1));//当霍霍在（i，j）时，岁阳被成功捕获的位置数
    int ans = 0;
    for (int i = 1; i < a1.size()+1; i++)
    {
        for (int j = 1; j < b1.size()+1; j++)
        {
            dp[i][j] = dp[i - 1][j] + dp[i][j - 1]-dp[i-1][j-1] + a1[i - 1] * b1[j - 1];
            ans += dp[i][j]* a1[i - 1] * b1[j - 1];
        }
    }
    cout << ans;
    return 0;
}
//dp[i][j]=dp[i-1][j]+dp[i][j-1]+a1[i]*a1[1]+b1[j]*b1[j];