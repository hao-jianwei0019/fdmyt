#include <iostream>
#include <array>
#include <string>
#include <istream>
#include <iomanip>
using namespace std;
#define ROW 10
#define COL ROW*2+1

int main()
{
	int col = COL;
	int mid = col / 2 + 1;;
	int arr[ROW][COL] = { 0 };
	arr[0][mid] = 1;
	int i;
	int j;
	int k;
	for (i = 1; i < ROW; i++)
	{
		for (k = 0; k <= i; k++)
		{
			arr[i][mid + k] = arr[i - 1][mid + k - 1] + arr[i - 1][mid + k + 1];
			arr[i][mid - k] = arr[i - 1][mid - k - 1] + arr[i - 1][mid - k + 1];
		}
	}
	for (i = 0; i < ROW; i++)
	{
		for (j = 1; j < COL; j++)
		{
			if (arr[i][j] == 0)
				cout << setw(3) << " ";
				//cout << " \t";
			else
				cout << setw(3) << arr[i][j];
				//cout << arr[i][j] << "\t";
		}
		cout << endl;
	}
	return 0;
}