#include <stdio.h>
#include <assert.h>
#include <string.h>
int my_strcmp(const char* e1, const char* e2)
{
	assert(e1 && e2);
	while (*e1 == *e2)
	{
		e1++;
		e2++;
		if (*e1 == '\0')//这里判断一个为‘\0’即可，因为若只有一个结束另一个不结束会直接减；'\0'是最小的
		{
			return 0;
		}
	}
	return *e1 - *e2;
}
int main()
{
	char arr[] = "hello ";
	char str[] = "hellow";
	char arr2[] = "hel";
	int b = my_strcmp(arr, str);
	int c = my_strcmp(arr, arr2);
	printf("%d %d",b, c);
	return 0;
}