const int N = 10010;
class Solution {
public:


    int nums[N];
    int vis[N];
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        // write code here
        vector<int>ans;
        int n = nums1.size();
        for (int i = 0; i < n; i++)
        {
            nums[nums1[i]]++;
        }
        n = nums2.size();
        for (int i = 0; i < n; i++)
        {
            if (nums[nums2[i]])
            {
                if (vis[nums2[i]] == 0)
                    ans.push_back(nums2[i]);
                vis[nums2[i]]++;
            }
        }
        return ans;
    }
};
