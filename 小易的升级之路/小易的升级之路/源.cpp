#include <iostream>
#include <numeric>
using namespace std;
typedef long long ll;
int main() {
    ll x;
    int n;
    cin >> n >> x;
    for (int i = 0; i < n; i++)
    {
        ll tmp;
        cin >> tmp;
        if (tmp <= x)x += tmp;
        else x += gcd(x, tmp);
    }
    cout << x;
    return 0;
}
// 64 λ������� printf("%lld")