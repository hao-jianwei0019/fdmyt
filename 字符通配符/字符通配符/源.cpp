#include <iostream>
#include <string>
#include <vector>
using namespace std;

bool ispipei(char a)
{
    if (a >= '0' && a <= '9' || a >= 'a' && a <= 'z' || a >= 'A' && a <= 'Z')return true;
    return false;
}
bool isdeng(char c1, char c2)
{
    if (c1 == c2)return true;
    if (c1 >= 'a' && c1 <= 'z') return (c1 + 'A' - 'a') == c2;
    else if (c1 >= 'A' && c1 <= 'Z') return (c1 + 'a' - 'A') == c2;
    return false;
}
bool match(string s1, string s2)
{
    s1 = ' ' + s1;//i
    s2 = ' ' + s2;//j
    //j macth i
    vector<vector<bool>> dp(s1.size() + 1, vector<bool>(s2.size() + 1, false));
    dp[0][0] = true;
    for (int i = 1; i < s2.size(); i++)
    {
        if (s2[i] == '*')dp[0][i] = true;
        else break;
    }
    for (int i = 1; i <= s1.size(); i++)
    {
        for (int j = 1; j <= s2.size(); j++)
        {
            if (s2[j] == '*')
                dp[i][j] = dp[i - 1][j] && ispipei(s1[i]) || dp[i][j - 1];//前面是匹配多个字符，后面是匹配空串
            else dp[i][j] = ((s2[j] == '?' && ispipei(s1[i])) || isdeng(s1[i], s2[j])) && dp[i - 1][j - 1];

        }
    }
    return dp[s1.size()][s2.size()];
}
int main() {
    string s1;
    string s2;
    cin >> s1 >> s2;
    if (match(s2, s1))cout << "true";
    else cout << "false";
    return 0;
}
// 64 位输出请用 printf("%lld")