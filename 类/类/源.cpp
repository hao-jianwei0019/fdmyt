#include<iostream>
using namespace std;
//class Date
//{
//public:
//    // 1.无参构造函数
//    Date()
//    {
//        cout << "Date()"<<endl;
//    }
//    // 2.带参构造函数
//    Date(int year, int month, int day)
//    {
//        _year = year;
//        _month = month;
//        _day = day;
//        cout << " Date(int year, int month, int day)" << endl;
//
//    }
//private:
//    int _year;
//    int _month;
//    int _day;
//};
//void TestDate()
//{
//    Date d1; // 调用无参构造函数
//    Date d2(2015, 1, 1); // 调用带参的构造函数
//    // 注意：如果通过无参构造函数创建对象时，对象后面不用跟括号，否则就成了函数声明
//    // 以下代码的函数：声明了d3函数，该函数无参，返回一个日期类型的对象
//    // warning C4930: “Date d3(void)”: 未调用原型函数(是否是有意用变量定义的?)
//    Date d3();
//}
//int main()
//{
//    TestDate();
//}
class Date
{
public:
    Date(int year = 1900, int month = 1, int day = 1)
    {
        _year = year;
        _month = month;
        _day = day;
    }
    int _year;
    int _month;
    int _day;
};

bool operator==(const Date& d1, const Date& d2)
{
    return d1._year == d2._year
        && d1._month == d2._month
        && d1._day == d2._day;
}
void Test()
{
    Date d1(2018, 9, 26);
    Date d2(2018, 9, 27);
    cout << (d1 == d2) << endl;
}
int main()
{
    Test();

}