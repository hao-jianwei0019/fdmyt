#include <stdio.h>
enum Day//星期
{
	Mon,
	Tues,
	Wed,
	Thur,
	Fri,
	Sat,
	Sun
};
enum Sex//性别
{
	MALE,
	FEMALE,
	SECRET
};
//enum Color//颜色
//{
//	RED = 1,
//	GREEN = 4,
//	BLUE 
//};
//int main()
//{
//	printf("%d  %d  %d", 
//		RED, GREEN, BLUE);
//}
enum Color//颜色
{
	RED = 1,
	GREEN = 2,
	BLUE = 4
};
enum Color clr = GREEN;//只能拿枚举常量给枚举变量赋值，才不会出现类型的差异。
clr = 5;