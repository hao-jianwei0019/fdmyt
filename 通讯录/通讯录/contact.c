#include "contact.h"
//静态版本
//void  InitContact(Contact* pc)
//{
//	assert(pc);
//	pc->sz = 0;
//	memset(pc->data, 0, sizeof(pc->data));
//}
//动态
void  InitContact(Contact* pc)
{
	assert(pc);
	pc->sz = 0;
	PeoInfo* ptr=(PeoInfo*)calloc( DEFAULT_SZ,sizeof(PeoInfo) );
	if (ptr == NULL)
	{
		perror("InitContact::calloc");
		return;
	}
	pc->data = ptr;
	pc->capacity = DEFAULT_SZ;
}
//静态
//void Addcontact(Contact* pc)
//{
//	assert(pc);
//	if (pc->sz == MAX)
//	{
//		printf("通讯录已满，无法添加\n");
//		return;
//	}
//	//添加信息
//	printf("请输入名字");
//	scanf("%s", pc->data[pc->sz].name);
//	printf("请输入年龄");
//	scanf("%d", &(pc->data[pc->sz].age));
//	printf("请输入性别");
//	scanf("%s", pc->data[pc->sz].sex);
//	printf("请输入地址");
//	scanf("%s", pc->data[pc->sz].addr);
//	printf("请输入电话");
//	scanf("%s", pc->data[pc->sz].tele);
//	pc->sz++;
//}

//动态
void check_capacity(Contact* pc)
{
	if (pc->sz == pc->capacity)
	{
		PeoInfo* ptr =(PeoInfo*)realloc(pc->data, (pc->capacity + INC_SZ) * sizeof(PeoInfo));
		if (ptr == NULL)
		{
			perror("check_capacity::calloc");
			return;
		}
		pc->data = ptr;
		pc->capacity += INC_SZ;
	}
}
void Addcontact(Contact* pc)
{
	assert(pc);
	check_capacity(pc);
	
	//添加信息
	printf("请输入名字");
	scanf("%s", pc->data[pc->sz].name);
	printf("请输入年龄");
	scanf("%d", &(pc->data[pc->sz].age));
	printf("请输入性别");
	scanf("%s", pc->data[pc->sz].sex);
	printf("请输入地址");
	scanf("%s", pc->data[pc->sz].addr);
	printf("请输入电话");
	scanf("%s", pc->data[pc->sz].tele);
	pc->sz++;
}
void Showcontact(const Contact* pc)
{
	assert(pc);
	if (pc->sz == 0)
	{
		printf("通讯录为空\n");
		return;
	}
	int i;
	printf("%-20s\t%-4s\t%-5s\t%-20s\t%-12s\n", "名字", "年龄", "性别", "地址", "电话");
	for (i = 0; i < pc->sz; i++)
	{
		printf("%-20s\t%-4d\t%-5s\t%-20s\t%-12s\n", pc->data[i].name,
			pc->data[i].age,
			pc->data[i].sex,
			pc->data[i].addr,
			pc->data[i].tele);
	}
}
//负号右对齐

int FindByName(const Contact* pc, char name[])
{
	assert(pc);
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		if (strcmp(pc->data[i].name, name) == 0)
		{
			return i;
		}
	}
	return -1;
}
void Findpeople(const Contact* pc)
{
	assert(pc);
	char name[NAME_MAX] = { 0 };

	if (pc->sz == 0)
	{
		printf("通讯录为空，无法查找\n");
		return;
	}
	printf("请输入要查找的人的名字:>");
	scanf("%s", name);
	int i = FindByName(pc, name);
	printf("%-20s\t%-4s\t%-5s\t%-20s\t%-12s\n", "名字", "年龄", "性别", "地址", "电话");
	printf("%-20s\t%-4d\t%-5s\t%-20s\t%-12s\n", pc->data[i].name,
		pc->data[i].age,
		pc->data[i].sex,
		pc->data[i].addr,
		pc->data[i].tele);
}
void Delcontact(Contact* pc)
{
	assert(pc);
	char name[NAME_MAX] = { 0 };
	if (pc->sz == 0)
	{
		printf("通讯录为空，无法删除\n");
		return;
	}
	//删除
	//找到要删除的人
	printf("请输入要删除的人的名字:>");
	scanf("%s", name);
	int ret = FindByName(pc, name);
	if (ret==-1)
	{
		printf("要删除的人不存在\n");
		return;
	}

	int i = ret;
	//删除
	for (i = ret; i < pc->sz - 1; i++)
	{
		pc->data[i] = pc->data[i + 1];
	}
	//pc->data[i] = pc->data[pc->sz-1];
	pc->sz--;
	printf("删除成功\n");
}

void Modifycontact(Contact* pc)
{
	assert(pc);
	char name[NAME_MAX] = { 0 };
	if (pc->sz == 0)
	{
		printf("通讯录为空，无法修改\n");
		return;
	}
	
	printf("请输入要修改的人的名字:>");
	scanf("%s", name);
	int ret = FindByName(pc, name);
	if (ret == -1)
	{
		printf("要删除的人不存在\n");
		return;
	}
	printf("请输入名字");
	scanf("%s", pc->data[ret].name);
	printf("请输入年龄");
	scanf("%d", &(pc->data[ret].age));
	printf("请输入性别");
	scanf("%s", pc->data[ret].sex);
	printf("请输入地址");
	scanf("%s", pc->data[ret].addr);
	printf("请输入电话");
	scanf("%s", pc->data[ret].tele);
	printf("修改完成\n");
}
int int_com(const void* e1, const void* e2)
{
	return ((*(PeoInfo*)e1).age) - ((*(PeoInfo*)e2).age);//升序，逆序则这两个数反一下
}

void DestoryContact(Contact* pc)
{
	free(pc->data);
	pc->data = NULL;
	pc->capacity = 0;
	pc->sz = 0;
	pc = NULL;

}
