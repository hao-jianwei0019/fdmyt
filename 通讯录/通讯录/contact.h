#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#define MAX 100
#define NAME_MAX 20
#define SEX_MAX 5
#define ADDR_MAX 30
#define TELE_MAX 12

#define DEFAULT_SZ   3//默认大小
#define INC_SZ 2//
typedef struct PeoInfo
{
	char name[NAME_MAX];
	int age;
	char sex[SEX_MAX];
	char addr[ADDR_MAX];
	char tele[TELE_MAX];
}PeoInfo;
//静态
//typedef struct Contact
//{
//	PeoInfo data[MAX];//存放人的信息的
//	int sz;//当前已经放的信息的个数
//}Contact;
//动态
typedef struct Contact
{
	PeoInfo *data;//指向(存放人的信息的)空间
	int sz;//当前已经放的信息的个数
	int capacity;//最大容量
}Contact;

//初始化
void  InitContact(Contact* pc);
//添加新用户
void Addcontact(Contact* pc);
//显示
void Showcontact(const Contact*pc);
//删除
void Delcontact(Contact* pc);
//查找
void Findpeople(const Contact* pc);
//修改
void Modifycontact(Contact* pc);

int int_com(const void* e1, const void* e2);
//销毁
void DestoryContact(Contact* pc);


