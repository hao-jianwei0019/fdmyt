//实现一个通讯录；
//
//通讯录可以用来存储1000个人的信息，每个人的信息包括：姓名、性别、年龄、电话、住址
//
//提供方法：
//
//添加联系人信息
//删除指定联系人信息
//查找指定联系人信息
//修改指定联系人信息
//显示所有联系人信息
//清空所有联系人
//以名字排序所有联系人
#include "contact.h"
enum function
{
	Exit,
	Add,
	Delete,
	Find,
	Modify,
	Show,
	empty,
	sort
};
void menu()
{
	printf("************************************\n");
	printf("******  1. add    2. del      ******\n");
	printf("******  3. search 4. modify   ******\n");
	printf("******  5. show   6. empty    ******\n");
	printf("******  7. sort.age  0. exit  ******\n");
	printf("************************************\n");
}
int main()
{
	int input;
	Contact con;
	InitContact(&con);
	do
	{
		menu();
		printf("ovo :");
		scanf("%d", &input);
		


		switch (input)
		{
		case Add:
			Addcontact(&con);
			break;
		case Delete:
			Delcontact(&con);
			break;
		case Find:
			Findpeople(&con);
			break;
		case Modify:
			Modifycontact(&con);
			break;
		case Show:
			Showcontact(&con);
			break;
		case empty:
			InitContact(&con);
			printf("清除成功");
			break;
		case sort:
			qsort(con.data, con.sz, sizeof(PeoInfo), int_com);
			break;
		case Exit:
			DestoryContact(&con);
			printf("退出系统，欢迎下次使用");
			break;

		default:
			printf("输入错误，请重新输入");
			break;
		}

	} while (input);
	return 0;
}
