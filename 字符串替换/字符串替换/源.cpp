class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param str string字符串 原串
     * @param arg char字符型vector 需替换数组
     * @return string字符串
     */
    string formatString(string str, vector<char>& arg) {
        // write code here
        int k = 0;
        int n = str.size();
        string ans;

        for (int i = 0; i < n; i++)
        {
            if (str[i] != '%')
            {
                ans += str[i];
            }
            else {
                ans += arg[k];
                k++;
                i++;
            }
        }
        for (; k < arg.size(); k++)
        {
            ans += arg[k];
        }
        return ans;
    }
};