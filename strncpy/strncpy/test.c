#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <assert.h>
#include <string.h>
char* my_strncpy(char* e1, char* e2,size_t n)
{
	assert(e1 && e2);
	const char* start = e1;
	int i;
	for(i=0;i<n;i++)
	{
		if (*e2 == '\0')
		{
			*e1++ = '\0';
		}
		else
		*e1++ = *e2++;
	}
	*e1 = '\0';
	return start;
}
int main()
{
	char arr[] = "hello 23wefc23";
	char str[] = "world";
	my_strncpy(arr, str,7);
	//strncpy(arr, str, 7);
	printf("%s", arr);
	return 0;

}