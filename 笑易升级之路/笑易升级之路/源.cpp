#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
int gcd(int a, int b)
{
    if (a < b)swap(a, b);

    while (int tmp = a % b)
    {
        a = b;
        b = tmp;
    }
    return b;
}
int main() {
    int a, n;
    while (cin >> a >> n)
    {
        int ret = n;
        int tmp;
        for (int i = 0; i < a; i++)
        {
            cin >> tmp;
            if (tmp <= ret)ret += tmp;
            else
            {
                ret += gcd(ret, tmp);
            }

        }
        cout << ret << endl;
    }
}
// 64 λ������� printf("%lld")