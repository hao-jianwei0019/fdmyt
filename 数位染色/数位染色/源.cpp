#include <cstring>
#include <iostream>
using namespace std;
int nums[20];
int main() {
    string s;
    cin >> s;
    int n = s.size();
    int sum = 0;
    for (int i = 1; i <= n; i++)
    {
        nums[i] = s[i - 1] - '0';
        sum += nums[i];
    }
    if (sum % 2 != 0)
    {
        cout << "No" << endl;
        return 0;
    }
    sum /= 2;
    bool dp[sum + 1];
    memset(dp, false, sizeof dp);
    dp[0] = true;
    for (int i = 1; i <= n; i++)
    {
        int tmp = nums[i];
        for (int j = sum; j >= 0; j--)
        {
            if (dp[j] && j + tmp <= sum)dp[j + tmp] = true;
        }

    }
    if (dp[sum])cout << "Yes" << endl;
    else cout << "No" << endl;
    return 0;
}
// 64 λ������� printf("%lld")