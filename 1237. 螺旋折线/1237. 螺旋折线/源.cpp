#include<iostream>
using namespace std;

//时间复杂度太高
//int main()
//{
//    double x, y;
//    cin >> x >> y;
//    double ans = 0;
//    double a = 0, b = 0;
//    int dir = 0;
//    int o = 0;
//    long long s = 1, z = 0;
//    while (a != x || b != y)
//    {
//        if (dir == 0)//右
//        {
//            a--;
//        }
//        else if (dir == 1)//左
//        {
//            b++;
//        }
//        else if (dir == 2)//上
//        {
//            a++;
//        }
//        else if (dir == 3)//下
//        {
//            b--;
//        }
//        z++;
//        if (z == s)
//        {
//            dir = (dir + 1) % 4;//端点
//            s++;
//            z = 0;
//        }
//
//        ans++;
//    }
//    cout << ans;
//    return 0;
//}
//#include <bits/stdc++.h>


int main()
{
    long long x, y;
    cin >> x >> y;
    //找层数
    long long k = max(abs(x), abs(y));
    long long ans;
    if (x >= y)ans = 4 * k * k + abs(x - k) + abs(y - k);
    else ans = 4 * k * k - abs(x - k) - abs(y - k);
    cout << ans;

    return 0;
}
