#include <climits>
#include <iostream>
#include <string>
using namespace std;
typedef long long ll;
int main() {
    string s;
    cin >> s;
    ll ans = -1;
    int n = s.size();

    for (char ch = 'a'; ch <= 'z'; ch++)
    {
        ll tmp = 0;
        for (int i = 0; i < n; i++)
        {
            tmp += min(abs(s[i] - ch), 26 - abs(ch - s[i]));
        }
        if (ans == -1)ans = tmp;
        else ans = min(tmp, ans);
    }
    cout << ans << endl;
    return 0;

}
// 64 λ������� printf("%lld")