#include <iostream>
using namespace std;
int t, h;
int func()
{
    int ret = 0, a = 1;
    while (h)
    {
        h -= a;
        ret++;
        if (h % (a * 2) == 0)
        {
            a *= 2;
        }
    }
    return ret;
}
int main() {
    cin >> t;
    while (t--)
    {
        cin >> h;
        cout << func() << endl;
    }
    return 0;
}
// 64 λ������� printf("%lld")