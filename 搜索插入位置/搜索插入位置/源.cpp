class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {
        int left = 0;
        int right = nums.size() - 1;
        int mid = 0;
        if (target > nums[right])
            return right + 1;
        else if (target <= nums[0])
            return 0;
        while (1)
        {
            mid = (left + right) / 2;
            if (right - left == 1)
            {
                return right;
            }
            else if (target > nums[mid])
            {
                left = mid;
            }
            else if (target <= nums[mid])
            {
                right = mid;
            }
        }

    }
};