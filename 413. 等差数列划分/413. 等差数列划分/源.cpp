class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& nums) {
        int n = nums.size();
        vector<int>dp(n, 0);
        int ret = 0;
        for (int i = 1; i < n - 1; i++)
        {
            if (nums[i + 1] - nums[i] == nums[i] - nums[i - 1])
            {
                dp[i] = dp[i - 1] + 1;
            }
            ret += dp[i];
        }
        return ret;
    }
};