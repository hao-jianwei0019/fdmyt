#include <iostream>
#include <vector>
using namespace std;
class Ant
{
public:
    int x;
    int y;
    int dir;
};
void go_f(Ant& ant)
{
    switch (ant.dir)
    {
    case 0:
        ant.x--;
        break;
    case 1:
        ant.y++;
        break;
    case 2:
        ant.x++;
        break;
    case 3:
        ant.y--;
        break;
    }
}
int main()
{
    int m, n;
    cin >> m >> n;
    vector<vector<int>> board(m, vector<int>(n, 0));
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cin >> board[i][j];
        }
    }
    int x, y;
    char s;
    int k;
    Ant ant;
    cin >> x >> y >> s >> k;
    ant.x = x;
    ant.y = y;
 
    switch (s)
    {
    case 'U':
        ant.dir = 0;
        break;
    case 'R':
        ant.dir = 1;
        break;
    case 'D':
        ant.dir = 2;
        break;
    case 'L':
        ant.dir = 3;
        break;
    }
    while (k)
    {
        if (board[ant.x][ant.y] == 0)//��
        {
            board[ant.x][ant.y] = 1;
            ant.dir = (ant.dir - 1 + 4) % 4;
        }
        else
        {
            board[ant.x][ant.y] = 0;
            ant.dir = (ant.dir + 1 + 4) % 4;
        }
        go_f(ant);
        k--;
    }
    cout << ant.x << ' ' << ant.y;

}