#include <cmath>
#include <iostream>
using namespace std;
typedef long long ll;
bool issu(ll num)
{
    for (int i = 2; i < sqrt(num) + 1; i++)
    {
        if (num % i == 0)return false;
    }
    return true;
}
int main() {
    string s;
    cin >> s;
    ll num = 0;
    int n = s.size();
    for (int i = 0; i < n; i++)
    {
        num = num * 10 + (s[i] - '0');
    }
    for (int i = n - 2; i >= 0; i--)
    {
        num = num * 10 + (s[i] - '0');
    }
    if (issu(num))cout << "prime" << endl;
    else cout << "noprime" << endl;
    return 0;
}
// 64 λ������� printf("%lld")