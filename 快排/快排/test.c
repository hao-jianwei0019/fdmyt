#include "quicksort.h"
void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
void TestQuickSort()
{
	//int a[] = { 6,1,2,7,9,3,4,5,10,8 };
	int a[] = { 6,1,2,6,9,3,4,6,10,8 };
	PrintArray(a, sizeof(a) / sizeof(int));

	QuickSortNonR(a, 0, sizeof(a) / sizeof(int) - 1);


	PrintArray(a, sizeof(a) / sizeof(int));
}
int main()
{
	TestQuickSort();


}