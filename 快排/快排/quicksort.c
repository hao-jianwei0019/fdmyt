#include "quicksort.h"
// 快速排序递归实现
// 快速排序hoare版本
void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}
//三数取中
int GetMidNumi(int* a, int left, int right)
{
	int mid = (left + right) / 2;
	if (a[left] < a[mid])
	{
		if (a[mid] < a[right])
		{
			return mid;
		}
		else if (a[left] > a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
	else // a[left] > a[mid]
	{
		if (a[mid] > a[right])
		{
			return mid;
		}
		else if (a[left] < a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
}
int PartSort1(int* a, int left, int right)
{
	
	int mid = GetMidNumi(a, left, right);

	if (mid != left)
		Swap(&a[mid], &a[left]);

	int keyi = left;
	
	while (left < right)
	{
		
		while (left < right && a[right] >= a[keyi])
		{
			right--;
		}
		while (left < right && a[left] <= a[keyi])
		{
			left++;
		}
		Swap(&a[right], &a[left]);

	}
	Swap(&a[keyi], &a[left]);
	keyi = left;
	return keyi;

}
// 快速排序挖坑法
int PartSort2(int* a, int left, int right)
{
	int mid = GetMidNumi(a, left, right);

	if (mid != left)
		Swap(&a[mid], &a[left]);

	int key = a[left];
	while (left < right)
	{
		while (left < right && a[right] >= key)
		{
			right--;

		}
		a[left] = a[right];
		while (left < right && a[left] <= key)
		{
			left++;
		}
		a[right] = a[left];
	}
	a[left] = key;
	return left;

}
// 快速排序前后指针法
int PartSort3(int* a, int left, int right)
{
	int mid = GetMidNumi(a, left, right);

	if (mid != left)
		Swap(&a[mid], &a[left]);

	int keyi = left;
	int prev = left;
	int cur = left + 1;
	while (cur <= right)
	{
		if (a[cur] < a[keyi] && ++prev != cur)//我个人感觉这个目的就是prev几乎是遍历前面，而这样可以保证prev经过的数全部小于a[keyi]
			Swap(&a[cur], &a[prev]);

		++cur;

	}
	Swap(&a[prev], &a[keyi]);
	return prev;


}
void QuickSort(int* a, int left, int right)
{
	if (left >= right)
		return;

	int keyi = PartSort3(a, left, right);
	QuickSort(a, left, keyi - 1);
	QuickSort(a, keyi + 1, right);
}

// 快速排序 非递归实现
void QuickSortNonR(int* a, int left, int right)
{
	ST ps;
	STInit(&ps);
	STPush(&ps, right);

	STPush(&ps, left);
	while (!STEmpty(&ps))
	{
		int begin = STTop(&ps);
		STPop(&ps);
		int end= STTop(&ps);
		STPop(&ps);
		int keyi=PartSort1(a, begin, end);
		//先进右边，再进左边
		if (keyi + 1 < end)
		{
			STPush(&ps, end);
			STPush(&ps, keyi + 1);
		}
		if (keyi - 1 > begin)
		{
			STPush(&ps, keyi - 1);
			STPush(&ps, begin);
		}
	}

	STDestroy(&ps);


}
