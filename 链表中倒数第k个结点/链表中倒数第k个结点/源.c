/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 */
#include <stdio.h>
struct ListNode {
    int val;
    struct ListNode* next;
    
};
 /**
  *
  * @param pListHead ListNode��
  * @param k int����
  * @return ListNode��
  */
struct ListNode* FindKthToTail(struct ListNode* pListHead, int k) {
    // write code here
    struct ListNode* fast = pListHead;
    struct ListNode* slow = pListHead;
    if (k == 0)
        return NULL;
    for (int i = 1; i < k; i++)
    {
        if (fast == NULL)
            return NULL;
        fast = fast->next;
    }
    if (fast == NULL)
        return NULL;
    while (fast->next)
    {
        fast = fast->next;
        slow = slow->next;
    }
    return slow;
}