#include <iostream>
#include<string>
#include <queue>
using namespace std;
string ans;
int nums[27];
int n;

int main() {
    cin >> n;
    char tmp;
    int max = 0;
    for (int i = 0; i < n; i++)
    {
        cin >> tmp;
        nums[tmp - 'a']++;
        if (nums[tmp - 'a'] > max)max = nums[tmp - 'a'];

    }
    if (max > (n + 1) / 2)
        cout << "no";
    else
    {
        cout << "yes" << endl;
        priority_queue<pair<int, char>>q;
        for (int i = 0; i < 26; i++)
        {
            if (nums[i])
                q.push({ nums[i],'a' + i });
        }
        while (!q.empty())
        {
            pair<int, char>a = q.top();
            q.pop();
            ans += a.second;


            if (q.empty())break;
            pair<int, char>b = q.top();
            q.pop();
            ans += b.second;

            if (a.first - 1 > 0)q.push({ a.first - 1,a.second });
            if (b.first - 1 > 0)q.push({ b.first - 1,b.second });
        }
        cout << ans;
    }
    return 0;

}
// 64 λ������� printf("%lld")