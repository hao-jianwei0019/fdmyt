#include <iostream>
#include<algorithm>
#include <cstring>
using namespace std;
int N;
const int M = 100010;
int nums[M];
int f[M];

void dp()
{
    if (N == 1)return;
    for (int i = 2; i <= N; i++)
    {
        f[i] = min(f[i - 1] + nums[i - 1], f[i - 2] + nums[i - 2]);
    }

}
int main() {
    memset(nums, 0, sizeof nums);
    memset(f, 0, sizeof f);

    cin >> N;
    for (int i = 0; i < N; i++)
    {
        int tmp;
        cin >> tmp;
        nums[i] = tmp;
    }
    dp();
    cout << f[N];
    return 0;

}
// 64 λ������� printf("%lld")