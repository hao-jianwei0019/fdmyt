#include <stdio.h>
void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {

    int sul = n + m - 1;
    int e1 = m - 1;
    int e2 = n - 1;
    while (e1 >=0&&e2>=0)
    {
        if (nums1[e1] > nums2[e2])
        {
            nums1[sul] = nums1[e1];
            e1--;
        }
        else
        {
            nums1[sul] = nums2[e2];
            e2--;
        }
        sul--;
    }
    if (e2 >= 0)
    {
        while (e2>=0)
        {
            nums1[sul--] = nums2[e2--];
        }
    }
}
void  test1()
{
    int arr1[6] = { 1,3,5,0,0,0 };
    int arr2[3] = {2, 4, 6};
    merge(arr1, 6,3, arr2, 3,3);
    for(int i=0;i<6;i++)
        printf(" %d", arr1[i]);


}
int main()
{
    test1();
}