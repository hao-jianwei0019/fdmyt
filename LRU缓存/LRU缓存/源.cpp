class LRUCache {
public:
    LRUCache(int capacity) { _capacity = capacity; }

    int get(int key) {
        if (nums.count(key)) {
            for (int i = 0; i < _key.size(); i++) {
                if (_key[i] == key) {
                    _key.erase(_key.begin() + i);
                    _key.push_back(key);
                    break;
                }
            }
            return nums[key];
        }
        else
            return -1;
    }

    void put(int key, int value) {
        if (nums.count(key) == 1) {
            nums[key] = value;
            for (int i = 0; i < _key.size(); i++) {
                if (_key[i] == key) {
                    _key.erase(_key.begin() + i);
                    _key.push_back(key);
                    break;
                }
            }
        }

        else {
            if (_key.size() < _capacity) {
                _key.push_back(key);
                nums[key] = value;
            }
            else {
                // cout<<_key[0]<<' ';
                nums.erase(nums.find(_key[0]));
                nums[key] = value;
                _key.erase(_key.begin());
                _key.push_back(key);
            }
        }
    }
    int _capacity;
    unordered_map<int, int> nums;
    vector<int> _key; // 越后面越新
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */