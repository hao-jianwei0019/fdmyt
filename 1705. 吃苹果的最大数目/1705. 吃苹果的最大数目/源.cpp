class Solution {
    int lan[40001];
public:
    int eatenApples(vector<int>& apples, vector<int>& days) {
        int maxlan = 0;
        int ans = 0;
        int j = 40000;
        int apple = 0;
        int i = 0;
        while (apple != 0 || i < apples.size())
        {
            if (i == j)//烂果
            {
                apple -= lan[j];
                j++;
                while (lan[j] == 0 && j <= maxlan)j++;

            }
            if (i < apples.size() && apples[i] != 0 && days[i] != 0)
            {
                apple += apples[i];//摘果
                lan[i + days[i]] += apples[i];
                j = min(j, i + days[i]);//先吃快烂的果
                maxlan = max(maxlan, i + days[i]);
            }


            if (apple > 0)//吃果
            {
                ans++;
                apple--;
                lan[j]--;
                while (lan[j] == 0 && j <= maxlan)j++;
            }
            i++;

        }
        return ans;
    }
};