#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int Fib(int a)
{
	if (a <= 2)
	{
		return 1;
	}
	else {
		return Fib(a - 1) + Fib(a - 2);
	}
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int ret = Fib(n);
	printf("%d", ret);
	return 0;
}