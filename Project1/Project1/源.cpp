#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

const int N = 17000;
int dp[N];
int main()
{
	memset(dp, 0x3f, sizeof(dp));
	dp[1] = 0;
	for (int i = 2; i < N; i++)
	{
		for (int j = 1; j < i; j++)
		{
			if (i % j == 0)dp[i] = min(dp[i], dp[j] + (i / j) + 1);
			dp[i] = min(dp[i], dp[j] + (i - j) + 2);
		}
	}
	for (int i = 1; i <= 15; i++)
	{
		cout << i << " " << dp[i] << endl;

	}
}