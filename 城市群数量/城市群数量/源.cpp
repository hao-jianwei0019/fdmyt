class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param m int整型vector<vector<>>
     * @return int整型
     */
    int ans;
    int n;
    bool vis[205];
    vector<vector<int> > board;
    void dfs(int l)
    {
        if (vis[l] == true)return;
        vis[l] = true;
        for (int i = 0; i < n; i++)
        {
            if (board[l][i] == 1 && !vis[i])dfs(i);
        }
    }
    int citys(vector<vector<int> >& m) {
        // write code here
        board = m;
        n = m.size();
        ans = 0;
        memset(vis, false, sizeof vis);
        for (int i = 0; i < n; i++)
        {
            if (!vis[i])
            {
                ans++;
                dfs(i);
            }
        }

        return ans;

    }
};