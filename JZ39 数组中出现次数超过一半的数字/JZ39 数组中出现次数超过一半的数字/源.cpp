class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param numbers int整型vector
     * @return int整型
     */
    int MoreThanHalfNum_Solution(vector<int>& numbers) {
        // write code here
        int ret = -1;
        int nus = 0;
        for (auto& ch : numbers)
        {
            if (ch != ret)
            {
                if (nus == 0)ret = ch;
                else nus--;
            }
            else nus++;
        }
        return ret;
    }
};