const int N = 1010;
int dp[N][N];
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param A string字符串
     * @return int整型
     */
    int getLongestPalindrome(string A) {
        // write code here
        int ans = 0;
        int n = A.size();
        memset(dp, 0, sizeof dp);
        for (int i = n - 1; i >= 0; i--)
            for (int j = i; j < n; j++)
            {
                if (A[i] == A[j])
                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
                if (dp[i][j])
                    ans = max(ans, j - i + 1);

            }
        return ans;
    }
};