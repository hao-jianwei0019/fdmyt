﻿class Solution {
public:
    int singleNumber(vector<int>& nums) {
        unordered_map<int, int> freq;
        for (int num : nums) {
            ++freq[num];
        }
        int ans = 0;
        for (auto [num, occ] : freq) {
            if (occ == 1) {
                ans = num;
                break;
            }
        }
        return ans;

    }
};
//
////由于数组中的元素都在 int\texttt{int}int（即 323232 位整数）范围内，因此我们可以依次计算答案的每一个二进制位是 000 还是 111。
//
//具体地，考虑答案的第 iii 个二进制位（iii 从 000 开始编号），它可能为 000 或 111。对于数组中非答案的元素，每一个元素都出现了 333 次，对应着第 iii 个二进制位的 333 个 000 或 333 个 111，无论是哪一种情况，它们的和都是 333 的倍数（即和为 000 或 333）。因此：
//
//答案的第 iii 个二进制位就是数组中所有元素的第 iii 个二进制位之和除以 333 的余数。
//
//这样一来，对于数组中的每一个元素 xxx，我们使用位运算(x  >> i) & 1\texttt{(x >> i) \ & 1}(x  >> i) & 1 得到 xxx 的第 iii 个二进制位，并将它们相加再对 333 取余，得到的结果一定为 000 或 111，即为答案的第 iii 个二进制位。


class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int ans = 0;
        for (int i = 0; i < 32; ++i) {
            int total = 0;
            for (int num : nums) {
                total += ((num >> i) & 1);
            }
            if (total % 3) {
                ans |= (1 << i);
            }
        }
        return ans;
    }
};

