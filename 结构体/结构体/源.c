#include <stdio.h>
#include <assert.h>
#include <errno.h>
//typedef struct Stu
//{
//	char name[20];//名字
//	int age;//年龄
//	char sex[5];//性别
//	char id[20];//学号
//}Stu;
//
//
//struct  stu
//{
//	int a;
//	char b;
//	float c;
//}x;
//typedef struct stu stu;
//
//struct
//{
//	int a;
//	char b;
//	float c;
//}a[20], * p;
//
//struct Point
//{
//	int x;
//	int y;
//}p1; //声明类型的同时定义变量p1
//struct Point p2; //定义结构体变量p2
//
//
//
////初始化：定义变量的同时赋初值。
//struct Point p3 = { 1, 2 };
//struct Stu 
//{
//	char name[15];//名字
//	int age; //年龄
//};
//struct Stu s = { "zhangsan", 20 };//初始化
//struct Stu g = { .age = 20,.name = "lisi" };
//struct Node
//{
//	int data;
//	struct Point p;
//	struct Node* next;
//}n1 = { 10, {4,5}, NULL }; //结构体嵌套初始化
//struct Node n2 = { 20, {5, 6}, NULL };//结构体嵌套初始化
//struct Node1
//{
//	int data;
//	struct Node1* next;
//};
//int main()
//{
//	/*p = &x;
//	perror("&x");*/
//	struct Point p3;
//
//	return 0;
//}
//struct Stu
//{
//	char name[20];
//	int age;
//};
//void print(struct Stu* ps)
//{
//	printf("name = %s ? age = %d\n", (*ps).name, (*ps).age);
//	//使用结构体指针访问指向对象的成员
//	printf("name = %s ? age = %d\n", ps->name, ps->age);
//}
//int main()
//{
//	struct Stu s = { "zhangsan", 20 };
//	print(&s);//结构体地址传参
//	return 0;
//}
//struct S1
//{
//	char c1;
//	int i;
//	char c2;
//};
//int main()
//{
//	printf("%d\n", sizeof(struct S1));
//}
//
//#include <stdio.h>
//#pragma pack(8)//设置默认对齐数为8
//struct S1
//{
//	char c1;
//	int i;
//	char c2;
//};
//#pragma pack()//取消设置的默认对齐数，还原为默认
//#pragma pack(1)//设置默认对齐数为1
//struct S2
//{
//	char c1;
//	int i;
//	char c2;
//};
//#pragma pack()//取消设置的默认对齐数，还原为默认
//int main()
//{
//	//输出的结果是什么？
//	printf("%d\n", sizeof(struct S1));
//	printf("%d\n", sizeof(struct S2));
//	
//		return 0;
//}
struct S
{
	int data[1000];
	int num;
};
struct S s = { {1,2,3,4}, 1000 };
//结构体传参
void print1(struct S s)
{
	printf("%d\n", s.num);
}
//结构体地址传参
void print2(struct S* ps)
{
	printf("%d\n", ps->num);
}
int main()
{
	print1(s);//传结构体
	print2(&s); //传地址
	return 0;
}