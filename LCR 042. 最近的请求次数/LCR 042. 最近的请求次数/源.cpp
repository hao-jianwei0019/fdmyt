class RecentCounter {
public:
    RecentCounter() {
        nums.push_back(0);
        cur = 0;
        i = 1;
    }

    int ping(int t) {
        nums.push_back(t);
        i++;
        while (t - nums[cur] > 3000)cur++;
        return cur == 0 ? i - cur - 1 : i - cur;
    }
    vector<int> nums;
    int cur;//
    int i;//下一个要插入的位置
};

/**
 * Your RecentCounter object will be instantiated and called as such:
 * RecentCounter* obj = new RecentCounter();
 * int param_1 = obj->ping(t);
 */