#include <iostream>
using namespace std;
const int N = 1005;
int f[N], g[N];
int nums[N];
int main() {
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++)
    {
        cin >> nums[i];
    }
    for (int i = 1; i <= n; i++)
    {
        f[i] = 1;
        for (int j = 1; j < i; j++)
        {
            if (nums[j] < nums[i])
                f[i] = max(f[i], f[j] + 1);
        }
    }
    for (int i = n; i > 0; i--)
    {
        g[i] = 1;
        for (int j = i + 1; j <= n; j++)
        {
            if (nums[i] > nums[j])
                g[i] = max(g[i], g[j] + 1);

        }
    }
    int len = 0;
    for (int i = 1; i <= n; i++)
    {
        len = max(len, g[i] + f[i] - 1);
    }
    cout << n - len << endl;
    return 0;
}
// 64 λ������� printf("%lld")