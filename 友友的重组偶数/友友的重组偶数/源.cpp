#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    while (n--)
    {
        int tmp;
        cin >> tmp;
        int k = -1;
        int b = 0;
        int ret = 0;
        while (tmp)
        {
            int a = tmp % 10;
            tmp /= 10;

            if (a % 2 == 0 && k == -1)
            {
                k = a;
                continue;
            }
            if (a == 0)
            {
                b++;
                continue;
            }
            ret = ret * 10 + a;
        }
        if (k == -1)
        {
            cout << -1 << endl;
        }
        else
        {
            ret = ret * 10 + k;
            while (b--)ret = ret * 10;
            cout << ret << endl;
        }
    }
    return 0;
}
// 64 λ������� printf("%lld")