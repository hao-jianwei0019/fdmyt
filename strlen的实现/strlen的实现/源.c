#include <string.h>
#include <stdio.h>
int my_strlen1(char* str)
{
	int count = 0;
	while (*str != '\0')
	{
		count++;
		str++;//字符指针+1，向后跳1个字符
	}
	return count;
}

int my_strlen2(char* str)
{
	if (*str != '\0')
		return 1 + my_strlen2(str + 1);
	else
		return 0;
}

int my_strlen3(char* str)
{
	int count = 0;
	char* start = str;
	while (*str != '\0')
	{
		
		str++;//字符指针+1，向后跳1个字符
	}
	return str-start;
}

int main()
{
	char arr[] = "hjw";

	int len1 = my_strlen1(arr);
	//数组名其实传递的是数组首元素的地址
	printf("%d\n", len1);
	int len2 = my_strlen2(arr);
	printf("%d\n", len2); 
	int len3 = my_strlen3(arr);
	printf("%d\n", len3);
	return 0;
}